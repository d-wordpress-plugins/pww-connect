<?php

/**
 * Plugin Name: Printwebwinkel Connect
 * Plugin URI: https://printwebwinkel.nl
 * Description: Ontdek de ultieme oplossing voor naadloze drukwerk verkoop binnen jouw WordPress-website! Bied gepersonaliseerde producten aan, van visitekaartjes tot posters, moeiteloos met WooCommerce.
 * Version: 1.2.42
 * Author: Printwebwinkel B.V.
 * Author URI: https://printwebwinkel.nl
 * Text Domain: pww-connect
 * Requires at least: 6.0
 * Requires PHP: 7.3
 * Tested up to: 6.3.2
 * 
 * WC requires at least: 8.0.2
 * WC tested up to: 8.2.0
 *
 * @package PwwConnect
*/

defined( 'ABSPATH' ) || exit;

function pww_connect_plugin_init()
{
    define( 'PWW_CONNECT_PLUGIN_NAME', 		'Printwebwinkel Connect' );
    define( 'PWW_CONNECT_PLUGIN_PATH', 		dirname( __FILE__ ));
    define( 'PWW_CONNECT_PLUGIN_ASSETS',    plugin_dir_url( __FILE__ ) . 'assets/' );
    define( 'PWW_CONNECT_PLUGIN_VIEWS',     PWW_CONNECT_PLUGIN_PATH . '/templates/' );

    define( 'PWW_CONNECT_PLUGIN_VERSION',   '1.2.42' );
    
    define( 'PWW_CONNECT_MIN_WC_VERSION', 	'7.1.0' );
    
    define( 'PWW_CONNECT_API_URL', 			'https://api.pwwservices.com/' );
    
    define( 'PWW_CONNECT_ECOM_PORTAL_URL',	'https://app.pwwservices.com/');
    define( 'PWW_CONNECT_ECOM_SUPPORT_URL', 'https://app.pwwservices.com/');

    /**
     * The code that runs during plugin activation.
     */
    register_activation_hook( __FILE__, function() {
        require_once plugin_dir_path( __FILE__ ) . 'includes/class-pww-connect-plugin-activator.php';
        PwwConnectPluginActivator::activate();
    });
    
    /**
     * The code that runs during plugin deactivation.
     */
    register_deactivation_hook( __FILE__, function() {
        require_once plugin_dir_path( __FILE__ ) . 'includes/class-pww-connect-plugin-deactivator.php';
        PwwConnectPluginDeactivator::deactivate();
    });

    require_once 'includes/class-pww-connect-plugin.php';

    $pww_connect_plugin = new PwwConnectPlugin;
    $pww_connect_plugin->run();
}

add_action( 'plugins_loaded', 'pww_connect_plugin_init' );

require 'plugin-update-checker/plugin-update-checker.php';
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;

PucFactory::buildUpdateChecker(
	'https://gitlab.com/d-wordpress-plugins/pww-connect/',
	__FILE__,
	'pww-connect'
);
