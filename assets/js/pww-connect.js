jQuery(function($) {
	'use strict';

	let pww_connect_product_configurator = '';
	let pww_connect_product_uploader = '';

	let printAppInstance;

	$( document ).ready(function() {
		pww_connect_product_configurator = $('#pww_connect_product_configurator');

		if(pww_connect_product_configurator.length > 0) {
			pww_connect_wc_product_init();
		}

		pww_connect_product_uploader = $('#pww_connect-print_uploader');
		if(pww_connect_product_uploader.length > 0) {
			var orderId = pww_connect_product_uploader.data('order-id');
			var orderEmail = pww_connect_product_uploader.data('order-email');

			var productUploadRequest = pww_action_request('uploader/configure', {
				"data": JSON.stringify({
					"order_id": orderId,
					"email": orderEmail
				})
			}, 'POST');
			
			productUploadRequest.done(function (response) {
				pww_connect_product_uploader.html(response.data.html);

				if (orderId != '' && orderEmail != '') {
					pww_connect_product_uploader.find('.pww-uploader-dropzone').each(function() {
						var calculationId = $(this).data('pww-calculation-id');
						var uploaderId = $(this).data('pww-uploader-id');

						pww_file_uploader('#pww-uploader-' + uploaderId, {
							'pww_calculation_id': calculationId,
							'pww_uploader_id': uploaderId,
							'pww_uploaded_when' : 'uploader'
						});
					});
				}
			});
		}

		const progressBar = document.getElementById('progress-bar');
		if (progressBar) {
			// Haal het data-progress attribuut op
			const progress = progressBar.getAttribute('data-percentage');

			// Werk de progress bar bij met de opgehaalde waarde
			updateProgressBar(progress);
		}

		$('.pww-config-btn button').click(function() {
			var customOffset = 0;

			var adminBar = $('body').find('#wpadminbar');
			if (adminBar.length) {
				customOffset += adminBar.outerHeight(true);
			}

			var headerDiv = $('body').find('.whb-header');
			if (headerDiv.length) {
				customOffset += headerDiv.outerHeight(true);
			}

			var targetOffset = $('#pww_connect_product_configurator').offset().top - customOffset;
			$('html, body').animate({
				scrollTop: targetOffset
			}, 1000); 
		});

		$('body').on('change', '[data-pww-round-value]', function() {
			// Haal de waarde op uit het inputveld
			var value = parseFloat($(this).val());
		
			// Controleer of er een getal achter de komma is
			if (!isNaN(value) && value % 1 !== 0) {
			  // Rond de waarde af op één decimaal
			  var roundedValue = Math.round(value * 10) / 10;
		
			  // Plaats de afgeronde waarde terug in het inputveld
			  $(this).val(roundedValue.toFixed(1));
			}
		});

		// Callback functie wanneer een input wordt aangepast.
		// $('body').on('input change', '[data-pww-func-callback]', function() {
		$('body').on('change', '[data-pww-func-callback]', function() {
			var variation = $(this);
			var variation_block = variation.parent().closest('.pww-variation-option');

			var selectedCode = variation.data('pww-func-field');
			var selectedValue = variation.val();

			var attributes = [];
			if ($(this).hasClass('pww-step-extra-select')) {
				var selectedOption = variation.find('option:selected');

				if (!selectedOption.hasClass('pww-variation-warning')) {
					variation.parent().closest('.pww-variation-block').find('.pww-loader-wrapper').removeClass('hide');
					pww_connect_product_configurator.find('.pww-sidebar-shopping-cart .pww-loader-wrapper').removeClass('hide');
				}

				var options = [];

				var opt = {
					"code": selectedCode,
					"value": selectedValue,
					"main_step": "extra_options"
				};

				options.push(opt);
			} else {
				if (variation_block.length > 0) {
					variation_block.find('[data-pww-func-field]').each(function() {
						attributes.push({
							"code": $(this).data('pww-func-field'),
							"value": $(this).val()
						});
					});
				}
			}

			// Haal de waarde van data-pww-func-callback attribuut op
    		var callbackFunctionName = $(this).data('pww-func-callback');

			if (callbackFunctionName == 'calculateSquareMeters') {
				var squareMetersBlock = pww_connect_product_configurator.find('.step-option-square-meters');
				var squareMetersForm = pww_connect_product_configurator.find('.step-option-square-meters-form');
				var squareMetersTotalSpan = squareMetersBlock.find('.step-option-square-meters-total');
				
				const squareMetersWidth = squareMetersForm.find('#option-width').val();
				const squareMetersHeight = squareMetersForm.find('#option-height').val();
				
				var squareMeters = '0';
				if (squareMetersWidth && squareMetersHeight) {
					squareMeters = calculateSquareMeters(squareMetersWidth, squareMetersHeight);
				}
				
				squareMetersTotalSpan.find('span').html(squareMeters);
			} else if (callbackFunctionName == 'setExtraOption') {
				if ($(this).hasClass('pww-step-extra-select') && selectedOption.hasClass('pww-variation-warning')) {
					var selected_option = {};
					selected_option["selected_code"] = selectedCode;
					selected_option["selected_value"] = selectedValue;

					var solutionRequest = pww_action_request(variation.data('pww-ajax-action'), {
						"data": JSON.stringify(selected_option)
					}, 'POST');

					solutionRequest.done(function (response) {
						$.modal.close();
						$('body .pww-modal').remove();

						if (response.data.options !== undefined) {
							pww_connect_wc_product_init({
								"options": response.data.options,
								"warning_fixed": response.data.warning_fixed
							});
						} else {
							$('.pww-config-modal-wrapper').html(response.data.html);
							$('body #pww-modal-solutions').modal();
						}
					});
				} else {
					pww_connect_wc_product_init({
						"options": options
					});
				}

			} else if (callbackFunctionName == 'setOption') {
				var options = {};

				let globalFieldChange = false;
				let globalFieldChangeValue; // Globaal definiëren
				const $element = $(this);
				const isGlobal = $element.data('pww-func-is-global') === 1;

				if (isGlobal) {
					globalFieldChange = $element.data('pww-func-field');
					globalFieldChangeValue = $element.val(); // Nu wordt globalFieldChangeValue globaal ingesteld
				}

				pww_connect_product_configurator.find('[data-pww-option-file-id]').each(function() {
					// Haal de huidige key op
					const key = $(this).data('pww-option-file-id');
					
					// Controleer of er al een object bestaat voor deze key, zo niet, initialiseer het
					options[key] = options[key] || {};

					// Vul het object voor deze key met de huidige data
					options[key][globalFieldChange ? globalFieldChange : $(this).data('pww-func-field')] = globalFieldChange ? globalFieldChangeValue : $(this).val();
				});

				pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

				pww_connect_wc_product_init({
					"files": options
				});
			} else if (callbackFunctionName == 'changeDesigns') {
				var minTotalDesigns = parseInt($(this).attr('min'));
				var maxTotalDesigns = parseInt($(this).attr('max'));
				var totalDesigns = parseInt($(this).val());

				if (totalDesigns < minTotalDesigns) {
					totalDesigns = minTotalDesigns;
				} else if (totalDesigns > maxTotalDesigns) {
					totalDesigns = maxTotalDesigns;
				}

				var options = [];

				var opt = {
					"code": $(this).data('pww-func-field'),
					"value": totalDesigns,
				};

				options.push(opt);

				pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

				pww_connect_wc_product_init({
					"options": options
				});
			} else if (callbackFunctionName == 'changeTotalDesigns') {
				var fieldName = $(this).data('pww-func-field');
				var totalDesigns = parseInt($('[data-pww-func-field="total-designs"]').val());

				if (fieldName == 'decrease') {
					totalDesigns--;
				} else {
					totalDesigns++;
				}

				totalDesigns.val(totalDesigns);
			} else if (callbackFunctionName == 'setQuantity') {
				var selectedQuantity = $(this).val();
				var selectedPrintMethod = $(this).data('pww-printmethod');

				var element = $('body').find('#' + selectedPrintMethod + '-custom-quantity');
				
				var loadElement = $('body').find('.quantities-custom-quantity-loader');
				
				element.html(loadElement.html());
				element.removeClass('hide');
				
				if (selectedQuantity.length === 0) {
					element.html('');
					element.addClass('hide');
				} else {
					pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

					var calculationId = $('input[name=pww-calculation-id]').val();
	
					var request = pww_action_request('calculation/' + calculationId + '/quantities/search', {
						"data": JSON.stringify({
							printMethod: selectedPrintMethod,
							quantity: selectedQuantity,
						})
					}, 'POST');
	
					request.done(function (response) {
						pww_connect_product_configurator.find('.pww-loader-wrapper').addClass('hide');

						element.replaceWith(response.data.html);
					});
				}
			} else if (callbackFunctionName == 'updateCalculation') {
				pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

				// Veld dat we willen aanpassen
				const $field = $(this);

				const fieldName = $(this).data('pww-ajax-field');
				const fieldValue = $(this).val();

				const updateData = {
					[fieldName]: fieldValue
				};

				const updateRequest = pww_action_request($(this).data('pww-ajax-action'), {
					"data": JSON.stringify(updateData)
				}, 'PUT');

				updateRequest.done(function (response) {
					pww_connect_product_configurator.find('.pww-loader-wrapper').addClass('hide');

					// Voeg een groene achtergrondkleur toe
					$field.css('background-color', 'green');

					// Verwijder de groene kleur na een bepaalde tijd
					setTimeout(function() {
						$field.css('background-color', ''); // Reset naar de oorspronkelijke kleur
					}, 2000);
				});
			} else if (callbackFunctionName == 'updateShipping') {
				pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

				pww_connect_wc_product_init({
					"shipment": $(this).val()
				});
			} else if (callbackFunctionName == 'updateShippingUrgency') {
				pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

				pww_connect_wc_product_init({
					"shipmentUrgency": $(this).val()
				});
			}
		});
	
		// Callback functie na klikactie.
		$('body').on('click', '[data-pww-func-callback-click]', function() {
			var clickEvent = $(this).data('pww-func-callback-click');
			var fieldName = $(this).data('pww-func-field');

			if (clickEvent == 'changeTotalDesigns') {
				var totalDesignsField = $('[data-pww-func-field="total-designs"]');
				var totalDesigns = parseInt(totalDesignsField.val());

				if (fieldName == 'decrease') {
					totalDesigns--;
				} else {
					totalDesigns++;
				}

				totalDesignsField.val(totalDesigns).trigger('change');
			} else if (clickEvent == 'searchQuantity') {
				var post_type = 'POST';
				if ($(this).data('pww-ajax-method')) {
					post_type = $(this).data('pww-ajax-method');
				}

				var field = $(fieldName);

				var options = {};
				options['given_quantity'] = field.val();

				var response = pww_action_request($(this).data('pww-ajax-action'), {
					"data": JSON.stringify(options)
				}, post_type);

				response.done(function (response) {	
					// pww-result-selector
				});
			} else if (clickEvent == 'setCustomSize') {
				var options = [];

				options.push({
					"code": $(this).data('pww-func-code'),
					"value": $(this).data('pww-func-value'),
					"width": parseFloat($('#option-width').val()),
					"height": parseFloat($('#option-height').val()),
				});
				
				pww_connect_wc_product_init({
					"options": options
				});
			} else if (clickEvent == 'setSquareMeters') {
				var options = [];

				options.push({
					"code": $(this).data('pww-func-code'),
					"value": $(this).data('pww-func-value'),
					"square_meter_width": parseFloat($('#option-width').val()),
					"square_meter_height": parseFloat($('#option-height').val()),
				});

				pww_connect_wc_product_init({
					"options": options
				});
			} else if (clickEvent == 'removeFile') {
				if (confirm('Let op: U staat op het punt om 1 bestand te verwijderen!')) {
					pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

					const key = $(this).data('pww-func-file-id');

					pww_connect_wc_product_init({
						"files": key
					});
				}
			} else if (clickEvent == 'removeFiles') {
				if (confirm('Let op: U staat op het punt om alle bestanden te verwijderen!')) {
					pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

					pww_connect_wc_product_init({
						"files": "remove"
					});
				}
			} else if (clickEvent == 'removeError') {
				pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

				pww_connect_wc_product_init({
					"removeError": fieldName,
					"forceStep": pww_connect_product_configurator.find('.product-configurator-step.last-step').data('pww-step-code')
				});
			} else if (clickEvent == 'setQuickChoice') {
				pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

				pww_connect_wc_product_init({
					"quick_choice_id": fieldName
				});
			} else if (clickEvent == 'setTextileSizes') {
				pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

				var textileSizes = {};

				pww_connect_product_configurator.find('[data-pww-textile-sizes-type]').each(function() {
					const sizeType = $(this).data('pww-textile-sizes-type');
					const sizeCode = $(this).data('pww-textile-sizes-option');
					
					// Controleer of er al een object bestaat voor deze key, zo niet, initialiseer het
					textileSizes[sizeType] = textileSizes[sizeType] || {};

					// Vul het object voor deze key met de huidige data
					textileSizes[sizeType][sizeCode] = $(this).val();
				});

				var options = [];

				var option = {
					"code": 'textile_sizes',
					"textile_sizes": textileSizes,
				};
				if (typeof $(this).data('pww-textile-sizes-id') !== undefined) {
					option["step_code"] = $(this).data('pww-textile-sizes-id');
				}

				options.push(option);

				pww_connect_wc_product_init({
					"options": options
				});
			} else if (clickEvent == 'setProductAccessories') {
				pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

				var productAccessories = {};

				// Iterate over all product accessory inputs
				pww_connect_product_configurator.find('.product-accessory-input').each(function() {
					var $current = $(this).closest('.step-option');
					var assId = $current.data('pww-variant-value'); // Fetch variant value
					var value = parseFloat($(this).val()); // Parse the value as a float (ensure numeric)

					// Only add the accessory if its value is greater than 0
					if (value > 0) {
						productAccessories[assId] = value;
					}
				});

				// Prepare options array with product accessories
				var options = [{
					code: "product_accessories",
					product_accessories: productAccessories
				}];

				// Initialize the product configurator with options
				pww_connect_wc_product_init({
					options: options
				});
			} else if (clickEvent == 'setOption') {
				// Show the loader
				pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

				// Create options array
				const options = [{
					code: fieldName,
					value: $(this).data('pww-func-field-value')
				}];

				// Initialize the product configurator with options
				pww_connect_wc_product_init({ options });
			}
		});

		$('body').on('click', '.vat-button', function() {
			if ($(this).data('vat-type') == 'excl') {
				$('.price-overview__excl').removeClass('hide');
				$('.price-overview__incl').addClass('hide');
			} else {
				$('.price-overview__incl').removeClass('hide');
				$('.price-overview__excl').addClass('hide');
			}

			$('.vat-button.active').removeClass('active');
			$(this).addClass('active');
		});
		
		$('body').on('click', '.is-custom-size', function() {
            if ($(this).hasClass('has-open-form')) {
                $(this).removeClass('has-open-form');
                $('body').find('.format-custom-size').addClass('hide');
            } else {
                $(this).addClass('has-open-form');
                $('body').find('.format-custom-size').removeClass('hide');
            }
        });
		
		$('body').on('click', '.is-has-more', function() {
			var wrap = $('body').find('.step-content-options-more');
			
            if (wrap.hasClass('hide')) {
                wrap.removeClass('hide');
				
				$(this).find('.show-text').addClass('hide');
				$(this).find('.hide-text').removeClass('hide');
            } else {
                wrap.addClass('hide');
				
				$(this).find('.show-text').removeClass('hide');
				$(this).find('.hide-text').addClass('hide');
            }
        });

		$('body').on('click', '.pww-step-extra-checkbox', function() {
			// Find the select element using the dynamic ID
			var selectElement = $('body').find('#extra_select_' + $(this).data('pww-extra-code'));

			if ($(this).is(':checked')) {
				var firstNonEmptyOption = selectElement.find('option').not(':empty').first();
				selectElement.val(firstNonEmptyOption.val()).trigger('change');

				// selectElement.select2('open');
				selectElement.selectmenu("open");
			} else {
				selectElement.val(null).trigger('change');
			}
		});

		$('body').on('click', '.pww-show-more-button', function() {
			var parentDiv = $(this).parent().closest('.pww-variation-options-extras-wrapper');
			var visibleContent = parentDiv.find('.pww-visible-content');			
			var hiddenContent = parentDiv.find('.pww-hidden-content');

			if (hiddenContent.hasClass('hide')) {
				visibleContent.addClass('hide');
				hiddenContent.removeClass('hide');
				
				$(this).html($(this).data('close-text'));
			} else {
				visibleContent.removeClass('hide');
				hiddenContent.addClass('hide');
				
				$(this).html($(this).data('original-text'));
			}
		});

		// Function for showing extra content box
		$('body').on('click', '[data-pww-variation-box="true"]', function() {
			var variation_option = $(this);
			var variation_option_box = variation_option.find('.pww-variation-option-box');

			if (variation_option_box.hasClass('hide')) {
				variation_option_box.removeClass('hide');
			}
		});

		$('body').on('click', '[data-pww-model-id]', function(event) {
			$.modal.close();
			$('body #pww-modal-' + $(this).data('pww-model-id')).modal();
		});

		$('body').on('click', '[data-pww-variant-step-btn]', function(event) {
			var variation = $(this);

			var variation_block = variation.parent().closest('.pww-variation-block');

			var options = [];
			var hasError = false;

			variation_block.find('[data-pww-option-form-field]').each(function() {
				var variation_block = $(this).parent().closest('.pww-variation-option-box');
				var blockError = variation_block.find('.pww-variation-option-error');
				blockError.addClass('hide');

				var error = false;

				if ($(this).data('pww-min-value') !== undefined && $(this).val() < $(this).data('pww-min-value')) {
					error = true;
				}
				if ($(this).data('pww-max-value') !== undefined && $(this).val() > $(this).data('pww-max-value')) {
					error = true;
				}

				if (error) {
					hasError = true;
	
					blockError.find('span').html('Formaat is niet geldig.');
					blockError.removeClass('hide');
				}

				var opt = {
					"code": $(this).data('pww-option-form-field'),
					"value": $(this).val()
				};

				options.push(opt);
			});

			if (hasError === false) {
				variation_block.find('.pww-loader-wrapper').removeClass('hide');
				pww_connect_product_configurator.find('.pww-sidebar-shopping-cart .pww-loader-wrapper').removeClass('hide');

				pww_connect_wc_product_init({
					"options": options
				});
			}
		});

		$('body').on('click', '.total_designs-stepper', function(event) {
			var form_block = $(this).parent().closest('.step-option-input');

			var inputField = form_block.find('input');

			// Get the current value, default to 0 if it's not a number
			var currentValue = parseInt(inputField.val()) || 0;

			let targetElement = $(event.target);
			if (targetElement.is('img')) {
				targetElement = targetElement.closest('.total_designs-stepper');
			}

			if (targetElement.hasClass('disabled')) {
				return; // Do nothing if the target element has the 'disabled' class
			}

			var newValue = parseInt(1);

			if (targetElement.hasClass('increment')) {
				if (currentValue < parseFloat(inputField.attr('max'))) {
					newValue = currentValue + 1; // Increase the value by 1
				}
			} else if (targetElement.hasClass('deincrement')) {
				if (currentValue > parseFloat(inputField.attr('min'))) {
					newValue = currentValue - 1; // Decrease the value by 1
				}
			}

			inputField.val(newValue);

			if (newValue === parseInt(inputField.data('previous-value'))) {
				form_block.find('.total_designs-btn').addClass('hide');
			} else {
				form_block.find('.total_designs-btn').removeClass('hide');
			}

			form_block.find('.deincrement').toggleClass('disabled', newValue === parseInt(inputField.attr('min')));
			form_block.find('.increment').toggleClass('disabled', newValue === parseInt(inputField.attr('max')));
		});

		// Monitor input field for manual value changes
		$('body').on('input', 'input[data-monitor="true"]', function() {
			var inputField = $(this);
			var value = parseFloat(inputField.val());

			// Check if the value is a valid number
			if (!isNaN(value)) {
				// Perform validation or any other checks here
				if (value < inputField.attr('min')) {
					inputField.val(inputField.attr('min'));
				} else if (value > inputField.attr('max')) {
					inputField.val(inputField.attr('max'));
				}
			} else {
				inputField.val(inputField.attr('min') || 1);
			}

			var form_block = inputField.closest('.step-option-input');
			if (isNaN(value) || value === parseInt(inputField.data('previous-value'))) {
				form_block.find('.total_designs-btn').addClass('hide');
			} else if (value < parseInt(inputField.attr('min'))) {
				form_block.find('.total_designs-btn').addClass('hide');
			} else {
				form_block.find('.total_designs-btn').removeClass('hide');
			}

			form_block.find('.deincrement').toggleClass('disabled', value === parseInt(inputField.attr('min')));
			form_block.find('.increment').toggleClass('disabled', value === parseInt(inputField.attr('max')));
		});
		
		// data-pww-variant
		$('body').on('click', '[data-pww-variant-code]', function(event) {
			var variation = $(this);

			if (variation.data('pww-variant-code') == 'product_accessories') {
				var inputField = variation.find('input');

				// Get the current value, default to 0 if it's not a number
				var currentValue = parseInt(inputField.val()) || 0;

				if ($(event.target).hasClass('step-option-increment')) {
					inputField.val(currentValue + 1); // Increase the value by 1
				} else if ($(event.target).hasClass('step-option-deincrement')) {
					inputField.val(currentValue - 1); // Decrease the value by 1
				}

				// Trigger the 'input' event after the value of the input field is updated
				inputField.trigger('input');
			} else if (variation.data('pww-variant-code') == 'total_designs') {
				const totalDesignBlock = variation.closest('.step-option-input');
				const inputField = totalDesignBlock.find('input');
				const totalDesignsValue = parseInt(inputField.val(), 10);

				const options = [{
					code: 'total_designs',
					input_value: totalDesignsValue
				}];

				pww_connect_wc_product_init({ options });
			} else if ($(event.target).closest('.product-configurator-input').length > 0) {

			} else if (variation.data('pww-variant-value') == 'upload_file') {
				var element = $('.product-configurator-uploader');
			
				pww_file_uploader('#' + element.attr('id'), {
					'pww_calculation_id': element.data('pww-calculation-id'),
					'pww_uploader_id': element.data('pww-uploader-id'),
					'pww_uploaded_when' : 'product_configure'
				});
				
				$('.product-configurator-uploader').removeClass('hide');
			} else if (variation.data('pww-variant-value') == 'design_online') {
				$('body').find('.pww-loader-wrapper').removeClass('hide');
				$('.product-configurator-uploader').addClass('hide');

				// Get handles to the UI elements we'll be using
				const previewDiv = document.getElementById('pww-product-uploaded-files');

				let calculation_id = $('input[name=pww-calculation-id]').val();
				if (variation.data('pww-calculation-id') !== undefined) {
					calculation_id = variation.data('pww-calculation-id');
				}

				pww_action_request('calculation/' + calculation_id + '/editor', {}, 'POST')
					.done(function (response) {
						initializePrintAppEditor(calculation_id, response.data);
					});
			} else if ($(event.target).closest('.step-option-info').length > 0) {
				var requestUrl = variation.find('.step-option-info').data('pww-ajax-action');
				var productOptionInfoTextRequest = pww_action_request(requestUrl, {}, 'GET');
				
				productOptionInfoTextRequest.done(function (response) {
					$.modal.close();
                    $('body .pww-config-modal-info').html(response.data.html);
                    $('body .pww-config-modal-info').modal();
                });
			} else if ($(event.target).closest('.option-is-selected').length > 0) {
				$('body').find('.pww-loader-wrapper').removeClass('hide');

				var requestUrl = variation.find('.option-is-selected').data('pww-ajax-action');
				pww_action_request(requestUrl, {}, 'GET');

				// Refresh the calculation
				pww_connect_wc_product_init({
					'forceStep': 'summary'
				});
			} else if (event.target.className == 'pww-variation-option-help') {
                var solutionRequest = pww_action_request(event.target.getAttribute('data-pww-ajax-action'), {}, 'GET');

                solutionRequest.done(function (response) {
					$.modal.close();
                    $('body .pww-config-modal-info').html(response.data.html);
                    $('body .pww-config-modal-info').modal();
                });
			} else if (event.target.className == 'pww-help-icon') {
				$('#' + event.target.getAttribute('data-pww-model-id')).modal();
			} else if (variation.data('pww-variant-value') == 'custom') {
				var variation_option_box = variation.find('.pww-variation-option-box');

				if (variation_option_box.hasClass('hide')) {
					variation_option_box.removeClass('hide');
				}
			} else if (variation.hasClass('pww-variation-warning') || variation.data('pww-main-step') == 'solution-step') {
				var selected_option = {};
				selected_option["selected_code"] = variation.data('pww-variant-code');
	
				if (variation.data('pww-variant-value')) {
					selected_option["selected_value"] = variation.data('pww-variant-value');
				}

				if (variation.data('pww-field-printmethod')) {
					selected_option["printmethod"] = variation.data('pww-field-printmethod');
				}

				if (variation.data('pww-main-step') == 'solution-step') {
					selected_option["action"] = "solution_fix";
				}

                var solutionRequest = pww_action_request(variation.data('pww-ajax-action'), {
                    "data": JSON.stringify(selected_option)
                }, 'POST');

                solutionRequest.done(function (response) {
					$.modal.close();
                    $('body .pww-modal').remove();
                    // $('body #pww-modal-solutions').remove();

					if (response.data.options !== undefined) {
						pww_connect_wc_product_init({
							"options": response.data.options,
							"warning_fixed": response.data.warning_fixed
						});
					} else {
						$('.pww-config-modal-wrapper').html(response.data.html);
						$('body #pww-modal-solutions').modal();
					}
    
                    // variation_block.find('.pww-loader-wrapper').addClass('hide');
                    // pww_connect_product_configurator.find('.pww-sidebar-shopping-cart .pww-loader-wrapper').addClass('hide');
                });
			} else {
				var variation_block = variation.parent().closest('.pww-variation-block');
				
				$('body').find('.pww-loader-wrapper').removeClass('hide');

				variation_block.find('.pww-loader-wrapper').removeClass('hide');
				pww_connect_product_configurator.find('.pww-sidebar-shopping-cart .pww-loader-wrapper').removeClass('hide');

				var options = [];
	
				var new_option = {};
				new_option["code"] = variation.data('pww-variant-code');

				if (variation.data('pww-variant-value') || variation.data('pww-variant-value') == "0") {
					new_option["value"] = variation.data('pww-variant-value');
				}

				if (variation.data('pww-main-step')) {
					new_option["main_step"] = variation.data('pww-main-step');
				}

				if (variation.data('pww-variant-printmethod')) {
					new_option["printmethod"] = variation.data('pww-variant-printmethod');
				}
				
				if (variation.data('pww-variant-step-back')) {
					new_option["step_back"] = true;
				}
				
				var validData = true;
				
				if (variation.find('[type=number]').length) {
					// Verwijzing naar het input-veld
					let inputField = variation.find('[type=number]');

					// Verkrijg de huidige waarde van het inputveld en converteer naar een getal
					let currentValue = parseFloat(inputField.val());
					let minValue = parseFloat(inputField.attr('min'));
					let maxValue = parseFloat(inputField.attr('max'));

					// Controleer of de waarde binnen de grenzen valt
					if (!isNaN(minValue) && currentValue < minValue) {
						validData = false;
    					pwwConnectShowError(`${currentValue} is lager dan het minimum (${minValue}).`);
					} else if (!isNaN(maxValue) && currentValue > maxValue) {
						validData = false;
						pwwConnectShowError(`${currentValue} is hoger dan het maximum (${maxValue}).`);
					} else {
						new_option["value"] = inputField.data('pww-option-form-field');
						new_option["input_value"] = currentValue;
					}
				}
				
				if (variation.data('pww-linked-value-id')) {
					new_option["linked_value"] = $(this).data('pww-linked-value-id');
				}

				if (variation.data('pww-variant-action') == 'clothing_sizes') {
					var clothingSizes = [];
					var clothingSizesMessages = [];

					variation_block.find('[data-pww-variant-field-code]').each(function() {
						var amountOfSize = $(this).val();

						var minValue = $(this).data('pww-min-value');
						var maxValue = $(this).data('pww-max-value');

						if (minValue !== undefined && amountOfSize > 0 && amountOfSize < minValue) {
							validData = false;

							clothingSizesMessages.push({
								"message": 'De waarde voor de maat ' + $(this).data('pww-size-title') + ' (' + $(this).data('pww-size-name') + ') is te laag. Dit moet minimaal ' + minValue + ' zijn'
							});
						}
						if (maxValue !== undefined && amountOfSize > maxValue) {
							validData = false;

							clothingSizesMessages.push('De waarde voor de maat ' + $(this).data('pww-size-title') + ' (' + $(this).data('pww-size-name') + ') is te hoog. Dit kan maximaal ' + maxValue + ' zijn');
						}

						var blockError = variation_block.find('.pww-variation-option-errors');
						if (clothingSizesMessages.length > 0) {
							blockError.removeClass('hide');

							var html = '';
							$.each(clothingSizesMessages, function(index, message) {
								html += '<div class="pww-variation-option-error"><span>' + message + '</span></div>';
							});

							blockError.html(html);
							$('.pww-sidebar-block').find('.pww-loader-wrapper').addClass('hide');
							variation_block.find('.pww-loader-wrapper').addClass('hide');
						} else {
							blockError.html('');
							blockError.addClass('hide');
						}

						var opt = {
							"code": $(this).data('pww-variant-field-code'),
							"value": $(this).val(),
							"type": $(this).data('pww-variant-field-clothing-type')
						};

						if ($(this).data('pww-variant-field-linked-id')) {
							opt["linked_value"] = $(this).data('pww-variant-field-linked-id');
						}

						clothingSizes.push(opt);
					});

					new_option["main_step"] = "clothing_sizes";
					new_option["values"] = clothingSizes;
				} else if (variation.data('pww-variant-action') !== undefined) {
					var variantActionValues = [];
					
					variation_block.find('[data-pww-variant-field-code]').each(function() {
						variantActionValues.push({
							"code": $(this).data('pww-variant-field-code'),
							"value": $(this).val(),
						});
					});

					new_option["values"] = variantActionValues;
				}
	
				options.push(new_option);

				if (variation.data('pww-variant-code') == 'accessories-cross-sell') {
					var accessories = [];
					variation_block.find('[data-pww-option-form-field]').each(function() {
						var opt = {
							"code": $(this).data('pww-option-form-field'),
							"value": $(this).val()
						};

						accessories.push(opt);
					});

					options.push({
						"code": "accessories",
						"options": accessories
					})
				} else if (variation.data('pww-variant-code') == 'apparelSize') {
					var apparelSizes = [];
					variation_block.find('[data-pww-option-form-field]').each(function() {
						var opt = {
							"code": $(this).data('pww-option-form-field'),
							"value": $(this).val()
						};

						apparelSizes.push(opt);
					});

					options.push({
						"code": "apparelSizes",
						"options": apparelSizes
					})
				}
				
				if (validData) {
					pww_connect_wc_product_init({
						"options": options
					});
				}
				
			}
		});

		// Form function
		// hier hier
		$('body').on('click', '[data-pww-option-form-btn]', function() {
			// check if min/max value are set
			var error = false;
			var errorType = '';
			var errorTypeValue = '';

			var ajax_action_btn = $(this);
			var ajax_action = ajax_action_btn.parent().closest('[data-pww-option-form]');

			var variation_option = ajax_action_btn.parent().closest('.pww-variation-option');

			if (ajax_action.data('pww-option-form') == 'configure') {
				var options = [];
				ajax_action.find('[data-pww-option-form-field]').each(function() {
					if ($(this).data('pww-min-value') !== undefined && $(this).val() < $(this).data('pww-min-value')) {
						error = true;
						
						errorType = 'value_to_low';
						errorTypeValue = $(this).data('pww-min-value');
					}
					if ($(this).data('pww-max-value') !== undefined && $(this).val() > $(this).data('pww-max-value')) {
						error = true;
						
						errorType = 'value_to_high';
						errorTypeValue = $(this).data('pww-max-value');
					}

					var opt = {
						"code": $(this).data('pww-option-form-field'),
						"value": $(this).val()
					};

					if (variation_option.data('pww-main-step')) {
						opt["main_step"] = variation_option.data('pww-main-step');
					}

					if ($(this).data('pww-main-step')) {
						opt["main_step"] = $(this).data('pww-main-step');
					}

					if (variation_option.find('[type=number]').length) {
						var inputField = variation_option.find('[type=number]');

						opt["code"] = inputField.data('pww-main-step-code');
						opt["value"] = inputField.data('pww-option-form-field');
						opt["input_value"] = inputField.val();
					}

					if ($(this).data('pww-option-form-field-extra')) {
						opt["input_value"] = $(this).val();
						opt["field_extra"] = $(this).data('pww-option-form-field-extra');
					}

					if ($(this).data('pww-linked-value-id')) {
						opt["linked_value"] = $(this).data('pww-linked-value-id');
					}
					
					options.push(opt);
				});
				
				if (error) {
					var blockError = ajax_action.find('.pww-variation-option-error');
					
					var message = 'Aantal is niet geldig.';
					if (errorType == 'value_to_low') {
						message = 'Waarde is te laag. Minimaal: ' + errorTypeValue;
					} else if (errorType == 'value_to_high') {
						message = 'Waarde is te hoog. Maximaal: ' + errorTypeValue;
					}

					blockError.find('span').html(message);
					blockError.removeClass('hide');
				} else {
					if (variation_option.data('pww-main-step') == 'solution-step') {
						var selected_option = {};
						selected_option["selected_code"] = variation_option.data('pww-variant-code');
			
						if (variation_option.data('pww-variant-value')) {
							selected_option["selected_value"] = variation_option.data('pww-variant-value');
						}
		
						if (variation_option.data('pww-main-step') == 'solution-step') {
							selected_option["action"] = "solution_fix";
						}

						selected_option["width"] = variation_option.find('[data-pww-option-form-field="width"]').val();
						selected_option["height"] = variation_option.find('[data-pww-option-form-field="height"]').val();
		
						var solutionRequest = pww_action_request(variation_option.data('pww-ajax-action'), {
							"data": JSON.stringify(selected_option)
						}, 'POST');
		
						solutionRequest.done(function (response) {
							$.modal.close();
							$('body .pww-modal').remove();
		
							if (response.data.options !== undefined) {
								pww_connect_wc_product_init({
									"options": response.data.options,
									"warning_fixed": response.data.warning_fixed
								});
							} else {
								$('.pww-config-modal-wrapper').html(response.data.html);
								$('body #pww-modal-solutions').modal();
							}
						});
					} else {
						pww_connect_wc_product_init({
							"options": options
						});
					}
				}
			}
		});

		// Form function
		$('body').on('click', '.pww-ajax-action-btn', function() {
			var deliveryBlock = $(this).parent('.pww-variation-block-delivery-form');
			var quantitySearch = deliveryBlock.find('#quantity_search');
			var alertBox = $('#pww-alert-search-quantity');

			if (quantitySearch.length) {
				var searchOk = true;

				if (quantitySearch.data('min-value')) {
					if (quantitySearch.val() < quantitySearch.data('min-value')) {
						searchOk = false;
					}
				}

				if (quantitySearch.data('max-value')) {
					if (quantitySearch.val() > quantitySearch.data('max-value')) {
						searchOk = false;
					}
				}

				if (searchOk) {
					alertBox.addClass('hide');

					pww_copy_next($(this));
				} else {
					alertBox.removeClass('hide');
				}
			}
		});

		$('body').on('change', '.pww-copy-select-changer', function() {
			pww_copy_next($(this));
		});

		// Changer function
		$('body').on('click', '.pww-variation-field', function() {
			var variation = $(this);
			var variation_input = variation.find('.form-check-input');

			var options = [];
			options.push({
				"code": variation_input.attr('name'),
				"value": variation_input.val()
			});

			pww_connect_wc_product_init({
				"options": options
			});
		});
		
		$('body').on('click', '.pww-product-uploader', function() {
			var wrapper = $(this).closest('.pww-product-uploader-wrapper');
			
			if (wrapper.find('.pww-opener-box').hasClass('hide')) {
	            wrapper.find('.pww-opener-box').removeClass('hide');

				wrapper.find('.pww-opener').data('original-text', wrapper.find('.pww-opener').html());
				wrapper.find('.pww-opener').html(wrapper.find('.pww-opener').data('close-text'));

				wrapper.find('.pww-opener').data('original-color', wrapper.find('.pww-opener').css('color'));

				wrapper.find('.pww-opener').css('color', 'red');

// 				pww_uploader('#pww-uploader-' + wrapper.find('.pww-uploader').data('uploader-id'), {
// 					cart_item_key: wrapper.find('.pww-uploader').data('cart-item-key'),
// 					calculation_id: wrapper.find('.pww-uploader').data('calculation-id'),
// 					uploader_id: wrapper.find('.pww-uploader').data('uploader-id')
// 				});
				
				pww_file_uploader('#pww-uploader-' + $(this).data('opener-id'), {
					'cart_item_key': wrapper.find('.pww-uploader-dropzone').data('cart-item-key'),
					'pww_calculation_id': wrapper.find('.pww-uploader-dropzone').data('pww-calculation-id'),
					'pww_uploader_id': wrapper.find('.pww-uploader-dropzone').data('pww-uploader-id'),
					'pww_uploaded_when' : 'cart'
				});
			} else {
				wrapper.find('.pww-opener-box').addClass('hide');
				wrapper.find('.pww-opener').html(wrapper.find('.pww-opener').data('original-text'));
				wrapper.find('.pww-opener').removeAttr('style');
			}
			
			return false;
		});

		$('body').on('click', '.show-or-hide', function() {
			var elm = $(this).closest('.product-name');
	
			if (elm.find('.pww_connect-block-short').hasClass('pww_connect-hide')) {
				elm.find('.pww_connect-block-short').removeClass('pww_connect-hide');
				elm.find('.pww_connect-variation_list_full').addClass('pww_connect-hide');
			} else {
				elm.find('.pww_connect-variation_list_full').removeClass('pww_connect-hide');
				elm.find('.pww_connect-block-short').addClass('pww_connect-hide');
			}
	
			return false;
		});
		
		$('body').on('click', '.pww-file-delete-link', function(e) {
			e.preventDefault();

			var uploaderFileId = jQuery(this).data('uploader-file-id');

			if (uploaderFileId) {
				if (confirm('Weet je zeker dat je het bestand wilt verwijderen?')) {
					var cartItemKey = jQuery(this).parent('.pww-product-files-list').data('cart-item-key');
					var uploaderId = jQuery(this).parent('.pww-product-files-list').data('file-list-id');

					if (uploaderId) {
						$.ajax({
							type: "POST",
							url: variables.pww_connect.ajax_url,
							data: {
								"action": "pww_connect_file_remove",
								"cart_item_key": cartItemKey,
								"uploader_id": uploaderId,
								"file_id": uploaderFileId,
								"_ajax_nonce": variables.pww_connect.ajax_nonce
							},
							success: function(response) {
								if (response.success && $.trim(response.data.html) !== '') {
									jQuery('[data-file-list-id="' + uploaderId + '"]').html(response.data.html);
								} else {
									jQuery('[data-file-list-id="' + uploaderId + '"]').html('');
									jQuery('[data-files-wrapper="' + uploaderId + '"]').addClass('hide');

									jQuery('[data-opener-id="' + uploaderId + '"]').html('Bestand uploaden');
								}
							}
						});
					}
				}
			}
		});

		// preloaded files
		$('body').on('click', '.pww-product-ready-file-choose', function() {
			var elm = $(this);

			var calculation_id = $('input[name=pww-calculation-id]').val();
			var readyFileUpdaterRequest = pww_action_request('calculations/' + calculation_id + '/files', {
				"data": JSON.stringify({
					file_id: elm.data('pww-product-ready-file-id')
				})
			}, 'POST');

			readyFileUpdaterRequest.done(function (response) {
				$('body .pww-product-ready-files').html(response.data.html);
			});
		});

		// When an input field with the class "textile-size-input" is changed
		$('body').on('input', '.textile-size-input', function() {
			var total = 0;

    		// Loop through all input fields with the class "textile-size-input"
			$('body').find('.textile-size-input').each(function() {
				var value = parseFloat($(this).val());

        		// Check if the value is a valid number and add it to the total
				if (!isNaN(value)) {
					total += value;
				}
			});

    		// Update the span with the class "textile-sizes-check-count" with the calculated total
			$('body').find('.textile-sizes-check-count').text(total);

			pww_connect_product_configurator.find('.textile-sizes-check-amount').addClass('hide');
		});

		$('body').on('input', '.product-accessory-input', function() {
			var variation_block = $(this).parent().closest('.product-configurator-step.product_accessories');

			var totalAccessories = 0;

			// Loop through all input fields with the class "product-accessory-input" within the variation block and sum the values
			variation_block.find('.product-accessory-input').each(function() {
				var input = $(this);
				var value = parseFloat(input.val());
				
				// Check if the value is a valid number and not negative, otherwise set input to 0
				if (isNaN(value) || value < 0) {
					value = 0;
					input.val(null); // Update the input value to 0
				}
				
				// Add the value to the total
				totalAccessories += value;
			});

			const btn = variation_block.find('.product-configurator-btn');

			// Check if the total accessories count is greater than 0
			if (totalAccessories > 0) {
				// If there are accessories, update the text to the value in "data-text-continue-with"
				btn.text(btn.data('text-continue-with'));
			} else {
				// If no accessories, update the text to "data-text-continue-without"
				btn.text(btn.data('text-continue-without'));
			}
		});

		$('body').on('added_to_cart', function() {
			pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');

			pww_connect_wc_product_init({
				'calculation_id': null
			});
		});
    });

    function pww_connect_wc_product_init(data = []) {
		let isFirstStep = true;

		const calculationIdInput = $("input[name=pww-calculation-id]").val();
		if (data.calculation_id !== undefined) {
			if (data.calculation_id === null) {
				delete data.calculation_id;
			} else {
				isFirstStep = false;
			}
		} else if (calculationIdInput) {
			data.calculation_id = calculationIdInput;
			isFirstStep = false;
		}

		if (variables.brand_hub !== undefined) {
			data["brand_hub"] = variables.brand_hub;
		}

		if (variables.visitor !== undefined) {
			data["visitor"] = variables.visitor;
		}

		var post_data = $.extend({}, {
			"code": variables.pww_product.id,
			"language_id": variables.pww_product.language_id,
			"wc_product": variables.wc_product,
			"wp_env": variables.wp,
			"wc_env": variables.wc,
			"debugging": variables.pww_connect.debugging
		}, data);
		
		var settings = {
			"url": variables.pww_connect_api_url + "products/configure",
			"method": "POST",
			"timeout": 0,
			"headers": {
				"Content-Type": "application/json",
                "Authorization": "Bearer " + variables.pww_connect_api_key,
				"Domain-Hash": variables.pww_connect_domain_hash,
				"ngrok-skip-browser-warning": "test"
			},
			"data": JSON.stringify(post_data),
		};
		  
		$.ajax(settings).done(function (response) {
			if (response.data && response.data.product_update !== undefined) {
				pww_connect_product_update_check();
			}

			$('#pww_connect_product_configurator').html(response.data.html);

			$('#pww_connect_product_configurator').find('.pww-step-extra-select').each(function() {
				// $('#' + $(this).attr('id')).select2({
				// 	minimumResultsForSearch: 20,
				// 	placeholder: "-"
				// });

				$('#' + $(this).attr('id')).selectmenu({
					change: function( event, data ) {
						$(this).trigger('change');
					}
				});
			});

			var uploadElement = $('#pww_connect_product_configurator').find('.product-configurator-uploader');
			if (uploadElement.length > 0) {
				// Set the attributes
				var attributes = {
					'pww_calculation_id': uploadElement.data('pww-calculation-id'),
					'pww_uploader_id': uploadElement.data('pww-uploader-id'),
					'pww_uploaded_when': 'product_configure'
				};
		
				// Add the callback if it exists
				var callback = uploadElement.data('pww-uploader-callback');
				if (callback) {
					attributes['pww_uploader_callback'] = callback;
				}
		
				// Pass the attributes to the uploader function
				pww_file_uploader('#' + uploadElement.attr('id'), attributes);
			}
			
			var lastStep = $('#pww_connect_product_configurator').find('.last-step');
			if (!isFirstStep && lastStep.length > 0 && $('body').find('#skip-scrolling-to').length === 0) {
				var customOffset = 0;

				var adminBar = $('body').find('#wpadminbar');
				if (adminBar.length) {
					customOffset += adminBar.outerHeight(true);
				}

				var headerDiv = $('body').find('.whb-header');
				if (headerDiv.length) {
					customOffset += headerDiv.outerHeight(true);
				}

				$('html, body').animate({
					scrollTop: lastStep.offset().top - customOffset
				}, 1000);
			}

			if (response.data.available_options) {
				$.map(response.data.available_options, function(available_option){
					if (available_option.hooks) {

						$.map(available_option.hooks, function(hook){

							var hooks_request = pww_action_request(hook.request.url, [], 'POST')
				
							hooks_request.done(function (response) {
								if (hook.request.selector) {
									if(response.data.html !== undefined) {
										if (response.data.html == '') {
											$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-variation-options-copies-holder').addClass('hide');	
										} else {
											if (hook.request.action !== undefined) {
												// $('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-copies-options').find('.pww-loader-wrapper').addClass('hide');
												$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-copies-options').find('.pww-loader-wrapper').remove();
												$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').replaceWith(response.data.html);
											} else {
												$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').html(response.data.html);
											}
										}
									} else if ($.isEmptyObject(response.data.prices)) {
										$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-variation-options-copies-old-holder').addClass('hide');
									} else {
										$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').find('[data-pww-variant-code=copies]').each(function() {
											if (response.data.prices[$(this).data('pww-variant-value')]) {
												const price = response.data.prices[$(this).data('pww-variant-value')];
												$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"] [data-pww-variant-value="' + price.quantity + '"] .pww-variation-option-content-right').html('<span class="pww-variation-option-price">' + price.product_price + '</span>');
											} else {
												$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"] [data-pww-variant-value="' + $(this).data('pww-variant-value') + '"]').addClass('hide');
											}
										});
									}
				
									if (response.data.hooks) {
										pwwHandleHooks(response.data.hooks);
									}
				
									$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').find('.pww-loader-wrapper').addClass('hide');
								}
							});
						});
					}
				});
			}

			if (response.data.hooks) {
				$.map(response.data.hooks, function(hook){
					var hooks_request = pww_action_request(hook.request.url, [], 'GET');
		
					hooks_request.done(function (response) {
						if (hook.request.selector) {
							if(response.data.html !== undefined) {
								if (response.data.html == '') {
									$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-variation-options-copies-holder').addClass('hide');	
								} else {
									if (hook.request.action !== undefined) {
										$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-copies-options').find('.pww-loader-wrapper').remove();
										$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').replaceWith(response.data.html);
									} else {
										$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').html(response.data.html);
									}
								}
							} else if ($.isEmptyObject(response.data.prices)) {
								$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-variation-options-copies-old-holder').addClass('hide');
							} else {
								$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').find('[data-pww-variant-code=copies]').each(function() {
									if (response.data.prices[$(this).data('pww-variant-value')]) {
										const price = response.data.prices[$(this).data('pww-variant-value')];
										$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"] [data-pww-variant-value="' + price.quantity + '"] .pww-variation-option-content-right').html('<span class="pww-variation-option-price">' + price.product_price + '</span>');
									} else {
										$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"] [data-pww-variant-value="' + $(this).data('pww-variant-value') + '"]').addClass('hide');
									}
								});
							}

							if (response.data.hooks) {
								pwwHandleHooks(response.data.hooks);
							}

							$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').find('.pww-loader-wrapper').addClass('hide');
						}
					});
		
				});
			}

			if (response.data.uploader) {
				if (response.data.editor) {
					printAppEditor(response.data.editor);
				} else {
					pww_file_uploader('#pww-uploader-' + response.data.uploader.id, {
						'pww_calculation_id': response.data.calculation_id,
						'pww_uploader_id': response.data.uploader.id,
						'pww_uploaded_when' : 'product_configure'
					});
				}

				// Check for workfiles
				pww_workfiles('#pww-workfiles', {
					'pww_calculation_id': response.data.calculation_id
				});
			}

			var customUploader = $('#pww_connect_product_configurator').find('#pww-custom-uploader');
			if (customUploader.length > 0) {
				pww_file_uploader(customUploader, {
					'pww_calculation_id': response.data.calculation_id,
					'pww_uploader_id': customUploader.data('pww-uploader-id'),
					'pww_is_custom_uploader': 'yes',
					'pww_step_code': customUploader.data('pww-step-code')
				});
			}
		});
	}

	function pww_connect_product_update_check() {
		var updateCheckerRequest = pww_action_request('product-updates/status', {
			"data": JSON.stringify({
				"product_id": variables.pww_product.id,
			})
		}, 'POST');

		updateCheckerRequest.done((response) => {
			if (!response.status) {
				setTimeout(() => {
					pww_connect_product_update_check();
				}, 2000);
			} else {
				pww_connect_wc_product_init();
			}
		});
	}

	function pwwConnectShowError(message) {
		const blockError = pww_connect_product_configurator.find('.pww-variation-option-errors');
		blockError.removeClass('hide').html(
			`<div class="pww-variation-option-error"><span>${message}</span></div>`
		);
		pww_connect_product_configurator.find('.pww-loader-wrapper').addClass('hide');
	}

	function pwwHandleHooks(hooks)
	{
		$.map(hooks, function(hook){

			var hooks_request = pww_action_request(hook.request.url, [], 'POST')

			hooks_request.done(function (response) {
				if (hook.request.selector) {
					if(response.data.html !== undefined) {
						if (response.data.html == '') {
							$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-variation-options-copies-holder').addClass('hide');	
						} else {
							if (hook.request.action !== undefined) {
								// $('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-copies-options').find('.pww-loader-wrapper').addClass('hide');
								$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-copies-options').find('.pww-loader-wrapper').remove();
								$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').replaceWith(response.data.html);
							} else {
								$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').html(response.data.html);
							}
						}
					} else if ($.isEmptyObject(response.data.prices)) {
						$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-variation-options-copies-old-holder').addClass('hide');
					} else {
						$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').find('[data-pww-variant-code=copies]').each(function() {
							if (response.data.prices[$(this).data('pww-variant-value')]) {
								const price = response.data.prices[$(this).data('pww-variant-value')];
								$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"] [data-pww-variant-value="' + price.quantity + '"] .pww-variation-option-content-right').html('<span class="pww-variation-option-price">' + price.product_price + '</span>');
							} else {
								$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"] [data-pww-variant-value="' + $(this).data('pww-variant-value') + '"]').addClass('hide');
							}
						});
					}

					$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').find('.pww-loader-wrapper').addClass('hide');
				}
			});

		});
	}

	function pww_action_request(endpoint, data = [], method = "POST") {
		if (endpoint.indexOf('/') === 0) {
			endpoint = endpoint.replace(/^\/+/, '');
		}

		var settings = $.extend({}, {
			"url": variables.pww_connect_api_url + endpoint,
			"method": method,
			"timeout": 0,
			"headers": {
				"Content-Type": "application/json",
                "Authorization": "Bearer " + variables.pww_connect_api_key,
				"Domain-Hash": variables.pww_connect_domain_hash,
				"ngrok-skip-browser-warning": "test"
			}
		}, data);
		  
		return $.ajax(settings);
	}

	function pww_ajax_action_request(action, customData, method = "POST") {
		return $.ajax({
			"type": method,
			"url": variables.pww_connect.ajax_url,
			"data": $.extend({}, {
				"action": action,
				"_ajax_nonce": variables.pww_connect.ajax_nonce
			}, customData)
		});
	}

	function pww_uploader(selector, attributes) {
		FilePond.registerPlugin(
			// encodes the file as base64 data
			FilePondPluginFileEncode,
			// validates the size of the file
			FilePondPluginFileValidateSize,
			// corrects mobile image orientation
			FilePondPluginImageExifOrientation,
			// previews dropped images
			FilePondPluginImagePreview,
			// previews dropped pdfs
			// FilePondPluginPdfPreview
		);
	
		const uploaderElement = document.querySelector(selector);
		FilePond.create(uploaderElement, {
			acceptedFileTypes: ['image/jpeg', 'image/png', 'image/jpg', 'application/pdf', 'image/tiff', 'image/tif', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword', 'application/x-zip-compressed', 'application/octet-stream', 'application/postscript'],
			credits: false
		});
	
		var eleme = $(uploaderElement);
	
		var calculation_id = $('input[name=pww-calculation-id]').val();
		if (attributes !== undefined && attributes['calculation_id']) {
			calculation_id = attributes['calculation_id'];
		}
	
		var uploader_id = $('.pww-uploader').data('pww-uploader-id');
		if (attributes !== undefined && attributes['uploader_id']) {
			uploader_id = attributes['uploader_id'];
		}

		FilePond.setOptions($.extend({
			server: {
				process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
					const formData = new FormData();
					formData.append(fieldName, file, file.name);
					if (attributes !== undefined && attributes['cart_item_key']) {
						formData.append('cart_item_key', attributes['cart_item_key']);
					}
					formData.append('pww_calculation_id', calculation_id);
					formData.append('pww_uploader_id', uploader_id);
					formData.append('action', 'pww_product_uploader');
					formData.append('_ajax_nonce', variables.pww_connect.ajax_nonce);
		
					const request = new XMLHttpRequest();
					request.open('POST', variables.pww_connect.ajax_url);
		
					request.upload.onprogress = (e) => {
						progress(e.lengthComputable, e.loaded, e.total);
					};
		
					request.onload = function () {
						if (request.status >= 200 && request.status < 300) {
							var requestResponse = JSON.parse(request.responseText);
		
							if (requestResponse.success) {
								if ($('[data-file-list-id="' + uploader_id + '"]').length > 0) {
									$('[data-file-list-id="' + uploader_id + '"]').html(requestResponse.data.html);
	
									$('[data-files-wrapper="' + uploader_id + '"]').removeClass('hide');
	
									$('.pww-product-uploader[data-opener-id="' + uploader_id + '"]').click();
									$('.pww-uploader[data-uploader-id="' + uploader_id + '"]').html('<div id="pww-uploader-' + uploader_id + '"></div>');
								}
	
								load(requestResponse.responseText);
							} else {
								alert(requestResponse.data.message);
								error(requestResponse.data.message);
							}
						} else {
							// Can call the error method if something is wrong, should exit after
							error('oh no');
						}
					};
		
					request.send(formData);
		
					return {
						abort: () => {
							// This function is entered if the user has tapped the cancel button
							request.abort();
		
							// Let FilePond know the request has been cancelled
							abort();
						},
					};
				}
			},
			stylePanelAspectRatio: 4 / 3, // Nieuwe breedte-hoogteverhouding
  			stylePanelLayout: 'stacked' // Of 'grid' afhankelijk van je voorkeur
		}, variables.uploader.translations, {
			maxFileSize: variables.uploader.max_upload_size
		}));
	}

	function printAppEditor(attributes) {
		// Get handles to the UI elements we'll be using
		const	launchButton = document.getElementById('pww-connect-designer-button'),
				previewDiv = document.getElementById('pww-connect-designer-previews');
				
		// Disable the Launch button until Print.App is ready for use
		launchButton.setAttribute('disabled', 'disabled');

		// Initialize Print.App
		const printAppInstance = new PrintAppClient(attributes);
	
		// Function to run once the app is validated (ready to be used)
		var appReady = () => {
			// Enable the Launch button
			launchButton.removeAttribute('disabled');
			
			// Attach event listener to the button when clicked to show the app
			launchButton.onclick = () => printAppInstance.showApp();
		};

		// Function to run once the user has saved their project
		var projectSaved = (value) => {
			let { data } = value;

			if (data && data.previews && data.previews.length) {
				// Show the preview images
				let html = '';
				for (let i = 0; i < data.previews.length; i++) {
					html += `<img src="${data.previews[i]}">`;
				}
				previewDiv.innerHTML = html;
			}
		};

		// Attach events to the app.
		printAppInstance.addEventListener('app:ready', appReady);
		printAppInstance.addEventListener('app:saved', projectSaved);
	}

	function updateUploaderProgress(currentFile, totalFiles, overallProgress) {
		toggleUploaderProgress(true);

		let message;
		if (overallProgress === 100) {
			message = $('.uploader-progress .progress-msg').data('uploader-done-msg');
		} else {
			const uploaderMsg = $('.uploader-progress').find('.progress-msg').data('uploader-msg');
			message = uploaderMsg ? uploaderMsg.replace('{currentFile}', currentFile).replace('{totalFiles}', totalFiles) : '';
		}

		$('.uploader-progress .progress-msg').html(message);

		$('.uploader-progress .progress-bar .progress-bar-fill').css('width', overallProgress + '%');
		$('.uploader-progress .progress-bar .progress-bar-fill').html(overallProgress + '%');
	}

	function toggleUploaderProgress(show) {
		if (show) {
			$('.uploader-progress').removeClass('hide');
		} else {
			$('.uploader-progress').addClass('hide');
		}
	}

	/**
	 * Refreshes the uploader with the given calculation ID and uploader ID.
	 *
	 * @param {number} calculationId - The ID of the calculation to refresh.
	 * @param {number} uploaderId - The ID of the uploader to refresh.
	 * @returns {Promise<boolean>} - A promise that resolves to true if the uploader was refreshed successfully, otherwise false.
	 *
	 * @async
	 * @function
	 */
	function refreshUploader(calculationId, uploaderId) {
		return new Promise(async (resolve, reject) => {
			try {
				const response = await pww_action_request(`uploader/${uploaderId}/refresh`, {
					data: JSON.stringify({
						calculation_id: calculationId,
					})
				}, 'POST');

				if (response && response.status) {
					// Checking if there are errors
					if (response.data.hasErrors && response.data.hasErrors === true) {
						// API call to retrieve the error message(s)
						await pww_action_request(`calculations/${calculationId}/alerts`, {
							'data': JSON.stringify({
								'position': 'after_upload',
							}),
						}, 'POST')
							.then((errorResponse) => {
								if (errorResponse && errorResponse.data && errorResponse.data.html) {
									$('#pww-product-uploaded-files-alerts').html(errorResponse.data.html);
								}
							})
							.catch((error) => {
								console.error('Error fetching error messages:', error);
							});
					} else {
						// Set html empty
						$('#pww-product-uploaded-files-alerts').html('');
					}

					if (response.data.reload && response.data.reload === true) {
						pww_connect_wc_product_init();							
					} else {
						let showUploadedFiles = true;
	
						if (response.data.editor) {
							// When PrintApp is given, show the editor
							if (response.data.finished && response.data.finished === true) {
								showUploadedFiles = true;
							} else {
								showUploadedFiles = false;
	
								initializePrintAppEditor(calculationId, response.data);
	
								this.removeFile(file);
							}
						}
	
						if (showUploadedFiles === true) {
							$('.pww-product-uploaded-files').removeClass('hide');
							$('.pww-product-uploaded-files-list').html(response.data.files.html);
						}
					}

					resolve(true);
				} else {
					console.error('Error refreshing uploader:', response.message);
					resolve(false);
				}
			} catch (error) {
				console.error('Error refreshing uploader:', error);
				reject(false);
			}
		});
	}

	function pww_file_uploader(selector, attributes) {
		if (!$(selector).hasClass('dropzone')) {
			// Maak een lokale kopie van attributes om de scope te behouden
			const localAttributes = { ...attributes };

			$(selector).dropzone({
				url: "https://files.print-assets.com/upload",
				paramName: "files",
				uploadMultiple: true,
				parallelUploads: 10,
				maxFilesize: variables.uploader.max_upload_size,
				acceptedFiles: ".png,.jpg,.jpeg,.gif,.pdf,.zip",
				dictDefaultMessage: "Sleep de bestanden hierheen om te uploaden",
				headers: { "X-Requested-With": "XMLHttpRequest" },
				method: "post",
				init: function() {
					let pendingTasks = 0;
					let jobsAreDone = false;

					this.on("successmultiple", function(files, response) {
						const calculationId = response.calculation_id;
						let intervalId;

						const checkTaskStatus = () => {
							fetch(`https://files.print-assets.com/progress/${calculationId}`)
								.then(res => res.json())
								.then(data => {
									updateUploaderProgress(data.completed_files, data.total_files, data.overall_progress);

									if (data.status === "completed") {
										// Clear the interval, stop polling
										clearInterval(intervalId);

										// Reload the uploader
										refreshUploader(calculationId, localAttributes.pww_uploader_id)
											.then(() => {
												// Remove all files from the uploader
												this.removeAllFiles();

												// Hide the progress bar
												setTimeout(() => {
													toggleUploaderProgress(false);
												}, 2000);
											})
											.catch(error => {	
												console.error('Error refreshing uploader:', error);
											});
									}
								})
								.catch(error => {
									console.error('Error checking task status:', error);
									clearInterval(intervalId); // Stop polling bij een fout
								});
						};

						intervalId = setInterval(checkTaskStatus, 500);
						checkTaskStatus(); // Eerste check meteen uitvoeren
					});
					this.on("sendingmultiple", function(files, xhr, formData) {
						pendingTasks = this.files.length;

						formData.append("calculation_id", localAttributes.pww_calculation_id);
						formData.append("custom_path", "calculations/" + localAttributes.pww_calculation_id);

						var customUploader = $('#pww_connect_product_configurator').find('#pww-custom-uploader');
						if (customUploader.length > 0) {
							formData.append("callback", customUploader.data('pww-uploader-callback'));
						} else {
							formData.append("callback", "openEditor");
						}

						if (attributes.pww_is_custom_uploader !== undefined) {
							pww_connect_product_configurator.find('.pww-loader-wrapper').removeClass('hide');
							
							formData.append('custom_uploader', attributes.pww_is_custom_uploader);
						}

						formData.append('total_files', pendingTasks);

						updateUploaderProgress(0, pendingTasks, 0);
					});
					this.on("error", function(file, errorMessage) {
						pendingTasks--; // Verminder de teller, ook als er een fout is
						if (pendingTasks === 0 && localAttributes.pww_is_custom_uploader !== undefined) {
							pww_connect_wc_product_init(); // Start alsnog als dit de laatste was
						}
					});
				}
			});
		}
		
		$(selector).addClass('dropzone');
	}
	
	function pww_workfiles(selector, attributes) {
		var workfilesRequest = pww_action_request('workfiles', {
			"data": JSON.stringify({
				"calculation_id": attributes.pww_calculation_id
			})
		}, 'POST');
		
		workfilesRequest.done(function (response) {
			$(selector).html(response.html);
		});
	}

	function calculateSquareMeters(squareMetersWidth, squareMetersHeight) {	
		// Initialiseer variabelen voor breedte en hoogte
		let width, height;
	
		width = parseFloat(squareMetersWidth);
		height = parseFloat(squareMetersHeight);

		// Controleer of zowel breedte als hoogte zijn ingesteld
		if (width && height) {
			// Bereken de oppervlakte in vierkante centimeters
			let oppervlakte_cm2 = width * height;

			// Omzetten naar vierkante meter (1 vierkante meter = 10.000 vierkante centimeter)
			let squareMeters = oppervlakte_cm2 / 10000;
			
			// Afronden op maximaal 3 decimalen
			let roundedSquareMeters = parseFloat(squareMeters.toFixed(3));

			return roundedSquareMeters;
		}

		return 0;
	}

	function pww_copy_next(element) {
		var ajax_action_btn = element;

		var ajax_action = ajax_action_btn.parent().closest('[data-pww-ajax-action]');
		var loaderCheck = ajax_action.data('pww-skip-loader-check');

		var post_type = 'PUT';
		if (ajax_action.data('pww-ajax-method')) {
			post_type = ajax_action.data('pww-ajax-method');
		}

		var options = {};
		ajax_action.find('[data-pww-ajax-field]').each(function() {
			options[$(this).data('pww-ajax-field')] = $(this).val();
		});

		var variation_block = ajax_action_btn.parent().closest('.pww-variation-block');
		if (loaderCheck === undefined) {
			variation_block.find('.pww-loader-wrapper').removeClass('hide');
			pww_connect_product_configurator.find('.pww-sidebar-shopping-cart .pww-loader-wrapper').removeClass('hide');
		}

		var response = pww_action_request(ajax_action.data('pww-ajax-action'), {
			"data": JSON.stringify(options)
		}, post_type)

		response.done(function (response) {	
			ajax_action.find('.pww-ajax-action-mess').html(response.message);
			  ajax_action.find('.pww-ajax-action-mess').fadeIn();
			ajax_action.find('.pww-ajax-action-mess').delay(3000).fadeOut();

			if (ajax_action.data('pww-result-selector')) {
				if (response.data.quantitiesDropdown !== undefined) {
					var $quatitiesDropdown = $('[data-pww-copy-select="' + response.data.quantitiesDropdown + '"]');
					// $quatitiesDropdown.empty();

					// Placeholder optie behouden
					var placeholderOption = $quatitiesDropdown.find('option[value=""]');

					// Opties behouden en selectbox leegmaken
					$quatitiesDropdown.find('option[value!=""]').remove();

					// Voeg de placeholder-optie terug toe
					$quatitiesDropdown.append(placeholderOption);

					var quantitiesDropdownOptions = response.data.quantitiesDropdownOptions;
					// Sorteer de eigenschappen van quantitiesDropdownOptions op hun bijbehorende waarden
					var sortedOptions = Object.keys(quantitiesDropdownOptions).sort(function(a, b) {
						// Haal de numerieke waarde op na de laatste '-' in elke string
						var numA = parseInt(a.split('-').pop(), 10);
						var numB = parseInt(b.split('-').pop(), 10);
						
						// Sorteer op basis van de numerieke waarde
						return numA - numB;
					});

					// Voeg nieuwe opties toe in gesorteerde volgorde
					sortedOptions.forEach(function(key) {
						$quatitiesDropdown.append($('<option>', {
							value: key,
							text: quantitiesDropdownOptions[key]
						}));
					});
				}

				if (response.data.htmlAppend !== undefined) {
					var $wrapper = $('[data-pww-result-wrapper="' + ajax_action.data('pww-result-selector') + '"]');
					var $targetElement = $wrapper.find('[data-pww-result-wrapper="quantity-search-result"]');

					if ($targetElement.length) {
						$targetElement.before(response.data.html);
					} else {
						$wrapper.append(response.data.html);
					}
				} else {
					$('[data-pww-result-wrapper="' + ajax_action.data('pww-result-selector') + '"]').html(response.data.html);
				}

				if (response.data.removeButton !== undefined && response.data.removeButton === true) {
					ajax_action_btn.remove();
				}

				if (response.data.hooks) {
					$.map(response.data.hooks, function(hook){
						var hooks_request = pww_action_request(hook.request.url, [], 'POST')
			
						hooks_request.done(function (response) {
							if (hook.request.selector) {
								if(response.data.html !== undefined) {
									if (response.data.html == '') {
										$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-variation-options-copies-holder').addClass('hide');	
									} else {
										if (hook.request.action !== undefined) {
											$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-copies-options').find('.pww-loader-wrapper').addClass('hide');
											$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').replaceWith(response.data.html);
										} else {
											$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').html(response.data.html);
										}
									}
								} else if ($.isEmptyObject(response.data.prices)) {
									$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').parent().closest('.pww-variation-options-copies-old-holder').addClass('hide');
								} else {
									$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').find('[data-pww-variant-code=copies]').each(function() {
										if (response.data.prices[element.data('pww-variant-value')]) {
											const price = response.data.prices[element.data('pww-variant-value')];
											$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"] [data-pww-variant-value="' + price.quantity + '"] .pww-variation-option-content-right').html('<span class="pww-variation-option-price">' + price.product_price + '</span>');
										} else {
											$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"] [data-pww-variant-value="' + element.data('pww-variant-value') + '"]').addClass('hide');
										}
									});

									// $.map(response.data.prices, function(price) {
									// 	$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"] [data-pww-variant-value="' + price.quantity + '"] .pww-variation-option-content-right').html('<span class="pww-variation-option-price">' + price.product_price + '</span>');
									// });
								}

								$('[data-' + hook.request.selector + '="' + hook.request.selector_value + '"]').find('.pww-loader-wrapper').addClass('hide');
							}
			
							// variation_block.find('.pww-loader-wrapper').addClass('hide');
							// pww_connect_product_configurator.find('.pww-sidebar-shopping-cart .pww-loader-wrapper').addClass('hide');
						});

					});
				}

				$('[data-pww-result-wrapper="' + ajax_action.data('pww-result-selector') + '"]').find('.pww-uploader-dropzone').each(function() {
					var uploaded_when = 'product_configure';
					if (ajax_action.data('pww-result-selector') == 'pww_connect-print_uploader') {
						uploaded_when = 'uploader';
					}
					
					pww_file_uploader('#' + element.attr('id'), {
						'pww_calculation_id': element.data('pww-calculation-id'),
						'pww_uploader_id': element.data('pww-uploader-id'),
						'pww_uploaded_when' : uploaded_when
					});
				});
			}

			if (response.data.skipLoaderCheck === undefined) {
				variation_block.find('.pww-loader-wrapper').addClass('hide');
				pww_connect_product_configurator.find('.pww-sidebar-shopping-cart .pww-loader-wrapper').addClass('hide');
			}
		});
	}

	function updateProgressBar(progress) {
		const progressBar = document.getElementById('progress-bar');
		progressBar.style.width = progress + '%';
	}

	// Functie voor debounce (wachten tot gebruiker stopt met typen)
    function debounce(func, wait) {
        let timeout;
        return function(...args) {
            const context = this;
            clearTimeout(timeout);
            timeout = setTimeout(() => func.apply(context, args), wait);
        };
    }

    // Productzoekfunctie die API aanroept
    function searchProduct(query, fieldName) {
		// Bouw het post_data object op met de zoekterm en eventueel andere data
		let post_data = {
			searchTerm: query, // De zoekwaarde die de gebruiker invoert
		};

		fetch(variables.pww_connect_api_url + "products/search", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": "Bearer " + variables.pww_connect_api_key,
				"Domain-Hash": variables.pww_connect_domain_hash,
				"ngrok-skip-browser-warning": "test"
			},
			body: JSON.stringify(post_data),
		})
		.then(response => {
			if (!response.ok) {
				throw new Error("Network response was not ok: " + response.statusText);
			}
			return response.json();
		})
		.then(data => {
			console.log('Product search response:', data);
			// Hier kun je de resultaten verwerken (bijv. tonen in een dropdown of lijst)
		})
		.catch(error => {
			console.error('There was a problem with the fetch operation:', error);
		});
    }

	/**
	 * Initializes the PrintAppClient instance and sets up event handling for the PrintApp editor.
	 * 
	 * This function performs the following tasks:
	 * - Initializes the PrintAppClient instance with editor configuration from the response.
	 * - Defines a function to handle events for specific event names and dynamically processes them.
	 * - Defines a callback function for when the application is ready, which binds all events from the response to the PrintAppClient instance.
	 * - Defines a callback function for when the user saves their project in the editor, which calls the editor endpoint and refreshes the uploader.
	 * - Sets up event listeners for 'app:ready' and 'app:saved' events.
	 */
	function initializePrintAppEditor(calculationId, configData)
	{
		// Initialize the PrintAppClient instance with editor configuration from the response
		const printAppInstance = new PrintAppClient(configData.editor);
	
		/**
		 * Handles the events for a specific event name (e.g., "page:rendered").
		 * Iterates over the sub-events and dynamically processes them.
		 *
		 * @param {string} eventName - The name of the event (e.g., "page:rendered").
		 * @param {Object} events - The list of sub-events to handle for the given event name.
		 */
		const handleEvents = (eventName, events) => {
			// Iterate through all sub-event types (e.g., "page:update", "photo:load")
			Object.keys(events).forEach((eventType) => {
				const eventData = events[eventType];
	
				if (Array.isArray(eventData)) {
					// If the event data is an array, process each item (e.g., multiple page updates)
					eventData.forEach((update) => {
						printAppInstance.fire(eventType, {
							property: update.propertyName, // Dynamic property name
							value: update.propertyValue, // Dynamic property value
						});
					});
				} else if (typeof eventData === 'object') {
					// If the event data is an object, fire the event directly with its data
					printAppInstance.fire(eventType, eventData);
				}
			});
		};
	
		/**
		 * Callback for when the application is ready.
		 * Dynamically binds all events from the response to the PrintAppClient instance.
		 */
		const appReady = async () => {
			$('body').find('.pww-loader-wrapper').addClass('hide');

			// Display the application interface
			printAppInstance.showApp();
	
			if (configData.events) {
				const events = configData.events; // Retrieve all events from the response
	
				// Iterate over each top-level event name (e.g., "page:rendered")
				Object.keys(events).forEach((eventName) => {
					// Attach an event listener to handle each event dynamically
					printAppInstance.on(eventName, (event) =>
						handleEvents(eventName, events[eventName])
					);
				});
			}
		};
	
		/**
		 * Callback for when the user saves their project in the editor.
		 * This function is called when the user saves their project in the editor.
		 */
		const editorSaved = (value) => {
			const { data } = value;
	
			// Call the editor endpoint with the project ID, so we know which project to load
			pww_action_request(`calculation/${calculationId}/editor`, {
				data: JSON.stringify({ editorId: data.projectId })
			}, 'POST').done(() => {
				// When the editor is saved, we need to refresh the uploader
				refreshUploader(calculationId, configData.uploader_id);
			});
		};
	
		// Wait for the application to be ready before setting up events
		printAppInstance.on('app:ready', appReady);
		printAppInstance.on('app:saved', editorSaved);
	}

    // Voeg event listener toe voor elk input veld met actie
    document.querySelectorAll('input[data-form-action="productSearch"]').forEach(function(input) {
        input.addEventListener('input', debounce(function(event) {
            const query = event.target.value;
            const fieldName = event.target.name;

            if (query.length > 2) {  // Wacht tot er minimaal 3 karakters zijn
                searchProduct(query, fieldName);
            }
        }, 300));  // Debounce tijd van 300ms
    });
});

function pww_action_request(url, options, method) {
	return new Promise((resolve, reject) => {
		const xhr = new XMLHttpRequest();
		xhr.open(method, url);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.onload = () => {
			if (xhr.status >= 200 && xhr.status < 300) {
				resolve(JSON.parse(xhr.responseText)); // Succesvolle respons
			} else {
				reject(new Error('Request failed with status ' + xhr.status)); // Foutafhandeling
			}
		};
		xhr.onerror = () => reject(new Error('Network error'));
		xhr.send(options.data);
	});
}