=== Printwebwinkel Connect ===
Contributors: dwaynehertroys
Requires at least: 6.0
Tested up to: 6.3
Requires PHP: 7.3
Stable tag: 1.0.1
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Ontdek de ultieme oplossing voor naadloze drukwerk verkoop binnen jouw WordPress-website! Bied gepersonaliseerde producten aan, van visitekaartjes tot posters, moeiteloos met WooCommerce.

== Description ==
Of je nu een creatieve professional, ondernemer of drukwerkexpert bent, onze oplossing tilt jouw online verkoop naar een hoger niveau.

Met onze plugin bied je naadloos en geïntegreerd gepersonaliseerde producten aan, variërend van verfijnde visitekaartjes tot opvallende posters, allemaal binnen de vertrouwde omgeving van jouw WordPress-website. Dankzij de perfecte integratie met WooCommerce, een gerenommeerd e-commerce platform, kunnen klanten genieten van een soepele en vertrouwde winkelervaring.

== Installation ==
= Minimum Requirements =

* PHP 7.3 or greater is required (PHP 8.0 or greater is recommended)
* MySQL 5.6 or greater, OR MariaDB version 10.1 or greater, is required

== Changelog ==

= 1.0.1 2023-08-16 =

* Added README

= 1.0.0 2023-08-16 =

* Initial release
