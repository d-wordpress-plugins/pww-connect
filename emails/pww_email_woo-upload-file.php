<?php
/**
 * PWW Connect upload file email
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
} else {
	$greet = sprintf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) );
	
	echo '<p>' . $greet . '</p>';
	echo '<a href="' . $upload_link . '">Bestanden uploaden</a>';
}
?>

<?php
do_action( 'woocommerce_email_footer', $email );
