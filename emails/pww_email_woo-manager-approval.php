<?php
/**
 * PWW Connect manager approval (brand hub) email
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
} else {
	$greet = sprintf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $brandHubManager->display_name ) );
	
	echo '<p>' . $greet . '</p>';

	echo '<p>Een bestelling is geplaatst door ' . $order->get_user()->display_name . ' en vereist goedkeuring voordat we overgaan tot productie.</p>';

	echo '<a href="' . $order_url . '">Bekijk bestelling</a>';
}
?>

<?php
do_action( 'woocommerce_email_footer', $email );
