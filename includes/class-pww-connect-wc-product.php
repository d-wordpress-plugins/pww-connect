<?php

/**
 * WooCommerce Product
 *
 * @since      1.0.0
 * @package    Pww_Connect
 * @subpackage Pww_Connect/includes
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectWcProduct
{
    public function run()
    {

		add_action( 'wp' , array( &$this, 'add_product_body_class' ) );
        add_filter( 'woocommerce_is_purchasable', array( &$this, 'woocommerce_is_purchasable' ) );
		add_filter( 'woocommerce_product_add_to_cart_text', array( &$this, 'add_to_cart_text' ), 30, 2 );
		add_filter( 'woocommerce_product_add_to_cart_url',array( &$this, 'add_to_cart_link' ), 30, 2 );
		add_filter( 'woocommerce_product_supports', array( &$this, 'disable_ajax_cart' ), 10, 3 );
		add_filter( 'woocommerce_get_price_html', array( &$this, 'get_price_html' ), 1000, 2 );
        add_filter( 'woocommerce_product_get_price', array( &$this, 'product_get_price' ), 10, 2);
        add_action( 'woocommerce_single_product_summary', array( &$this, 'add_configure_button' ), 21 );
        add_filter( 'woocommerce_product_tabs', array( &$this, 'woocommerce_product_tabs' ) );

		add_action( 'woocommerce_before_calculate_totals', array( &$this, 'woocommerce_before_calculate_totals' ), 20, 1 );

    }
	
	public function add_product_body_class() {

		add_filter('body_class', function($classes) {
			if($this->pww_product_is_configurable()) {
				$classes[] = 'pww_connect-product';
			}
			
			return $classes;
		});

	}

    public function woocommerce_is_purchasable() {

        if ($this->pww_product_is_configurable()) {
            return true;
        }

        return true;

    }

	public function add_to_cart_text( $text, $product ) {

        if ($this->pww_product_is_configurable()) {
			$text = 'Samenstellen';
		}
		
		return $text;

	}
	
	public function add_to_cart_link( $link, $product ) { 

        if ($this->pww_product_is_configurable()) {
			$link = $product->get_permalink();
		}
		
		return $link;

	}
	
	public function disable_ajax_cart( $value, $feature, $product ) {
		
        if ($this->pww_product_is_configurable()) {
			if ( $feature == 'ajax_add_to_cart' ) $value = false;
		}
		
		return $value;

	}

	public function get_price_html($price, $product)
	{

        if ($this->pww_product_is_configurable()) {
            return '';
        }
		
		return $price;

	}

    public function product_get_price( $price, $product )
    {

        if (!$price) {
            return 0;
        }

        return $price;

    }
	
	public function add_configure_button() {

        if ($this->pww_product_is_configurable()) {
            echo apply_filters( 'pww_connect_product_configure_button', '<div class="pww-config-btn"><button>Samenstellen</button></div>' );
        }

	}

    public function woocommerce_product_tabs( $tabs ) {

        if ($this->pww_product_is_configurable()) {
            $tabs['pww_product_configruator_init'] = array(
                'id'        => 'pww_product_configurator',
                'title'     => 'Samenstellen',
                'priority'  => 1,
                'callback'  => array( $this, 'pww_connect_wc_product_tab_render' )
            );
        }

        return $tabs;

    }

    public function pww_connect_wc_product_tab_render() {

        echo '<div class="pww-product-not-available hide"><span>Helaas is het product op het moment niet beschikbaar.</span></div>';
        echo '<div id="pww_connect_product_configurator" data-wc-product-id="123" data-pww-product-id="123123"></div>';

    }

    public function pww_product_uploader() {

        global $woocommerce;

        check_ajax_referer('pww-connect-nonce');

        if (isset($_POST['pww_calculation_id'])) {
            $allowedFileTypes = ['image/jpeg', 'image/png', 'image/jpg', 'application/pdf', 'image/JPEG', 'image/PNG', 'image/JPG', 'application/zip', 'application/x-zip-compressed', 'application/octet-stream', 'application/postscript'];

            foreach ($_FILES as $key => $file) {
                if (in_array($file['type'], $allowedFileTypes)) {
                    $path_array  = wp_upload_dir();
                    $path = $path_array['path'] . '/print-files/';
			
                    if(!file_exists($path)){
                        mkdir($path, 0755, true);
                    }

                    $file_name = $_POST['pww_calculation_id'] . '-' . date('YmdHis') . '-' . str_replace(' ', '-', sanitize_text_field($file['name']));
                    $final_target = $path . $file_name;

                    $uploaded_files = [];
                    if (move_uploaded_file($file['tmp_name'], $final_target)) {
                        array_push($uploaded_files, [
                            'file_name' => sanitize_text_field($file['name']),
                            'file_path' => $path_array['url'] . '/print-files/' . $file_name,
                            'file_type' => $file['type'],
                        ]);
						
						$uploaded_by = 'customer';
						$uploaded_when = 'product_configure';
						
						if (isset($_POST['pww_uploaded_when'])) {
							$uploaded_when = $_POST['pww_uploaded_when'];
						} elseif (isset($_POST['cart_item_key'])) {
							$uploaded_when = 'cart';
						}

                        $postData = (array) [
                            'files' => $uploaded_files,
                            'uploaded_by' => $uploaded_by,
                            'uploaded_when' => $uploaded_when
                        ];

                        if (in_array($file['type'], ['application/zip', 'application/x-zip-compressed'])) {
                            $unzip = unzip_file($final_target, $unzipPath = $path . $_POST['pww_uploader_id']);

                            if ($unzip) {
                                $zip_files = [];
                                $unzippedFiles = list_files($unzipPath);
                                foreach ($unzippedFiles as $unzippedFileKey => $unzippedFile) {
                                    // Zoek de positie van "/wp-content" in de string
                                    $wp_content_pos = strpos($unzippedFile, '/wp-content');
    
                                    // Als "/wp-content" gevonden wordt, haal alles voor en inclusief "/wp-content" weg
                                    if ($wp_content_pos !== false) {
                                        $file_url = substr($unzippedFile, $wp_content_pos);
                                    } else {
                                        // Als "/wp-content" niet gevonden wordt, behoud de oorspronkelijke string
                                        $file_url = $unzippedFile;
                                    }

                                    $fileUrl = home_url() . $file_url;
                                    $fileType = pathinfo($fileUrl, PATHINFO_EXTENSION);

                                    array_push($zip_files, [
                                        'file_name' => sanitize_text_field(basename($fileUrl)),
                                        'file_path' => $fileUrl,
                                        'file_type' => $fileType,
                                    ]);
                                }

                                $postData['zip_files'] = $zip_files;
                            }
                        }
						
                        // upload file to uploader using the PWW API
                        $request = wp_remote_post( PWW_CONNECT_API_URL . 'uploader/' . $_POST['pww_uploader_id'] . '/files', array(
                            'timeout' => 60,
                            'body' => json_encode( $postData ),
                            'headers' => array(
                                'Authorization' => 'Bearer ' . get_option('pww_connect-api_token'),
                                'Content-Type' => 'application/json'
                            )
                        ) );
						
                        $response = json_decode(wp_remote_retrieve_body($request));

                        if (isset($_POST['cart_item_key'])) {
                            $woocommerce->cart->cart_contents[$_POST['cart_item_key']]['pww_connect']['uploader']['files'] = $response->data->linked;
                            $woocommerce->cart->set_session();
                        }
                        
                        if ($response->status) {
                            return wp_send_json_success($response->data);
                        }

                        return wp_send_json_error([
                            'message' => 'Er is iets misgegaan tijdens het uploaden, probeer het opnieuw.'
                        ]);
                    }
                } else {
                    return wp_send_json_error([
                        'message' => 'Upload een geldig bestandstype voor dit product. Geldige bestandstypen zijn: ' . implode(',', $allowedFileTypes),
						'file_type' => $file['type']
                    ]);
                }
            }


        }

    }
	
	public function pww_connect_file_remove() {

        global $woocommerce;
		
		$request = wp_remote_request( PWW_CONNECT_API_URL . 'uploader/' . $_POST['uploader_id'] . '/files/' . $_POST['file_id'], array(
			'method'  => 'DELETE',
			'headers' => array(
				'Content-Type' => 'application/json',
				'Authorization' => 'Bearer ' . get_option('pww_connect-api_token'),
			),
		) );

        $response = json_decode(wp_remote_retrieve_body($request));

        $woocommerce->cart->cart_contents[$_POST['cart_item_key']]['pww_connect']['uploader']['files'] = $response->data->linked;
        $woocommerce->cart->set_session();

        wp_send_json_success($response->data);

    }

    public function pww_connect_file_preview_save()
    {
        $uploaded_urls = [];

        if (isset($_POST['previews'])) {
            $path_array = wp_upload_dir();
            $path = $path_array['path'] . '/print-files/';
            $url_base = $path_array['url'] . '/print-files/';
        
            if (!file_exists($path)) {
                mkdir($path, 0755, true);
            }
        
            foreach ($_POST['previews'] as $preview) {
                if (isset($preview['url']) && str_starts_with($preview['url'], 'data:image/')) {
                    // Extract base64 encoded string from the data URL
                    $data = explode(',', $preview['url'], 2);
        
                    if (count($data) === 2) {
                        $base64String = $data[1];
        
                        // Decode the base64 string
                        $imageData = base64_decode($base64String);
        
                        if ($imageData !== false) {
                            // Generate a file name based on specific logic
                            $file_name = $_POST['pww_calculation_id'] . '-' . date('YmdHis') . '-image.png';
                            $final_target = $path . $file_name;
        
                            // Save the decoded image data to the file
                            if (file_put_contents($final_target, $imageData)) {
                                // Add the file URL to the uploaded URLs array
                                $uploaded_urls[] = $url_base . $file_name;
                            }
                        }
                    }
                }
            }
        }

        wp_send_json_success([
            'preview_urls' => $uploaded_urls,
        ]);
    }
	
	public function woocommerce_before_calculate_totals( $cart ) {

		foreach( $cart->cart_contents as $item ) {
			if( isset( $item['pww_connect'] ) && isset($item['pww_connect']['price']) ) {
                $price = $item['pww_connect']['price'] / $item['pww_connect']['quantity'];

				$item['data']->set_price( ( $price ) );
			}
		}

	}

    public function pww_product_is_configurable($product_id = null) {

        if (is_null($product_id)) {
			$product_id = get_the_id();
        }

        if (get_post_type( $product_id ) != 'product') {
            return false;
        }

        $productData = wc_get_product( $product_id );

        if ($productData->get_meta( '_pww_connect-product_id' )) {
            return true;
        }

        return false;

    }
}