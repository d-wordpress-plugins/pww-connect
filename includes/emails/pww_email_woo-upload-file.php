<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A custom Expedited Order WooCommerce Email class
 *
 * @since 0.1
 * @extends \WC_Email
 */
class PwwConnectWcEmailFileUpload extends WC_Email {

	public function __construct()
	{

        define( 'CUSTOM_TEMPLATE_PATH', PWW_CONNECT_PLUGIN_PATH . '/' );

        $this->customer_email = true;

		$this->id = 'pww_connect_wc_email_upload_file';
		$this->title = '[PWW] Bestand uploaden';
		$this->description = 'U kunt via onderstaande link een bestand uploaden.';

		$this->heading = 'Bestand uploaden';
		$this->subject = 'Bestand uploaden';

		$this->template_html  = 'emails/pww_email_woo-upload-file.php';
		// $this->template_plain = 'emails/plain/woo-upload-file.php';

        $this->template_base = CUSTOM_TEMPLATE_PATH;
		
		parent::__construct();
		
		$this->upload_page_id = $this->get_option('upload_page_id');

	}

	public function trigger( $options )
	{
		
		$this->uploadPageLink = get_permalink($this->upload_page_id);
		if ( ! $this->uploadPageLink )
            return;
		
		$order_id = $options['pww_connect-wc_order']['order_id'];

		if ( ! $order_id )
			return;
		
		$this->object = new WC_Order( $options['pww_connect-wc_order']['order_id'] );

		$order_line = $this->object->get_item($options['pww_connect-wc_order']['order_line_id']);

		$this->find[] = '{order_date}';
		$this->replace[] = date_i18n( wc_date_format(), strtotime( $this->object->get_date_created() ) );

		$this->find[] = '{order_number}';
		$this->replace[] = $this->object->get_order_number();
		
		$this->find[] = '{customer_name}';
		$this->replace[] = $this->object->get_billing_first_name() . ' ' . $this->object->get_billing_last_name();
		
		$this->find[] = '{product_name}';
		$this->replace[] = $order_line->get_name();
		
		$this->uploadPageLink .= '?order-id=' . $this->object->get_id() . '&order-email=' . $this->object->get_billing_email();
	
		$this->find[] = '{upload_link}';
		$this->replace[] = '<a href="' . $this->uploadPageLink . '">Bestand uploaden</a>';

        $this->recipient = $this->object->get_billing_email();

		if ( ! $this->is_enabled() || ! $this->get_recipient() )
			return;

		// woohoo, send the email!
		$sendStatus = $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
		
		if ($sendStatus) {
			$this->object->add_order_note('Bestand uploaden mail verstuurd naar ' . $this->get_recipient());
		}
		
		return $sendStatus;

	}

	public function get_content_html()
	{

		ob_start();

		wc_get_template( $this->template_html, array(
            'order'              => $this->object,
			'upload_link'		 => $this->uploadPageLink,
            'email_heading'      => $this->get_heading(),
            'additional_content' => $this->get_additional_content(),
            'sent_to_admin'      => false,
            'plain_text'         => false,
            'email'              => $this,
        ), 'my-custom-email/', $this->template_base );

		return ob_get_clean();

	}
	
	public function init_form_fields()
	{

		/* translators: %s: list of placeholders */
		$placeholder_text  = sprintf( __( 'Available placeholders: %s', 'woocommerce' ), '<code>' . esc_html( implode( '</code>, <code>', array_keys( $this->placeholders ) ) ) . '</code>' );
		$this->form_fields = array(
			'enabled'            => array(
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable this email notification', 'woocommerce' ),
				'default' => 'yes',
			),
			'subject'            => array(
				'title'       => __( 'Subject', 'woocommerce' ),
				'type'        => 'text',
				'desc_tip'    => true,
				'description' => $placeholder_text,
				'placeholder' => $this->get_default_subject(),
				'default'     => '',
			),
			'heading'            => array(
				'title'       => __( 'Email heading', 'woocommerce' ),
				'type'        => 'text',
				'desc_tip'    => true,
				'description' => $placeholder_text,
				'placeholder' => $this->get_default_heading(),
				'default'     => '',
			),
			'additional_content' => array(
				'title'       => __( 'Additional content', 'woocommerce' ),
				'description' => __( 'Text to appear below the main email content.', 'woocommerce' ) . ' ' . $placeholder_text,
				'css'         => 'width:400px; height: 75px;',
				'placeholder' => __( 'N/A', 'woocommerce' ),
				'type'        => 'textarea',
				'default'     => $this->get_default_additional_content(),
				'desc_tip'    => true,
			),
			'email_type'         => array(
				'title'       => __( 'Email type', 'woocommerce' ),
				'type'        => 'select',
				'description' => __( 'Choose which format of email to send.', 'woocommerce' ),
				'default'     => 'html',
				'class'       => 'email_type wc-enhanced-select',
				'options'     => $this->get_email_type_options(),
				'desc_tip'    => true,
			),
			'upload_page_id'  	=> array(
				'title'       => 'Upload pagina',
				'type'        => 'select',
				'description' => 'Kies de pagina waar de klant heen moet worden gestuurd voor het uploaden van een bestand.',
				'placeholder' => 'Test',
				'default'     => '',
				'class'       => 'wc-enhanced-select',
				'options'     => $this->get_upload_page_options(),
				'desc_tip'    => true,
			),
		);

	}
	
	private function get_upload_page_options()
	{
		
		$pages = [];
		$pages[0] = "Selecteer een pagina";

		foreach (get_pages() as $page) {
			$pages[$page->ID] = $page->post_title;
		}

		return $pages;
		
	}

}