<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A custom Expedited Order WooCommerce Email class
 *
 * @since 0.1
 * @extends \WC_Email
 */
class PwwConnectWcEmailTrackingUrl extends WC_Email {

	public function __construct()
	{

        // define( 'CUSTOM_TEMPLATE_PATH', PWW_CONNECT_PLUGIN_PATH . '/' );

        $this->customer_email = true;

		$this->id = 'pww_connect_wc_email_tracking_url';
		$this->title = '[PWW] Track & Trace';
		$this->description = 'Versturen van verzendinformatie';

		$this->heading = 'Verzendbevestiging';
		$this->subject = 'Verzendbevestiging';

		$this->template_html  = 'emails/pww_email_woo-tracking-url.php';

        $this->template_base = CUSTOM_TEMPLATE_PATH;
		
		parent::__construct();

	}

	public function trigger( $options )
	{
		
		$order_id = $options['pww_connect-wc_order']['order_id'];

		if ( ! $order_id )
			return;
		
		$this->object = new WC_Order( $options['pww_connect-wc_order']['order_id'] );

		$order_line = $this->object->get_item($options['pww_connect-wc_order']['order_line_id']);

		$this->find[] = '{order_date}';
		$this->replace[] = date_i18n( wc_date_format(), strtotime( $this->object->get_date_created() ) );

		$this->find[] = '{order_number}';
		$this->replace[] = $this->object->get_order_number();
		
		$this->find[] = '{customer_name}';
		$this->replace[] = $this->object->get_billing_first_name() . ' ' . $this->object->get_billing_last_name();
		
		$this->find[] = '{product_name}';
		$this->replace[] = $order_line->get_name();
		
		$this->tracking_url = $options['pww_connect-track_and_trace']['link'];
	
		$this->find[] = '{tracking_url}';
		$this->replace[] = '<a href="' . $this->tracking_url . '">Track & Trace</a>';

        $this->recipient = $this->object->get_billing_email();

		if ( ! $this->is_enabled() || ! $this->get_recipient() )
			return;

		// woohoo, send the email!
		$sendStatus = $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
		
		if ($sendStatus) {
			$this->object->add_order_note('Track & Trace mail verstuurd naar ' . $this->get_recipient());
		}
		
		return $sendStatus;

	}

	public function get_content_html()
	{

		ob_start();

		wc_get_template( $this->template_html, array(
            'order'              => $this->object,
			'tracking_url'		 => $this->tracking_url,
            'email_heading'      => $this->get_heading(),
            'additional_content' => $this->get_additional_content(),
            'sent_to_admin'      => false,
            'plain_text'         => false,
            'email'              => $this,
        ), 'my-custom-email/', $this->template_base );

		return ob_get_clean();

	}

}