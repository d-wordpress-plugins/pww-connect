<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A custom Expedited Order WooCommerce Email class
 *
 * @since 0.1
 * @extends \WC_Email
 */
class PwwConnectWcEmailManagerApproval extends WC_Email {

	public function __construct()
	{
        $this->customer_email = true;

		$this->id = 'pww_connect_wc_email_manager_approval';
		$this->title = '[PWW] Manager goedkeuring';
		$this->description = 'Versturen van goedkeuringsmail naar manager (brand hub).';

		$this->heading = 'Goedkeuring nodig voor bestelling';
		$this->subject = 'Goedkeuring nodig voor bestelling';

		$this->template_html  = 'emails/pww_email_woo-manager-approval.php';

        $this->template_base = CUSTOM_TEMPLATE_PATH;
		
		parent::__construct();
	}

	public function trigger( $order_id, $order )
	{
		if ( ! $order_id )
			return;
        
        $brandHubUser = $order->get_meta('brand_hub_data');
        $brandHub = get_option('brand_hub_' . $brandHubUser['id']);

		if ( ! $brandHub['manager_email'])
			return;

		$this->brandHubManager = get_userdata( $brandHub['wp']['manager_id'] );

		$this->object = $order;
		// $order_line = $this->object->get_item($options['pww_connect-wc_order']['order_line_id']);

		$this->find[] = '{order_date}';
		$this->replace[] = date_i18n( wc_date_format(), strtotime( $this->object->get_date_created() ) );

		$this->find[] = '{order_number}';
		$this->replace[] = $this->object->get_order_number();

        $this->find[] = '{user_display_name}';
        $this->replace[] = $this->object->get_user()->display_name;
        $this->find[] = '{user_email}';
        $this->replace[] = $this->object->get_user()->user_email;
		
		$this->order_url = '#';
	
		$this->find[] = '{order_url}';
		$this->replace[] = '<a href="' . $this->order_url . '">Bestelling bekijken</a>';

        $this->recipient = $this->object->get_billing_email();

		if ( ! $this->is_enabled() )
			return;

		$sendStatus = $this->send( $brandHub['manager_email'], $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );

		if ($sendStatus) {
			$this->object->add_order_note('Mail verstuurd naar ' . $brandHub['manager_email']);
		}
		
		return $sendStatus;
	}

	public function get_content_html()
	{

		ob_start();

		wc_get_template( $this->template_html, array(
            'order'              => $this->object,
			'order_url'          => $this->order_url,
			'brandHubManager' 	 => $this->brandHubManager,
            'email_heading'      => $this->get_heading(),
            'additional_content' => $this->get_additional_content(),
            'sent_to_admin'      => false,
            'plain_text'         => false,
            'email'              => $this,
        ), 'my-custom-email/', $this->template_base );

		return ob_get_clean();

	}

}