<?php

/**
 * WooCommerce Cart
 *
 * @since      1.0.0
 * @package    Pww_Connect
 * @subpackage Pww_Connect/includes
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectWcOrder
{
    public function run()
    {

        add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'woocommerce_checkout_create_order_line_item' ), 10, 4 );

//         add_filter( 'woocommerce_order_item_display_meta_key', array( $this, 'woocommerce_order_item_display_meta_key'), 20, 3 );
//         add_filter( 'woocommerce_order_item_display_meta_value', array( $this, 'woocommerce_order_item_display_meta_value'), 20, 3 );
		
		add_filter( 'woocommerce_order_item_get_formatted_meta_data', array( $this, 'woocommerce_order_item_get_formatted_meta_data' ), 10, 2);


    }

    public function woocommerce_checkout_create_order_line_item( $item, $cart_item_key, $values, $order )
    {

		if (is_array($values) && isset($values['pww_connect'])) {
			$item->add_meta_data( 'pww_connect-order_data', json_encode($values['pww_connect']) );
		}

		if ($pww_connect_loyalty_points = WC()->session->get('pww_connect_loyalty_points')) {
			$order->update_meta_data( 'pww_connect-loyalty_points', $pww_connect_loyalty_points );
		}
		
		if ($pww_connect_loyalty_points_redeemed = WC()->session->get('pww_connect_loyalty_points_redeemed')) {
			$order->update_meta_data( 'pww_connect-loyalty_points-redeemed', $pww_connect_loyalty_points_redeemed );
		}

    }
	
	public function woocommerce_order_item_display_meta_key( $display_key, $meta, $item )
	{
		
		if ($display_key == 'pww_connect-order_data') {

		}
		
		echo '<pre>';
		var_dump(
			$display_key,
			$meta, $item
		);
		exit;
	}
	
	public function woocommerce_order_item_display_meta_value( $display_value, $meta, $item )
	{
		echo '<pre>';
		var_dump(
			$display_value,
			$meta, $item
		);
		exit;
	}
	
	public function woocommerce_order_item_get_formatted_meta_data( $formatted_meta, $item )
	{

        foreach ($formatted_meta as $key => $meta) {
			if ($meta->key == 'pww_connect-order_data') {
                $meta->display_key = 'Configuratie';
				$values = json_decode($meta->value);

				$html = '<br>';
				foreach($values->list as $list_item) {
					$html .= '<b>' . $list_item->parent_name . '</b>' . ' - ' . $list_item->option_name . '<br/>';
				}
				
				$meta->display_value = $html;
			} elseif ($meta->key == 'pww_connect-loyalty_points') {
				$meta->display_key = 'Punten';
				$meta->display_value = $meta->value;
			}
		}

        return $formatted_meta;

	}

}