<?php

/**
 * @since      1.0.0
 * @package    Pww_Connect
 * @subpackage Pww_Connect/includes
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectWpAdmin {

	/**
	 * Adds meta links to the plugin in the WP Admin > Plugins screen
	 *
	 * @param array $links
	 * @param string $file
	 *
	 * @return array
	 */
	public function pww_connect_plugin_row_meta( $links, $file ) {

		if ( $file !== 'pww-connect/pww-connect.php' ) {
			return $links;
		}

		$links[] = '<a href="' . PWW_CONNECT_ECOM_SUPPORT_URL . '" target="_blank">Documentatie</a>';
        $links[] = '<a href="' . PWW_CONNECT_ECOM_PORTAL_URL . '" target="_blank">Portal</a>';

		return $links;

	}

}