<?php

/**
 * WooCommerce Cart
 *
 * @since      1.0.0
 * @package    Pww_Connect
 * @subpackage Pww_Connect/includes
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectWcCart
{
    public function run()
    {

        add_filter( 'woocommerce_cart_item_quantity', array( &$this, 'woocommerce_cart_item_quantity' ), 10, 3 );
		add_filter( 'woocommerce_after_cart_item_name', array( $this, 'woocommerce_after_cart_item_name' ), 10, 3 );
		add_filter( 'woocommerce_cart_item_thumbnail', array( $this, 'woocommerce_cart_item_thumbnail' ), 10, 3 );
        add_filter( 'woocommerce_widget_cart_item_quantity', array( &$this, 'woocommerce_widget_cart_item_quantity' ), 10, 3 );
		
	    add_filter( 'woocommerce_add_cart_item', array( $this, 'woocommerce_add_cart_item' ), 10, 3 );
		add_filter( 'woocommerce_add_cart_item_data', array( &$this, 'woocommerce_add_cart_item_data' ), 10, 2 );
        add_filter( 'woocommerce_cart_item_price', array( $this, 'woocommerce_cart_item_price' ), 1000, 3 );

		add_action( 'woocommerce_before_cart', array( $this, 'woocommerce_before_cart' ) );

    }
	
	public function woocommerce_cart_item_quantity( $product_quantity, $cart_item_key, $cart_item ) {
		
		if (is_cart() && isset($cart_item['pww_connect']) && isset($cart_item['pww_connect']['quantity'])) {
			$product_quantity = sprintf( '%2$s <input type="hidden" name="cart[%1$s][qty]" value="%2$s" />', $cart_item_key, $cart_item['pww_connect']['quantity'] );
		}
		
		return $product_quantity;

	}

	public function woocommerce_after_cart_item_name( $cart_item, $cart_item_key ) {

		if(!is_cart()) {
            return '';
		}
		
// 		echo '<pre>';var_dump($cart_item);exit;

		$item_name = '';
		if (isset($cart_item['pww_connect']) && isset($cart_item['pww_connect']['list'])) {
			
			if (isset($cart_item['pww_connect']['layout']) && $cart_item['pww_connect']['layout'] == 'multiple_files') {
				
				$item_name .= '<span class="pww_connect-block-short"><dl class="pww_connect-variation_list">';
				foreach ($cart_item['pww_connect']['list'] as $singleListItem) {
					if (isset($singleListItem['parent_code']) && $singleListItem['parent_code'] == 'urgency') {
						$item_name .= '<dt class="pww_connect-variation_list-name">' . $singleListItem['parent_name'] . ':</dt>';
						$item_name .= '<dd class="pww_connect-variation_list-value">' . $singleListItem['option_name'] . '</dd>';
					} else {
						$item_name .= '<dt class="pww_connect-variation_list-name"></dt>';
						$item_name .= '<dd class="pww_connect-variation_list-value">' . $singleListItem['file_name'] . '</dd>';
					}
				}
				$item_name .= '</dl><a href="#" class="pww_connect-link show-or-hide">Gedetailleerde weergave</a></span>';

				$item_name .= '<div class="pww_connect-variation_list_full pww_connect-variation_list_full_files pww_connect-hide"><dl class="pww_connect-variation_list">';
				foreach ($cart_item['pww_connect']['list'] as $product_key) {
					if (!isset($product_key['file_name'])) {
						continue;
					}

					$item_name .= '<div class="pww_connect-variation_list-file"><span>' . $product_key['file_name'] . '</span><div class="pww_connect-variation_list-file-values">';

					foreach ($product_key['values'] as $value) {
						$item_name .= '<dt class="pww_connect-variation_list-name">' . $value['parent_name'] . ':</dt>';
						$item_name .= '<dd class="pww_connect-variation_list-value">' . $value['option_name'] . '</dd>';
					}

					$item_name .= '</div></div>';
				}
				$item_name .= '</dl><a href="#" class="pww_connect-link show-or-hide">Gedetailleerd verbergen</a></div>';

			} else {
				$count = $cart_item['pww_connect']['list'];
	
				$copies_index = array_search('copies', array_column($cart_item['pww_connect']['list'], 'parent_code'));
				if (!$copies_index) {
					$copies_index = array_search('quantity', array_column($cart_item['pww_connect']['list'], 'parent_code'));
				}
				$urgency_index = array_search('urgency', array_column($cart_item['pww_connect']['list'], 'parent_code'));
				if(!$urgency_index) {
					$urgency_index = array_search('pww_delivery', array_column($cart_item['pww_connect']['list'], 'parent_code'));
				}
				
				if ($copies_index && $urgency_index) {
					$foreach_array = [
						$cart_item['pww_connect']['list'][$copies_index],
						$cart_item['pww_connect']['list'][$urgency_index]
					];
				} else {
					$foreach_array = array_slice($cart_item['pww_connect']['list'], 0, 2);
				}

				if (isset($cart_item['pww_connect']['design_service']) && !empty($cart_item['pww_connect']['design_service'])) {
					$foreach_array[] = [
						'option_name' => 'Ja',
						'parent_name' => $cart_item['pww_connect']['design_service']['title']
					];
				}
	
				$item_name .= '<span class="pww_connect-block-short">';
	
					$item_name .= '<dl class="pww_connect-variation_list">';
					foreach ($foreach_array as $product_key) {
						$item_name .= '<dt class="pww_connect-variation_list-name">' . $product_key['parent_name'] . ':</dt>';
						$item_name .= '<dd class="pww_connect-variation_list-value">' . $product_key['option_name'] . '</dd>';
						// <span class="__break"></span>
					}
					$item_name .= '</dl>';
	
					if ($count > 2) {
						$item_name .= '<a href="#" class="pww_connect-link show-or-hide">Alle opties weergeven</a>';
					}
	
				$item_name .= '</span>';
	
				if ($count > 2) {
					$item_name .= '<div class="pww_connect-variation_list_full pww_connect-hide"><dl class="pww_connect-variation_list">';
					foreach ($cart_item['pww_connect']['list'] as $product_key) {
						$item_name .= '<dt class="pww_connect-variation_list-name">' . $product_key['parent_name'] . ':</dt>';
						$item_name .= '<dd class="pww_connect-variation_list-value">' . $product_key['option_name'] . '</dd>';
					}

					if (isset($cart_item['pww_connect']['design_service']) && !empty($cart_item['pww_connect']['design_service'])) {
						$item_name .= '<dt class="pww_connect-variation_list-name">' . $cart_item['pww_connect']['design_service']['title'] . ':</dt>';
						$item_name .= '<dd class="pww_connect-variation_list-value">Ja</dd>';
					}

					$item_name .= '</dl><a href="#" class="pww_connect-link show-or-hide">Opties verbergen</a></div>';
				}
			}
			
			if (isset($cart_item['pww_connect']['uploader'])) {
				$check = ( !isset($cart_item['pww_connect']['uploader']['files']) || ( isset($cart_item['pww_connect']['uploader']['files']) && empty($cart_item['pww_connect']['uploader']['files']) ) );
				$item_name .= '<div class="pww-product-files-wrap' . ($check ? ' hide' : '') . '" data-files-wrapper="' . $cart_item['pww_connect']['uploader']['id'] . '"><span>Drukbestanden<span><br><div class="pww-product-files-list" data-cart-item-key="' . $cart_item_key . '" data-file-list-id="' . $cart_item['pww_connect']['uploader']['id'] . '">';
				if (isset($cart_item['pww_connect']['uploader']['files']) && !empty($cart_item['pww_connect']['uploader']['files'])) {
					foreach ($cart_item['pww_connect']['uploader']['files'] as $uploader_file) {
						$uploader_file = (object) $uploader_file;

						$item_name .= '<a href="' . $uploader_file->file_url . '" target="_blank">' . $uploader_file->file_name . '</a> - <a href="#" class="pww-file-delete-link" data-uploader-file-id="' . $uploader_file->id . '">verwijderen</a><br>';
					}
				}
                $item_name .= '</div></div>';

                $item_name .= '<div class="pww-product-uploader-wrapper">';
					if (isset($cart_item['pww_connect']['file'])) {
						$item_name .= 'Gekozen bestand: <a href="' . $cart_item['pww_connect']['file']['link'] . '" target="_blank">' . $cart_item['pww_connect']['file']['name'] . '</a>';
					} elseif (isset($cart_item['pww_connect']['uploader']['link']) && !empty($cart_item['pww_connect']['uploader']['link'])) {
						$item_name .= '<a href="' . $cart_item['pww_connect']['uploader']['link'] . '" target="_blank" class="pww-upload-link">Bestand uploaden</a>';
					} else {
						$item_name .= '<a href="#" class="pww-upload-link pww-product-uploader" data-opener-id="' . $cart_item['pww_connect']['uploader']['id'] . '" data-close-text="Klik hier om uploaden te annuleren">Bestand uploaden</a>';
						$item_name .= '<br><span class="pww-product-files-upload pww-opener-box hide">';
					
						$item_name .= '<div id="pww-uploader-' . $cart_item['pww_connect']['uploader']['id'] . '" class="pww-uploader-dropzone" data-pww-calculation-id="' . $cart_item['pww_connect']['calculation_id'] . '" data-pww-uploader-id="' . $cart_item['pww_connect']['uploader']['id'] . '" data-cart-item-key="' . $cart_item_key . '">';
							$item_name .= '<div class="dz-message">Sleep bestanden hier naartoe om te uploaden</div>';
						$item_name .= '</div>';
	
						$item_name .= '</span>';
					}
                $item_name .= '</div>';
			}
			
			if (isset($cart_item['pww_connect']['workfiles']) && !empty($cart_item['pww_connect']['workfiles'])) {
				$item_name .= '<div class="pww-product-workfiles-wrap"><span>Werkbestand:&nbsp;</span><div class="pww-product-workfiles-list">';
				foreach ($cart_item['pww_connect']['workfiles'] as $workfile) {
					$workfile = (object) $workfile;

					$item_name .= '<a href="' . $workfile->url . '" target="_blank">' . $workfile->name . '</a>';
				}
				$item_name .= '</div></div>';
				
			}
			
		}

		echo $item_name;

	}
	
	public function woocommerce_cart_item_thumbnail( $product_image, $cart_item, $cart_item_key ) {

		$attachments = get_post_meta($cart_item['product_id'], '_pww_connect-product_images', true);

		if (isset($attachments) && isset($attachments[0]) && isset($attachments[0]['url'])) {
			$custom_product_image = $attachments[0]['url'];

			return '<img width="300" height="300" src="' . $custom_product_image . '" class="woocommerce-placeholder wp-post-image" alt="Plaatshouder" decoding="async" fetchpriority="high" sizes="(max-width: 300px) 100vw, 300px">';
		}

		return $product_image;

	}

    public function woocommerce_widget_cart_item_quantity( $output, $cart_item, $cart_item_key ) {

        if (isset($cart_item['pww_connect'])) {
            $output = '<span class="quantity">' . $cart_item['pww_connect']['quantity'] . ' stuks voor ' . wc_price($cart_item['line_total']) . '</span>';
        }

        return $output;

    }
	
	public function woocommerce_add_cart_item( $cart_item ) {

        if(isset($cart_item['pww_connect']) && isset($cart_item['pww_connect']['quantity'])) {
            $cart_item['quantity'] = $cart_item['pww_connect']['quantity'];
        }

	    return $cart_item;

    }
	
	public function woocommerce_add_cart_item_data( $cart_item_data, $product_id ) {
		
		// Check if PWW product
		if (1 == 1) {
			// Set unique key for forcing new cart item
			$cart_item_data['unique_key'] = md5( microtime().rand() );

            $request = wp_remote_post( PWW_CONNECT_API_URL . 'calculations/' . $_POST['pww-cart-calculation-id'] . '/cart', array(
                'body' => json_encode( (array) [
                    'include_tax' => wc_prices_include_tax()
                ] ),
                'headers' => array(
                    'Authorization' => 'Bearer ' . get_option('pww_connect-api_token'),
                    'Content-Type' => 'application/json'
                )
            ) );
			
            $response = json_decode(wp_remote_retrieve_body($request), true)['data'];
			
			$cart_item_data['pww_connect'] = $response;
		}

		return $cart_item_data;

	}

    public function woocommerce_cart_item_price( $price, $cart_item, $cart_item_key ) {
        
        if (isset($cart_item['pww_connect']) && isset($cart_item['pww_connect']->price)) {
            $price = wc_price($cart_item['pww_connect']->price / $cart_item['pww_connect']->quantity);
        }

        return $price;

    }

	public function woocommerce_before_cart() {
		$slug = 'pww_connect_message_';

		$session_items = WC()->session->get_session_data();
		$pww_connect_messages = array_filter($session_items, function($key) use ($slug) {
			return strpos($key, $slug) === 0;
		}, ARRAY_FILTER_USE_KEY);

		if ($pww_connect_messages) {
			foreach ($pww_connect_messages as $messageType => $message) {
				WC()->session->__unset( $messageType );

				$messageType = str_replace($slug, '', $messageType);

				wc_print_notice( $message, $messageType );
			}
		}
	}
}