<?php

/**
 * @since      1.0.0
 * @package    Pww_Connect
 * @subpackage Pww_Connect/includes
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectWpService
{

    public function pww_connect_wp_http_request_args($http_args) {
	
		if(isset($http_args['headers'])) {
			if(isset($_SERVER['REMOTE_ADDR'])) {
        		$http_args['headers']['pww-connect-client-ip']  = $_SERVER['REMOTE_ADDR'];
			}
			if(isset($_SERVER['HTTP_HOST'])) {
        		$http_args['headers']['pww-connect-client-url']  = $_SERVER['REMOTE_ADDR'];
			} elseif(isset($_SERVER['SERVER_NAME'])) {
        		$http_args['headers']['pww-connect-client-url']  = $_SERVER['SERVER_NAME'];
			}
		}

        $http_args['headers']['Authorization'] = 'Bearer ' . get_option('pww_connect_auth_token');

        return $http_args;
    }

	public function pww_connect_wp_footer() {
		echo '<div class="pww-config-modal-info modal pww-modal"></div>';
	}

}