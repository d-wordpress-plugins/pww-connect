<?php

/**
 * WooCommerce Email
 *
 * @since      1.0.0
 * @package    Pww_Connect
 * @subpackage Pww_Connect/includes
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectWcEmail
{
    public function run()
    {
		
		add_filter( 'woocommerce_email_classes', array( &$this, 'pww_woocommerce_email_classes' ) );

    }
	
	public function pww_woocommerce_email_classes( $email_classes ) {

		// include our custom email class
		require_once( PWW_CONNECT_PLUGIN_PATH . '/includes/emails/pww_email_woo-upload-file.php' );
		require_once( PWW_CONNECT_PLUGIN_PATH . '/includes/emails/pww_email_woo-tracking-url.php' );
		require_once( PWW_CONNECT_PLUGIN_PATH . '/includes/emails/pww_email_woo-manager-approval.php' );

		// add the email class to the list of email classes that WooCommerce loads
		$email_classes['PwwConnectWcFileUploadEmail'] = new \PwwConnectWcEmailFileUpload();
		$email_classes['PwwConnectWcEmailTrackingUrl'] = new \PwwConnectWcEmailTrackingUrl();
		$email_classes['PwwConnectWcEmailManagerApproval'] = new \PwwConnectWcEmailManagerApproval();
		
		return $email_classes;

	}
}