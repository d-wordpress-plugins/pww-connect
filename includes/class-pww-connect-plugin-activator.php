<?php

class PwwConnectPluginActivator
{
	/**
	 * Plugin activation.
	 *
	 * Check if WooCommerce is enabled.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
    {
		if (!defined( 'WC_VERSION' ) || ! class_exists( 'WooCommerce' )) {
            wp_die( 'Could not be activated. ' . self::woocommerce_not_installed_admin_notices() );
		} else {
			flush_rewrite_rules(true);
		}
	}

	/**
	 * Writing the admin notice when WooCommerce is not enabled.
	 *
	 * @since    1.0.0
	 */
    protected static function woocommerce_not_installed_admin_notices()
    {
        return sprintf(
            '%1$s requires WooCommerce version %2$s or higher installed and active. You can download WooCommerce latest version %3$s OR go back to %4$s.',
            '<strong>' . PWW_CONNECT_PLUGIN_NAME . '</strong>',
            PWW_CONNECT_MIN_WC_VERSION,
            '<strong><a href="https://downloads.wordpress.org/plugin/woocommerce.latest-stable.zip">from here</a></strong>',
            '<strong><a href="' . esc_url( admin_url( 'plugins.php' ) ) . '">plugins page</a></strong>'
        );
    }
}