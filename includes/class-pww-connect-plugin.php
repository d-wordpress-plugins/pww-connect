<?php

/**
 * Core class for Printwebwinkel Connect.
 *
 * @since      1.0.0
 * @package    Pww_Connect
 * @subpackage Pww_Connect/includes
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectPlugin
{
    public function run()
    {

        $this->pww_connect_load_depencies();

        $pww_connect_wp_service = new PwwConnectWpService;
        $pww_connect_wc_service = new PwwConnectWcService;
        $pww_connect_wc_product = new PwwConnectWcProduct;
        $pww_connect_branding_hub = new PwwConnectBrandingHub;
		$pww_connect_wc_cart	= new PwwConnectWcCart;
        $pww_connect_wc_order   = new PwwConnectWcOrder;
		$pww_connect_email	 	= new PwwConnectWcEmail;
		$pww_connect_endpoints 	= new PwwConnectEndpoints;
		
		$pww_connect_uploader 	= new PwwConnectUploader;

		$pww_connect_form    	= new PwwConnectForm;

// 		dit gaat niet goed als we WPML activeren
//         add_filter( 'http_request_args', array( $pww_connect_wp_service, 'pww_connect_wp_http_request_args' ) );
        add_filter( "woocommerce_webhook_http_args", array( $pww_connect_wc_service, 'pww_connect_wc_webhook_http_args' ) );

        add_action( 'wp_footer', array($pww_connect_wp_service, 'pww_connect_wp_footer') );

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 50 );
		
		add_action('template_redirect', function () {
            foreach(WC()->cart->get_cart() as $key => $item) {
				if(isset($item['pww_connect']) && isset($item['pww_connect']['product_id']) && $item['line_subtotal'] == 0) {
                    WC()->cart->remove_cart_item( $key );   
                }
            }
        });

		$pww_connect_wc_service->run();
        $pww_connect_wc_product->run();
        $pww_connect_branding_hub->run();
		$pww_connect_wc_cart->run();
		$pww_connect_wc_order->run();
		$pww_connect_email->run();
		$pww_connect_endpoints->run();
		$pww_connect_uploader->run();
        $pww_connect_form->run();

        if ( wp_doing_ajax() ) {

            add_action('wp_ajax_pww_product_uploader', array( $pww_connect_wc_product, 'pww_product_uploader' ) );
            add_action('wp_ajax_nopriv_pww_product_uploader', array( $pww_connect_wc_product, 'pww_product_uploader' ) );

            add_action('wp_ajax_pww_connect_file_remove', array( $pww_connect_wc_product, 'pww_connect_file_remove' ) );
            add_action('wp_ajax_nopriv_pww_connect_file_remove', array( $pww_connect_wc_product, 'pww_connect_file_remove' ) );

            add_action('wp_ajax_pww_connect_file_preview_save', array( $pww_connect_wc_product, 'pww_connect_file_preview_save' ) );
            add_action('wp_ajax_nopriv_pww_connect_file_preview_save', array( $pww_connect_wc_product, 'pww_connect_file_preview_save' ) );

        } elseif ( is_admin() ) {
            
            $pww_connect_wp_admin = new PwwConnectWpAdmin;

            add_filter( 'plugin_row_meta', array( $pww_connect_wp_admin, 'pww_connect_plugin_row_meta' ), 10, 2 );

        } elseif ( !is_admin() ) {
            # code...
        }
        
    }
	
	public function enqueue_scripts()
	{
        global $wp_version;

        $variables['pww_connect'] = [
            'ajax_url' => admin_url('admin-ajax.php'),
            'ajax_nonce' => wp_create_nonce('pww-connect-nonce'),
            'debugging' => defined('PWW_CONNECT_DEBUGGING') ? PWW_CONNECT_DEBUGGING : null,
        ];

        // && !empty(get_post_meta(get_the_ID(), "_pww_product_id", true))

        if (is_product() || is_cart()) {
            $variables['wp'] = [
                'site_url' => site_url(),
                'wp_version' => $wp_version,
                'wc_version' => WC()->version
            ];

            wp_enqueue_script( 'pww-product-configurator-modal-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js', array('jquery') );
			wp_enqueue_style( 'pww-product-configurator-modal-css', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css' );
            
            wp_enqueue_script( 'pww-product-configurator-jquery-ui-js', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('jquery') );
			wp_enqueue_style( 'pww-product-configurator-jquery-ui-css', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css' );

            wp_enqueue_script( 'pww-product-editor-js', 'https://editor.print.app/js/client.js', array('jquery') );
        }

        $variables['visitor'] = [
            'country' => WC_Geolocation::geolocate_ip()['country'] ?? false,
            // 'browser' => $this->getBrowser()
        ];

        if (is_product()) {
            $product = wc_get_product( get_the_ID() );

            $variables['wc']['price'] = [
                'currency_symbol' => get_woocommerce_currency_symbol(),
                'decimal_separator'  => wc_get_price_decimal_separator(),
                'thousand_separator' => wc_get_price_thousand_separator(),
                'decimals'           => wc_get_price_decimals()
            ];
    
            $variables['wc']['tax'] = [
                'enabled' => wc_tax_enabled(),
                'inc_tax_or_vat_label' => WC()->countries->inc_tax_or_vat(),
                'ex_tax_or_vat_label' => WC()->countries->ex_tax_or_vat(),
				'display_shop' => get_option('woocommerce_tax_display_shop')
            ];

            if ($variables['wc']['tax']['enabled']) {
                $tax_obj = new \WC_Tax();
                $tax_rates_data = $tax_obj->find_rates( array(
                    'country'   => WC()->customer->get_shipping_country() ? WC()->customer->get_shipping_country() : WC()->customer->get_billing_country(),
                    'state'     => WC()->customer->get_shipping_state() ? WC()->customer->get_shipping_state() : WC()->customer->get_billing_state(),
                    'city'      => WC()->customer->get_shipping_city() ? WC()->customer->get_shipping_city() : WC()->customer->get_billing_city(),
                    'postcode'  => WC()->customer->get_shipping_city() ? WC()->customer->get_shipping_city() : WC()->customer->get_billing_city(),
                    'tax_class' => $product->get_tax_class()
                ) );
            
                if(!empty($tax_rates_data)) {
                    $tax_rate = reset($tax_rates_data)['rate'];
                }
    
                $variables['wc']['tax']['rate'] = $tax_rate ?? null;
            }
    
            $variables['wc_product'] = [
                'id'        => $product->get_id(),
                'permalink' => $product->get_permalink()
            ];
			
			$pww_connect_product_id = get_post_meta(get_the_ID(), '_pww_connect-product_id', true);
			if (!$pww_connect_product_id) {
				$pww_connect_product_id = get_post_meta(get_the_ID(), '_pww_connect-branding_hub_product_id', true);
			}
			
			$pww_connect_product_language_id = get_post_meta(get_the_ID(), '_pww_connect-language_id', true);
			
            $variables['pww_product'] = [
                'id' => $pww_connect_product_id,
				'language_id' => $pww_connect_product_language_id,
            ];

            if (is_user_logged_in()) {
                $variables['wp']['user_id'] = get_current_user_id();

                $userBrandHub = get_user_meta( get_current_user_id(), 'brand_hub', true );

                if (is_array($userBrandHub) && isset($userBrandHub['user'])) {
                    $variables['brand_hub'] = [
                        'hub_id' => $userBrandHub['id'],
                        'user_id' => $userBrandHub['user'],
                    ];
                }
            }
        }
		
		$variables['pww_connect_api_url'] = PWW_CONNECT_API_URL;
		$variables['pww_connect_api_key'] = get_option('pww_connect-api_token');
		$variables['pww_connect_domain_hash'] = get_option('pww_connect-domain_hash');

        // if (is_product() || is_cart()) {
            $variables['uploader']['max_upload_size'] = wp_max_upload_size() / 1024 / 1024;
            $variables['uploader']['translations'] = [
                'labelIdle' => 'Sleep bestanden hierheen of klik om te <span class="filepond--label-action"> Bladeren </span>',
                'labelInvalidField' => 'Veld bevat ongeldige bestanden',
                'labelFileWaitingForSize' => 'Wachten op maat',
                'labelFileSizeNotAvailable' => 'Maat niet beschikbaar',
                'labelFileLoading' => 'Bezig met laden',
                'labelFileLoadError' => 'Fout tijdens laden',
                'labelFileProcessing' => 'Uploaden',
                'labelFileProcessingComplete' => 'Upload compleet',
                'labelFileProcessingAborted' => 'Upload geannuleerd',
                'labelFileProcessingError' => 'Fout tijdens uploaden',
                'labelFileProcessingRevertError' => 'Fout tijdens terugzetten',
                'labelFileRemoveError' => 'Fout tijdens verwijderen',
                'labelTapToCancel' => 'tik om te annuleren',
                'labelTapToRetry' => 'tik om opnieuw te proberen',
                'labelTapToUndo' => 'tik om ongedaan te maken',
                'labelButtonRemoveItem' => 'Verwijderen',
                'labelButtonAbortItemLoad' => 'Afbreken',
                'labelButtonRetryItemLoad' => 'Opnieuw proberen',
                'labelButtonAbortItemProcessing' => 'Annuleren',
                'labelButtonUndoItemProcessing' => 'Ongedaan maken',
                'labelButtonRetryItemProcessing' => 'Opnieuw proberen',
                'labelButtonProcessItem' => 'Uploaden',
                'labelMaxFileSizeExceeded' => 'Bestand is te groot',
                'labelMaxFileSize' => 'Maximale bestandsgrootte is ' . $variables['uploader']['max_upload_size'] . 'MB'
            ];
			
			wp_enqueue_script( 'pww-filepond-encode', PWW_CONNECT_PLUGIN_ASSETS . 'js/filepond-plugin-file-encode.min.js', [], PWW_CONNECT_PLUGIN_VERSION );
			wp_enqueue_script( 'pww-filepond-validate', PWW_CONNECT_PLUGIN_ASSETS . 'js/filepond-plugin-file-validate-size.min.js', [], PWW_CONNECT_PLUGIN_VERSION );
			wp_enqueue_script( 'pww-filepond-ext', PWW_CONNECT_PLUGIN_ASSETS . 'js/filepond-plugin-image-exif-orientation.min.js', [], PWW_CONNECT_PLUGIN_VERSION );
			wp_enqueue_script( 'pww-image-preview', PWW_CONNECT_PLUGIN_ASSETS . 'js/filepond-plugin-image-preview.min.js', [], PWW_CONNECT_PLUGIN_VERSION );
			wp_enqueue_script( 'pww-pdf-preview', PWW_CONNECT_PLUGIN_ASSETS . 'js/filepond-plugin-pdf-preview.min.js', [], PWW_CONNECT_PLUGIN_VERSION );

			wp_enqueue_script( 'pww-filepond-js', PWW_CONNECT_PLUGIN_ASSETS . 'js/filepond.min.js', [], PWW_CONNECT_PLUGIN_VERSION );

            wp_enqueue_style( 'pww-filepond-image-preview-css', PWW_CONNECT_PLUGIN_ASSETS . 'css/filepond-plugin-image-preview.min.css', [], PWW_CONNECT_PLUGIN_VERSION );
            wp_enqueue_style( 'pww-filepond-css', PWW_CONNECT_PLUGIN_ASSETS . 'css/filepond.min.css', [], PWW_CONNECT_PLUGIN_VERSION );


            // Dropzone
			wp_enqueue_script( 'pww-dropzone-js', 'https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js' );
            wp_enqueue_style( 'pww-dropzone-css', 'https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.css' );

        // }

        wp_enqueue_script( 'pww-connect', PWW_CONNECT_PLUGIN_ASSETS . 'js/pww-connect.js', ['jquery'], PWW_CONNECT_PLUGIN_VERSION, true );
        wp_localize_script('pww-connect', 'variables', $variables);

        wp_register_style( 'pww-connect', PWW_CONNECT_PLUGIN_ASSETS . 'css/pww-connect.css', array(), PWW_CONNECT_PLUGIN_VERSION );
        wp_enqueue_style( 'pww-connect' );

        wp_enqueue_style( 'configurator-styling', 'https://file.print-assets.com/assets/styling.css', array(), null );
	}

    private function pww_connect_load_depencies()
    {

        $files = apply_filters( 'pww_connect_load_depencies', array(
            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-wp-admin.php',
            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-wp-service.php',

            // PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-api-service.php',

            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-wc-service.php',
            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-wc-product.php',
            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-branding-hub.php',
            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-wc-cart.php',
            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-wc-order.php',
            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-wc-email.php',
            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-endpoints.php',
			
            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-uploader.php',
            PWW_CONNECT_PLUGIN_PATH . '/includes/class-pww-connect-form.php',
        ) );

        foreach ( $files as $local => $file ) {
            if ( file_exists( $file ) ) {
                include $file;
            }
        }

    }

    private function getBrowser() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = 'Unknown';
    
        // Platform detection
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'Linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'Mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'Windows';
        }
    
        // Browser detection using more concise matching
        if (preg_match('/MSIE|Trident/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = 'MSIE';
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = 'Firefox';
        } elseif (preg_match('/Chrome/i', $u_agent) && !preg_match('/Edge/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = 'Chrome';
        } elseif (preg_match('/Safari/i', $u_agent) && !preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = 'Safari';
        } elseif (preg_match('/Opera|OPR/i', $u_agent)) {
            $bname = 'Opera';
            $ub = 'Opera';
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = 'Netscape';
        } else {
            $bname = 'Unknown Browser';
        }
    
        // Extract version using more specific regex for known browsers
        $pattern = '#(?<browser>' . implode('|', [$ub, 'Version']) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        preg_match_all($pattern, $u_agent, $matches);
    
        if (isset($matches['version'][0])) {
            $version = $matches['version'][0];
        }
    
        return [
            'name' => $bname,
            'version' => $version ?: '?',
            'platform' => $platform,
        ];
    }
    
}