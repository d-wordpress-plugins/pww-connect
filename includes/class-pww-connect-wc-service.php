<?php

/**
 * @since      1.0.0
 * @package    Pww_Connect
 * @subpackage Pww_Connect/includes
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectWcService {
	
	public function run() {

		add_action( 'woocommerce_thankyou', array($this, 'pww_connect_woocommerce_thankyou'), 10, 2 );
		
		add_action('add_meta_boxes', function() {
			add_meta_box('custom_box', 'Printwebwinkel', array( $this, 'pww_connect_order_sidebar' ), 'shop_order', 'side', 'high');
		});
		
	}

    public function pww_connect_wc_webhook_http_args($http_args) {
        // $http_args['headers']['PWW-STORE-ID'] = get_option('pww_connect_store_id');
        // $http_args['headers']['Authorization'] = 'Bearer ' . get_option('pww_connect_auth_token');
		
		$http_args['headers']['PWW-CLIENT-ID'] = 1;
		$http_args['headers']['PWW-STORE-ID'] = 1;

        return $http_args;
    }
	
	public function pww_connect_woocommerce_thankyou( $order_id ) {
		$order = wc_get_order($order_id);

		$response = wp_remote_post( PWW_CONNECT_API_URL . 'orders', array(
			'headers' => array(
				'Content-Type' => 'application/json',
				'Authorization' => 'Bearer ' . get_option('pww_connect-api_token')
			),
			'body' => json_encode([
				'order_id' => $order_id,
				'order_number' => $order->get_order_number()
			]),
		));

		if (is_wp_error($response)) {
			$error_message = $response->get_error_message();
			error_log('API-fout: ' . $error_message);
		} else {
			$response = json_decode(wp_remote_retrieve_body($response));
			
			if (isset($response->status)) {
				update_post_meta($order_id, '_pww_connect-order_details', $response);
			}
		}
	}
	
	public function pww_connect_order_sidebar()
	{

		$order_id = get_the_ID();
		$meta_data = get_post_meta($order_id, '_pww_connect-order_details', true);
		
		if ($meta_data) { ?>
		<table>
			<?php if (isset($meta_data->status)) { ?>
			<tr>
				<td><strong>Status</strong></td>
				<td><?php echo $meta_data->status->name ?? ''; ?></td>
			</tr>
			<?php } ?>
			<?php if (isset($meta_data->orderNumber)) { ?>
			<tr>
				<td><strong>Order</strong></td>
				<td><?php echo $meta_data->orderNumber; ?></td>
			</tr>
			<?php } ?>
			<?php if (isset($meta_data->createdAt)) { ?>
			<tr>
				<td><strong>Aangemaakt</strong></td>
				<td><?php echo date_i18n(get_option('date_format'), strtotime($meta_data->createdAt)); ?></td>
			</tr>
			<?php } ?>
			<?php if (isset($meta_data->orderId)) { ?>
			<tr></tr>
			<tr>
				<td colspan="2"><a href="<?php echo PWW_CONNECT_ECOM_PORTAL_URL; ?>orders/<?php echo $meta_data->orderId; ?>" target="_blank">Bekijk binnen Printwebwinkel</a></td>
			</tr>
			<?php } ?>
		</table>
		<?php }
	}

}