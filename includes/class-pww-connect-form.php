<?php

class PwwConnectForm
{

	public function run()
	{
		add_shortcode('pww_api_form', array($this, 'pww_fetch_form_from_api'));
	}

	public function pww_fetch_form_from_api($atts)
	{
        $atts = shortcode_atts([
            'id' => '', // ID moet worden doorgegeven als attribuut
        ], $atts);
    
        if (empty($atts['id'])) {
            return 'Form ID is missing.';
        }

        $response = wp_remote_get(PWW_CONNECT_API_URL . 'forms/' . esc_attr($atts['id']), array(
            'headers' => array(
                'Authorization' => 'Bearer ' . get_option('pww_connect-api_token'),
                'Content-Type' => 'application/json'
            ),
        ));
    
        if (is_wp_error($response)) {
            return 'Error retrieving form.';
        }

        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body, true);

        if (isset($data['data']['html'])) {
            return $data['data']['html'];
        }
    
        return 'No form data found.';
	}

}