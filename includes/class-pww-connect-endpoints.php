<?php

/**
 * @since      1.0.0
 * @package    Pww_Connect
 * @subpackage Pww_Connect/includes
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectEndpoints
{

    public function run()
	{
		
		add_action('rest_api_init', array(&$this, 'init_rest_api_endpoints'));
		
	}
	
	public function init_rest_api_endpoints()
	{

		register_rest_route( 'pww-connect/v1', 'site-languages', array(
			'methods' => 'GET',
			'permission_callback' => '__return_true',
			'callback' => array(&$this, 'pww_connect_get_site_languages'),
		));
		
		register_rest_route( 'pww-connect/v1', 'site-options', array(
			'methods'  => 'POST',
			'callback' => array(&$this, 'pww_connect_save_site_options'),
// 			'permission_callback' => array(&$this, 'pww_connect_permission_check')
			'permission_callback' => '__return_true',
		));
		
		register_rest_route( 'pww-connect/v1', 'categories', array(
			'methods'  => 'POST',
			'callback' => array(&$this, 'pww_connect_categories'),
			'permission_callback' => '__return_true',
		));
		
		register_rest_route( 'pww-connect/v1', 'file-upload', array(
			'methods' => 'POST',
			'callback' => array(&$this, 'pww_connect_file_upload'),
			'permission_callback' => '__return_true',
		));
		
		register_rest_route( 'pww-connect/v1', 'track-and-trace', array(
			'methods' => 'POST',
			'callback' => array(&$this, 'pww_connect_track_and_trace'),
			'permission_callback' => '__return_true',
		));
		
		register_rest_route( 'pww-connect/v1', 'branding-hub/sync', array(
			'methods'  => 'POST',
			'callback' => array(&$this, 'pww_connect_brand_hub_sync'),
			'permission_callback' => '__return_true',
		));
		
		register_rest_route( 'pww-connect/v1', 'branding-hub/categories', array(
			'methods'  => 'POST',
			'callback' => array(&$this, 'pww_connect_brand_hub_categories'),
			'permission_callback' => '__return_true',
		));

		register_meta( 'user',  'brand_hub_id', array(
			'single' => true,
			'type' => 'string',
			'show_in_rest' => array(
				'schema' => array(
					'type' => 'string',
					'default' => true
				)
			),
		));

		register_meta( 'user',  'brand_hub', array(
			'single' => true,
			'type' => 'object',
			'show_in_rest' => array(
				'schema' => array(
					'type'                 => 'object',
					'properties'           => array(
						'id' => array(
							'type' => 'string',
						),
						'role'  => array(
							'type' => 'string',
						),
						'user'  => array(
							'type' => 'string',
						),
						'product_ids'  => array(
							'type' => 'array',
						),
					),
					'additionalProperties' => array(
						'type' => 'string',
					),
				),
			),
		));

		register_meta( 'post',  'brand_hub_restriction', array(
			'single' => true,
			'type' => 'object',
			'show_in_rest' => array(
				'schema' => array(
					'type'                 => 'object',
					'properties'           => array(
						'_bh_access_id' => array(
							'type' => 'string',
						),
						'_bh_restrict_custom_message' => array(
							'type' => 'string',
						),
						'_bh_access_redirect_url' => array(
							'type' => 'string',
						),
					),
					'additionalProperties' => array(
						'type' => 'integer',
					),
				),
			),
		));

		register_meta( 'post',  'brand_hub_id', array(
			'single' => true,
			'type' => 'string',
			'show_in_rest' => array(
				'schema' => array(
					'type' => 'string',
					'default' => true
				)
			),
		));
	}
	
	public function pww_connect_permission_check()
	{

		if (function_exists('WC') && is_callable(array(WC()->session, 'get_customer_id'))) {
			return true;
			$user_id = WC()->session->get_customer_id();

			// Controleer of de gebruiker een geldig ID heeft en machtigingen heeft
			if ($user_id && current_user_can('edit_user', $user_id)) {
				return true; // Toestaan dat de gebruiker de opties beheert
			}
		}

		return false; // De gebruiker heeft geen toestemming
		
	}
	
	public function pww_connect_get_site_languages()
	{

		global $sitepress;

		$active_languages = array();
		$languages = $sitepress->get_active_languages();

		foreach ( $languages as $language ) {
			if($language['active'] == "1") {
				$active_languages[] = array(
					'code' => $language['code'],
					'name' => $language['display_name'],
					'default' => $sitepress->get_default_language() === $language['code'],
				);
			}
		}

		$response = array(
			'languages' => $active_languages,
		);

		return rest_ensure_response( $response );

	}
	
	public function pww_connect_save_site_options($request)
	{
		
		$options = $request->get_json_params();

		$total_updates = 0;
		foreach ($options as $option_slug => $option_value) {
			if (str_contains($option_slug, "pww_connect-") !== false && update_option($option_slug, $option_value)) {
				$total_updates++;
			}
		}
		
// 		echo '<pre>';var_dump($options, $total_updates);exit;
		
		$response = ($total_updates == count($options)) ? ['success' => true] : ['success' => false];
		return $response;

	}
	
	public function pww_connect_categories($request)
	{
		
		$response = $request->get_json_params();
		
		if (isset($response['pww_connect-categories']) && !empty($response['pww_connect-categories'])) {
			foreach ($response['pww_connect-categories'] as $category_data ) {
				$this->create_category_recursive( $category_data );
			}
		}
		
		$categories = get_terms( array(
			'taxonomy'   => 'product_cat',
			'meta_key'   => '_pww_connect_category_id',
			'meta_value' => '',
			'meta_compare' => 'EXISTS',
			'hide_empty' => false, // Toon ook lege categorieën als er geen producten aan zijn gekoppeld
		) );
		
		$db_categories = array();

		foreach ( $categories as $category ) {
			$category_id = $category->term_id;
			$meta_data = get_term_meta( $category_id, '_pww_connect_category_id', true );

			$db_categories[] = array(
				'id' => $category_id,
				'name' => $category->name,
				'slug' => $category->slug,
				'parent' => $category->parent,
				'pww_cat_id' => $meta_data
			);
		}
		
		return [
			'success' => true,
			'categories' => $db_categories
		];
		
	}
	
	/**
	 * Recursive function to create categories and their child categories.
	 */
	function create_category_recursive( $category_data, $parent_id = 0 ) {
		$category_args = array(
			'description' => '',
			'slug'        => sanitize_title( $category_data['name'] ),
			'parent'      => $parent_id,
		);

		// Check if the term already exists
		$existing_term = term_exists( $category_data['name'], 'product_cat', $parent_id );
		if ( $existing_term ) {
			$new_category = array(
				'term_id' => $existing_term['term_id'],
			);
		} else {
			$new_category = wp_insert_term( $category_data['name'], 'product_cat', $category_args );
		}

		if ( ! is_wp_error( $new_category ) ) {
			// Create category meta data if provided.
			if ( isset( $category_data['meta_data'] ) && is_array( $category_data['meta_data'] ) ) {
				foreach ( $category_data['meta_data'] as $meta_data_key => $meta_data_value ) {
					update_term_meta( $new_category['term_id'], $meta_data_key, $meta_data_value );
				}
			}

			// Create child categories if provided.
			if ( isset( $category_data['childs'] ) && is_array( $category_data['childs'] ) ) {
				foreach ( $category_data['childs'] as $child_data ) {
					$this->create_category_recursive( $child_data, $new_category['term_id'] );
				}
			}
		} else {
			echo '<pre>';var_dump($new_category);exit;
		}
	}
	
	public function pww_connect_file_upload( $request )
	{
		
		$options = $request->get_json_params();
		
		$status = WC()->mailer()->emails['PwwConnectWcFileUploadEmail']->trigger( $options );
		
		echo json_encode([
			'status' => $status
		]);
		exit;
		
	}
	
	public function pww_connect_track_and_trace( $request )
	{
		
		$options = $request->get_json_params();
		
		$status = WC()->mailer()->emails['PwwConnectWcEmailTrackingUrl']->trigger( $options );
		
		echo json_encode([
			'status' => $status
		]);
		exit;
		
	}

	public function pww_connect_brand_hub_sync($request)
	{
		$requestBody = $request->get_json_params();

		if (isset($requestBody['brand_hub_data'])) {
			update_option('brand_hub_' . $requestBody['brand_hub_id'], $requestBody['brand_hub_data']);
		}

		echo json_encode([
			'success' => true,
		]);
		exit;
	}

	public function pww_connect_brand_hub_categories( $request )
	{
		$requestBody = $request->get_json_params();

		if (isset($requestBody['brand_hub_data'])) {
			update_option('brand_hub_' . $requestBody['brand_hub_id'], $requestBody['brand_hub_data']);
		}

		$mainCategoryIdInWc = false;

		$linkedCategories = [];

		// Maak de hoofdcategorie voor de branding aan.
		if (isset($requestBody['main_category'])) {
			$mainCategoryIdInWc = $this->pww_connect_brand_hub_categories_handler(
				$requestBody['main_category'],
				$requestBody['restriction']
			);

			$linkedCategories[$requestBody['main_category']['id']] = $mainCategoryIdInWc;
		}

		// Maak de sub categories aan onder de main branding hub categorie
		if ($mainCategoryIdInWc && isset($requestBody['sub_categories']) && !empty($requestBody['sub_categories'])) {
			foreach ($requestBody['sub_categories'] as $subCategory) {
				$subCategoryIdInWc = $this->pww_connect_brand_hub_categories_handler(
					$subCategory,
					$requestBody['restriction'],
					$mainCategoryIdInWc
				);
	
				$linkedCategories[$subCategory['id']] = $subCategoryIdInWc;
			}
		}
		
		echo json_encode([
			'status' => true,
			'categories' => $linkedCategories
		]);
		exit;
	}

	private function pww_connect_brand_hub_categories_handler( $category_data, $restriction_data = [], $parent_id = 0 ) {
		$category_args = array(
			'description' => '',
			'slug'        => sanitize_title( $category_data['name']['nl'] ),
			'parent'      => $parent_id,
		);

		// Check if the term already exists
		$existing_term = term_exists( $category_data['name']['nl'], 'product_cat', $parent_id );

		if ( $existing_term ) {
			$new_category = array(
				'term_id' => $existing_term['term_id'],
			);
		} else {
			$new_category = wp_insert_term( $category_data['name']['nl'], 'product_cat', $category_args );
		}

		if ( ! is_wp_error( $new_category ) ) {
			update_term_meta( $new_category['term_id'], 'brand_hub_category', 1 );

			// Create category meta data if provided.
			if (!empty($restriction_data)) {
				update_term_meta( $new_category['term_id'], 'brand_hub_restriction', $restriction_data );
			}

			return $new_category['term_id'];
		}

		return false;
	}

}