<?php

/**
 * Printwebwinkel Branding Hub
 *
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectBrandingHub
{
	private $turnOff = false;

	/**
	 * @var bool
	 */
	private $isHubUser = false;

	/**
	 * If true then we use individual restrict content options
	 * for post
	 *
	 * @var bool
	 */
	private $singular_page;

	/**
	 * @var bool
	 */
	private $redirect_handler;

	/**
	 * @var bool
	 */
	private $allow_access;

	private $ignore_exclude = false;

	private $user_brand_hub = false,
			$brand_hub_user_settings = [];

	private $restrictedAccessPostTypes = [
		'post' => "1", 
		'page' => "1", 
		'product' => "1"
	];

	private $restrictedAccessTaxonomies = [
		'product_cat' => "1",
	];

	// The key where we store the restriction settings.
	private $metaKey = 'brand_hub_restriction';

	public function run() {}

	public function __construct()
	{
		if (!$this->turnOff) {
			add_action('init', array(&$this, 'brandig_hub_custom_order_status'));
			add_filter('wc_order_statuses', array(&$this, 'add_brandig_hub_custom_order_status_to_order_statuses'));

			add_action('pre_get_posts', array(&$this, 'brandingHubPreGetPosts'), 99, 1);
			add_filter('the_posts', array(&$this, 'brandingHubThePosts'), 99, 2);


			// Callbacks for changing terms query.
			add_action('pre_get_terms', array(&$this, 'pww_exclude_hidden_terms_query'), 99, 1);

			// protect pages for wp_list_pages func
			add_filter('get_pages', array(&$this, 'filter_protected_posts'), 99, 2);

			// Sending an email notification when order get 'manager-approval' status
			add_action('woocommerce_order_status_manager-approval', array( &$this, 'backorder_status_custom_notification' ), 20, 2);

			add_shortcode('brand_hub_user_manage', array($this, 'brand_hub_user_manage'));

			// Check if we can set custom header for the branding.
			if ($this->user_has_brand_hub()) {
				add_action('wp', array(&$this, 'addBrandHubClassToBody'));
				add_action('template_redirect', array(&$this, 'customHomepageContent'));
				add_filter('xts_active_options_presets', array(&$this, 'maybeChangeLayoutId'), 10, 2);

				// Remove cart button from mini-cart
				remove_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10);
				// When add a product to cart, redirect to checkout
				add_action('woocommerce_init', array(&$this, 'brand_hub_add_to_cart_redirect'));

				add_filter('woodmart_ajax_add_to_cart', '__return_false');

				// Change added to cart message
				add_filter('wc_add_to_cart_message_html', array(&$this, 'pww_brand_hub_wc_add_to_cart_message_html'), 10, 3);

				add_filter('woocommerce_checkout_fields' , array(&$this, 'pww_remove_billing_fields_from_checkout'));
				add_filter('woocommerce_ship_to_different_address_checked', '__return_true');

				add_action('woocommerce_checkout_create_order', array(&$this, 'brand_hub_woocommerce_checkout_create_order'), 10, 2);

				add_action('woocommerce_before_edit_account_form', array($this, 'brand_hub_woocommerce_before_edit_account_form'));

				$userRole = $this->get_brand_hub_user_settings('role');
				if (is_string($userRole) && $userRole == 'user') {
					// Disable payment only for the user. Only the manager is allowed to make payments.
					add_filter('woocommerce_cart_needs_payment', '__return_false');
					// Verander de naam van de bestelknop.
					add_filter('woocommerce_order_button_text', array(&$this, 'brand_hub_woocommerce_pay_order_button_text'));
					// Verstuur standaard WC mail.
					add_action('woocommerce_order_status_wc-manager-approval', array(WC(), 'send_transactional_email'), 10, 1);

					add_filter('woocommerce_order_received_verify_known_shoppers', array(&$this, 'pww_brand_hub_woocommerce_order_received_verify_known_shoppers'), 10, 1);
				}

				add_action('woocommerce_before_account_orders', array(&$this, 'pww_brand_hub_woocommerce_before_account_orders'));
				// add_filter('woocommerce_my_account_my_orders_query', array(&$this, 'pww_brand_hub_woocommerce_my_account_my_orders_query'), 10, 1);
				add_filter('woocommerce_get_endpoint_url', array(&$this, 'pww_brand_hub_woocommerce_get_endpoint_url'), 9999, 4);

				add_filter('user_has_cap', array(&$this, 'pww_check_order_accessibility'), 10, 3);

				if ($this->pww_user_is_brand_hub_manager()) {
					add_filter('woocommerce_valid_order_statuses_for_payment', array(&$this, 'pww_filter_valid_order_statuses_for_payment'), 10, 2);
					add_filter('woocommerce_valid_order_statuses_for_payment_complete', array(&$this, 'pww_filter_valid_order_statuses_for_payment'), 10, 2);

					add_action('woocommerce_view_order', array(&$this, 'pww_order_pay_button'));

					add_filter('woocommerce_account_menu_items', array(&$this, 'pww_brand_hub_woocommerce_account_menu_items'), 10, 2);
					add_action('woocommerce_account_brand-hub-users_endpoint', array(&$this, 'pww_brand_hub_users_endpoint'));

					add_action('woocommerce_before_checkout_billing_form', array(&$this, 'pww_brand_hub_woocommerce_before_checkout_billing_form'));

					add_action('woocommerce_order_details_before_order_table', array(&$this, 'pww_brand_hub_woocommerce_order_details_before_order_table'), 10, 1);
				}
			} else {
				add_filter('woocommerce_valid_order_statuses_for_payment', array(&$this, 'pww_filter_valid_order_statuses_for_payment'), 10, 2);
				add_filter('woocommerce_valid_order_statuses_for_payment_complete', array(&$this, 'pww_filter_valid_order_statuses_for_payment'), 10, 2);
			}
		}
	}

	public function pww_brand_hub_woocommerce_order_details_before_order_table($order)
	{
		$orderUserId = $order->get_meta('brand_hub_wc_user_id');

		// Controle of gebruiker over het budget heen gaat met deze
		$amountSpent = $this->getUserAmountSpent($orderUserId);
		$budget = $this->getUserBudget($orderUserId);

		$newAmountSpent = $amountSpent + floatval($order->total);

		if ($newAmountSpent > $budget) {
			wc_get_template(
				'brand-hub/budget-alert.php',
				array(
					'budget' => $budget,
					'overBudget' => $newAmountSpent - $amountSpent,
				),
			'', PWW_CONNECT_PLUGIN_VIEWS);
		}
	}

	private function getUserBudget($user_id = false)
	{
		$meta = get_user_meta($user_id ? $user_id : get_current_user_id(), 'brand_hub', true);

		return floatval($meta['budget']);
	}

	private function getUserAmountSpent($user_id = false)
	{
		$orders_by_user = wc_get_orders([
			'limit' => -1,
			'meta_key' => 'brand_hub_wc_user_id',
			'meta_compare' => '=',
			'meta_value' => $user_id ? $user_id : get_current_user_id(),
		]);

		$amountSpent = floatval(0);
		foreach ($orders_by_user as $order_by_user) {
			$amountSpent += floatval($order_by_user->total);
		}

		return floatval($amountSpent);
	}

	public function brand_hub_woocommerce_before_edit_account_form()
	{
		$user = wp_get_current_user();

		if (isset($user->brand_hub['budget'])) {
			$orders_by_user = wc_get_orders([
				'limit' => -1,
				'meta_key' => 'brand_hub_wc_user_id',
				'meta_compare' => '=',
				'meta_value' => get_current_user_id(),
			]);

			$amountSpent = 0;
			foreach ($orders_by_user as $order_by_user) {
				$amountSpent += floatval($order_by_user->total);
			}

			$amountSpent = 10000;

			$totalBudget = floatval($user->brand_hub['budget']);
			
			$percentage = ($amountSpent / $totalBudget) * 100;
			// Optioneel: afronden op 2 decimalen
			$percentage = round($percentage, 2);

			wc_get_template(
				'brand-hub/budget-bar.php',
				array(
					'amountSpent' => $amountSpent,
					'totalBudget' => $totalBudget,

					'percentage' => $percentage,
				),
			'', PWW_CONNECT_PLUGIN_VIEWS);
		}

	}

	public function pww_brand_hub_woocommerce_before_checkout_billing_form()
	{
		$billing_info = $this->get_user_brand_hub_settings('billing');

		$factuurgegevens_blok = "<div class='brand-hub-billing-details'>{$billing_info['first_name']} {$billing_info['last_name']}<br>
		{$billing_info['company']}<br>
		{$billing_info['address_1']}" . ($billing_info['address_2'] ? ", {$billing_info['address_2']}" : "") . "<br>
		{$billing_info['postcode']} {$billing_info['city']} ({$billing_info['country']})</div>";

		echo $factuurgegevens_blok;

		$shipping_info = $this->get_user_brand_hub_settings('shipping');

		echo "<div class='brand-hub-shipping-details'>
			<span>Verzendgegevens</span>
			<p>{$shipping_info['company']} t.a.v. {$shipping_info['first_name']} {$shipping_info['last_name']}<br>
			{$shipping_info['address_1']}" . ($shipping_info['address_2'] ? ", {$shipping_info['address_2']}" : "") . "<br>
			{$shipping_info['postcode']} {$shipping_info['city']} ({$shipping_info['country']})</p>
		</div>";
	}

	public function pww_brand_hub_woocommerce_order_received_verify_known_shoppers($default)
	{
		global $wp;

		$order = wc_get_order($wp->query_vars['order-received']);

		if ($order && get_current_user_id() == $order->get_meta('brand_hub_wc_user_id')) {
			return false;
		}

		return $defualt;
	}

	public function pww_brand_hub_users_endpoint()
	{
		$user_id = filter_input(INPUT_GET, 'user_id', FILTER_SANITIZE_SPECIAL_CHARS);
		$user_create = filter_input(INPUT_GET, 'user_create', FILTER_SANITIZE_SPECIAL_CHARS);
		
		if ($user_id || $user_create) {
			$user = $user_id ? get_user_by('id', $user_id) : [];

			wc_get_template(
				'brand-hub/user-edit.php',
				array(
					'brand_hub_user' => $user,
					'max_budget' => $this->get_brand_hub_user_settings('budget')
				),
			'', PWW_CONNECT_PLUGIN_VIEWS);
		} else {
			$args = [
				'include' => $this->user_ids_in_same_brand_hub(),
			];
	
			$brand_hub_users = get_users($args);

			$spent_per_user = [];
			foreach ($brand_hub_users as $key => $value) {

				// $q['meta_key'] = 'brand_hub_wc_user_id';
				// $q['meta_compare'] = '=';
				// $q['meta_value'] = get_current_user_id();
				
				// $q['customer'] = $this->get_user_brand_hub_settings('manager_id');

				$orders_by_user = wc_get_orders([
					'customer' => get_current_user_id(),
					'limit' => -1,
					'meta_key' => 'brand_hub_wc_user_id',
					'meta_compare' => '=',
					'meta_value' => $value->id,
				]);

				$total_spent = 0;
				foreach ($orders_by_user as $order_by_user) {
					$total_spent += floatval($order_by_user->total);
				}

				$spent_per_user[$value->id] = $total_spent;

				// $wcUser = new WC_Customer($value->id);
				// $spent_per_user[$value->id] = $wcUser->get_total_spent();
			}

			wc_get_template(
				'brand-hub/users.php',
				array(
					'brand_hub_users' => $brand_hub_users,
					'spent_per_user' => $spent_per_user,
					'max_users' => $this->get_user_brand_hub_settings('max_users') ?? 5
				),
			'', PWW_CONNECT_PLUGIN_VIEWS);
		}
	}

	public function brand_hub_user_manage()
	{
		return 'test';
	}

	public function pww_brand_hub_woocommerce_account_menu_items($items, $endpoints)
	{
		$items['brand-hub-users'] = 'Gebruikers';

		return $items;
	}

	public function pww_brand_hub_wc_add_to_cart_message_html($message, $products, $show_qty)
	{
		$titles = [];

		foreach ($products as $product_id => $quantity) {
			$titles[] = strip_tags( get_the_title( $product_id ) );
		}

		$products_text = wc_format_list_of_items($titles);

		return 'Fantastisch! Je hebt ' . $products_text . ' toegevoegd aan je bestelling! 🎉';
	}

	/**
	 * Function to retrieve other user IDs within the same branding hub.
	 * This function checks if there are other users with the same branding hub as the current user and returns their user IDs.
	 *
	 * @return bool|array Returns an array containing the user IDs located in the same branding hub, or false if there are no users or if the branding hub of the current user is not set.
	 */
	private function user_ids_in_same_brand_hub(): bool | array
	{
		// Retrieve the branding hub ID of the current user.
		$current_user_brand_hub = $this->pww_current_brand_hub_id();

    	// Check if the branding hub of the current user is set.
		if ( isset( $current_user_brand_hub ) && ! empty( $current_user_brand_hub ) ) {
			// Query to retrieve other users with the same branding hub.
			$user_query = new WP_User_Query( array( 'fields' => 'ID', 'meta_key' => 'brand_hub_id', 'meta_value' => $current_user_brand_hub ) );
			$same_brand_hub_users = $user_query->get_results();

        	// Return the found user IDs.
			return $same_brand_hub_users;
		}

    	// Return false if the branding hub of the current user is not set.
		return false;
	}

	/**
	 * Retrieve the current branding hub ID of the user.
	 *
	 * @return null|string Returns the branding hub ID of the current user, or null if it is not set.
	 */
	private function pww_current_brand_hub_id(): null | string
	{
		return get_user_meta( get_current_user_id(), 'brand_hub_id', true );
	}

	private function pww_current_brand_hub_user_id(): null | string
	{
		return $this->get_brand_hub_user_settings('user');
	}
	
	/**
	 * Checks if the current user has access to view or pay for the order based on the brand hub.
	 *
	 * @param array   $allcaps An array of all the capabilities for the user.
	 * @param array   $caps    Required capabilities or roles.
	 * @param array   $args    Arguments typically passed to the capability-checking functions.
	 * @return array  The modified array of capabilities.
	 */
	public function pww_check_order_accessibility($allcaps, $caps, $args)
	{
		if (isset($caps[0])) {
			if (isset($args[2])) {
				$order = wc_get_order($args[2]);

				if ($order && get_current_user_id() == $order->get_meta('brand_hub_wc_user_id')) {
					switch ($caps[0]) {
						case 'view_order':
							$allcaps['view_order'] = true;
							break;
						case 'pay_for_order':
							$allcaps['pay_for_order'] = true;
							break;
					}
				}
			}
		}

		return $allcaps;
	}

	/**
	 * Generates and displays the pay button for a specific order.
	 *
	 * @param int $order_id The ID of the order for which the pay button is generated.
	 * 
	 * @return void This function doesn't return a value. It echoes the HTML output for the pay button.
	 */
	public function pww_order_pay_button($order_id)
	{
		// Get an instance of the `WC_Order` Object
		$order = wc_get_order($order_id);

		// Check if the order status is "manager-approval"
		if ($order->get_status() == "manager-approval") {
			// Display the pay button with a link to the checkout payment URL
			printf('<a class="button pay" href="%s">%s</a>',
				$order->get_checkout_payment_url(), __("Pay for this order", "woocommerce")
			);
		}
	}

	/**
	 * Filters the list of valid order statuses for payment to include "manager-approval".
	 *
	 * @param array $array The array of valid order statuses.
	 * 
	 * @return array  The modified array of valid order statuses.
	 */
	public function pww_filter_valid_order_statuses_for_payment($statuses)
	{
		$statuses[] = 'manager-approval';

		return $statuses;
	}

	/**
	 * Modifies the query for retrieving orders based on user's brand hub.
	 *
	 * @param WP_Query $q The WP_Query object representing the query.
	 * @return WP_Query The modified WP_Query object.
	 */
	public function pww_brand_hub_woocommerce_my_account_my_orders_query($q)
	{
		if (!$this->pww_user_is_brand_hub_manager()) {
			$q['meta_key'] = 'brand_hub_wc_user_id';
			$q['meta_compare'] = '=';
			$q['meta_value'] = get_current_user_id();
			
			$q['customer'] = $this->get_user_brand_hub_settings('manager_id');
		}

		if (isset($_GET['status']) && !empty($_GET['status'])) {
			$q['status'] = array($_GET['status']);
		}

		return $q;
	}

	/**
	 * Checks if the current user is a brand hub manager.
	 *
	 * @return bool Returns true if the current user is a brand hub manager, otherwise false.
	 */
	private function pww_user_is_brand_hub_manager(): bool
	{
		return $this->get_brand_hub_user_settings('role') == 'manager';
	}

	/**
	 * Removes billing and shipping fields from the checkout page.
	 *
	 * @param array $fields The array of billing and shipping fields.
	 * @return array The modified array with empty billing and shipping fields.
	 */
	function pww_remove_billing_fields_from_checkout($fields)
	{
		$fields['billing'] = array();
		$fields['shipping'] = array();

		return $fields;
	}

	/**
	 * Generate status filter links before account orders in WooCommerce.
	 *
	 * @param bool $has_orders Flag indicating if the user has orders.
	 */
	public function pww_brand_hub_woocommerce_before_account_orders($has_orders)
	{
		// Check if user has orders
		if ($has_orders) {
			echo '<span>Filter op:</span><div>';
	
        	// Array to store status links
			$status_links = [];
	
			// Generate link for all statuses
			$customer_orders = 0; // Initialize total customer orders
			$all_status_link = isset($_GET['status']) && !empty($_GET['status']) ? '<a href="' . remove_query_arg('status') . '">Alle statussen (%d)</a>' : '<b>Alle statussen (%d)</b>';
			$status_links[] = sprintf($all_status_link, $customer_orders);
	
        	// Loop through all statuses
			foreach (wc_get_order_statuses() as $slug => $name) {
            	// Count orders for this status
				$status_orders = count(wc_get_orders(['status' => $slug, 'customer' => get_current_user_id(), 'limit' => -1]));
	
            	// If there are orders for this status, add the link to the array
				if ($status_orders > 0) {
					$link = sprintf('<a href="%s">%s (%d)</a>', add_query_arg('status', $slug, wc_get_endpoint_url('orders')), $name, $status_orders);
	
					// Check if the current status is selected
					$status_links[] = ($_GET['status'] ?? '') === $slug ? '<b>' . $link . '</b>' : $link;
				}
	
				// Update total customer orders
				$customer_orders += $status_orders;
			}

			// Update link for all statuses with the total count
			$status_links[0] = sprintf($all_status_link, $customer_orders);
	
        	// Add all status links to the output
			echo implode('<span class="delimit"> - </span>', $status_links);
	
			echo '</div>';
		}
	}


	// OLD CODE, NEEDS TO BE CHECKED

	public function pww_brand_hub_woocommerce_get_endpoint_url($url, $endpoint, $value, $permalink)
	{
		if ('orders' == $endpoint && isset($_GET['status']) && !empty($_GET['status'])) {
			return add_query_arg('status', $_GET['status'], $url);
		}

		// if ($endpoint === 'my-courses') {
		// 	// TODO: Zorgen dat je een pagina kan aanmaken voor alle brand hubs voor het instellen van een beheer.
		// 	$url = get_post_permalink(644);
		// }

		return $url;
	}

	public function backorder_status_custom_notification( $order_id, $order )
	{
		WC()->mailer()->emails['PwwConnectWcEmailManagerApproval']->trigger( $order_id, $order );
	}

	public function brand_hub_woocommerce_checkout_create_order($order, $data)
	{
		// Set the billing address
		$order->set_address($this->get_user_brand_hub_settings('billing'), 'billing');
		// Set the shipping address
		$order->set_address($this->get_user_brand_hub_settings('shipping'), 'shipping');

		// Store brand hub data to the order
		$order->update_meta_data('brand_hub_data', get_user_meta(get_current_user_id(), 'brand_hub', true));
		$order->update_meta_data('brand_hub_user_id', $this->get_brand_hub_user_settings('user'));
		$order->update_meta_data('brand_hub_wc_user_id', get_current_user_id());

		if (!$this->pww_user_is_brand_hub_manager()) {
			$order->set_customer_id($this->get_user_brand_hub_settings('manager_id'));
	
			if(!$order->has_status('manager-approval')) {
				$order->set_status( 'manager-approval', 'Order wacht op goedkeuring van beheerder.' );
			}
		}
	}

	function brand_hub_add_to_cart_redirect() {
		if ( version_compare( WC_VERSION, '3.0.0', '<' ) ) {
			add_filter( 'add_to_cart_redirect', function() {
				return wc_get_checkout_url();
			} );
		} else {
			add_filter( 'woocommerce_add_to_cart_redirect', function() {
				return wc_get_checkout_url();
			} );
		}
	}

	public function brand_hub_woocommerce_pay_order_button_text( $text )
	{
		return 'Bestelling doorsturen naar beheerder';
	}

	/**
	 * Add custom order status to WooCommerce
	 */
	public function brandig_hub_custom_order_status()
	{
		$this->save_branding_hub_user();

		add_rewrite_endpoint( 'brand-hub-users', EP_PAGES );

		register_post_status('wc-manager-approval', array(
			'label' => 'Wachtend op managergoedkeuring',
			'public' => true,
			'exclude_from_search' => false,
			'show_in_admin_all_list' => true,
			'show_in_admin_status_list' => true,
			'label_count' => _n_noop('Wachtend op managergoedkeuring <span class="count">(%s)</span>', 'Wachtend op managergoedkeuring <span class="count">(%s)</span>')
		));
	}

	private function save_branding_hub_user()
	{
		$user_id = filter_input(INPUT_GET, 'user_id', FILTER_SANITIZE_SPECIAL_CHARS);
		$user_create = filter_input(INPUT_GET, 'user_create', FILTER_SANITIZE_SPECIAL_CHARS);

		if (($user_id || $user_create) && isset($_POST) && isset($_POST['save_hub_user_details'])) {
			$brandHubId = $this->pww_current_brand_hub_id();

			if ($user_id) {
				$userBrandHub = get_user_meta( $user_id, 'brand_hub', true );
				$url = 'brand-hub/' . $brandHubId . '/users/' . $userBrandHub['user'] . '/update';

				$postData = [
					'first_name' => sanitize_text_field($_POST['account_first_name']),
					'last_name' => sanitize_text_field($_POST['account_last_name']),
					'email' => sanitize_email($_POST['account_email']),
					'budget' => sanitize_text_field($_POST['account_budget']),
				];
			} else {
				$url = 'brand-hub/' . $brandHubId . '/users/create';

				$postData = [
					'first_name' => sanitize_text_field($_POST['account_first_name']),
					'last_name' => sanitize_text_field($_POST['account_last_name']),
					'email' => sanitize_email($_POST['account_email']),

					'budget' => sanitize_text_field($_POST['account_budget']),
					'username' => sanitize_text_field($_POST['account_username']),
					'password' => sanitize_text_field($_POST['account_password']),
				];
			}

			$request = wp_remote_post(PWW_CONNECT_API_URL . $url, array(
				'headers' => array(
					'Authorization' => 'Bearer ' . get_option('pww_connect-api_token'),
					'Content-Type' => 'application/json'
				),
				'body' => json_encode((array) $postData)
			));

			$response = json_decode(wp_remote_retrieve_body($request), true);

			if (isset($response['message'])) {
				wc_add_notice($response['message'], ($response['success'] ? 'success' : 'error'));
			}

			wp_redirect(wc_get_account_endpoint_url('brand-hub-users'));
            exit();
		}
	}

	/**
	 * Add custom order status to WooCommerce dropdown
	 * 
	 * @return array
	 */
	public function add_brandig_hub_custom_order_status_to_order_statuses( $order_statuses ) {
		$order_statuses['wc-manager-approval'] = 'Wachtend op managergoedkeuring';

		return $order_statuses;
	}

	/**
	 * @param WP_Query $query
	 */
	public function exclude_posts( $query ) {
		if ( current_user_can( 'administrator' ) ) {
			return;
		}

		// use these functions is_search() || is_admin() for getting force hide all posts
		// don't handle `hide from WP_Query` and show 404 option for searching and wp-admin query
		if ( $query->is_main_query() || ! empty( $query->query_vars['um_main_query'] ) ) {
			$force = is_feed() || is_search() || is_admin();

			if ( is_object( $query ) ) {
				$is_singular = $query->is_singular();
			} else {
				$is_singular = ! empty( $query->is_singular ) ? true : false;
			}

			if ( ! $is_singular ) {
				// need to know what post type is here
				$q_values = ! empty( $query->query_vars['post_type'] ) ? $query->query_vars['post_type'] : array();
				if ( ! is_array( $q_values ) ) {
					$q_values = explode( ',', $query->query_vars['post_type'] );
				}

				// 'any' will cause the query var to be ignored.
				if ( in_array( 'any', $q_values, true ) || empty( $q_values ) ) {
					$exclude_posts = $this->exclude_posts_array( $force );
				} else {
					$exclude_posts = $this->exclude_posts_array( $force, $q_values );
				}

				if ( ! empty( $exclude_posts ) ) {
					$post__not_in = $query->get( 'post__not_in', array() );
					$query->set( 'post__not_in', array_merge( wp_parse_id_list( $post__not_in ), $exclude_posts ) );
				}
			}
		}
	}

	/**
	 * Get array with restricted posts
	 *
	 * @param bool $force
	 * @param bool|array|string $post_types
	 *
	 * @return array
	 */
	public function exclude_posts_array( $force = false, $post_types = false ) {
		if ( $this->ignore_exclude ) {
			return array();
		}

		static $cache = array();

		$cache_key = $force ? 'force' : 'default';

		// `force` cache contains all restricted post IDs we can get them all from cache instead new queries
		$force_cache_key = '';
		if ( 'default' === $cache_key ) {
			$force_cache_key = 'force';
		}

		// make $post_types as array if string
		if ( ! empty( $post_types ) ) {
			$post_types = is_array( $post_types ) ? $post_types : array( $post_types );
			$cache_key .= md5( serialize( $post_types ) );
			if ( ! empty( $force_cache_key ) ) {
				$force_cache_key .= md5( serialize( $post_types ) );
			}
		}

		if ( array_key_exists( $cache_key, $cache ) ) {
			return $cache[ $cache_key ];
		}

		$exclude_posts = array();
		if ( current_user_can( 'administrator' ) ) {
			$cache[ $cache_key ] = $exclude_posts;
			return $exclude_posts;
		}

		// @todo using Object Cache `wp_cache_get()` `wp_cache_set()` functions

		// `force` cache contains all restricted post IDs we can get them all from cache instead new queries
		if ( ! empty( $force_cache_key ) && array_key_exists( $force_cache_key, $cache ) ) {
			$post_ids = $cache[ $force_cache_key ];

			if ( ! empty( $post_ids ) ) {
				foreach ( $post_ids as $post_id ) {
					$content_restriction = $this->get_post_privacy_settings( $post_id );
					echo '<pre>';
					var_dump($content_restriction);
					exit;
					if ( ! empty( $content_restriction['_um_access_hide_from_queries'] ) ) {
						array_push( $exclude_posts, $post_id );
					}
				}
			}
		} else {
			$restricted_posts = $this->restrictedAccessPostTypes;
			if ( ! empty( $restricted_posts ) ) {
				$restricted_posts = array_keys( $restricted_posts );
				if ( ! empty( $post_types ) ) {
					$restricted_posts = array_intersect( $post_types, $restricted_posts );
				}
			}

			if ( ! empty( $restricted_posts ) ) {
				$this->ignore_exclude = true;
				// exclude all posts assigned to current term without individual restriction settings
				$post_ids = get_posts(
					array(
						'fields'      => 'ids',
						'post_status' => 'any',
						'post_type'   => $restricted_posts,
						'numberposts' => -1,
						'meta_query'  => array(
							array(
								'key'     => 'brand_hub_restriction',
								'compare' => 'EXISTS',
							),
						),
					)
				);

				$this->ignore_exclude = false;
			}

			$post_ids = empty( $post_ids ) ? array() : $post_ids;
			$restricted_taxonomies = $this->restrictedAccessTaxonomies;

			if ( ! empty( $restricted_taxonomies ) ) {
				$restricted_taxonomies = array_keys( $restricted_taxonomies );
				foreach ( $restricted_taxonomies as $k => $taxonomy ) {
					if ( ! taxonomy_exists( $taxonomy ) ) {
						unset( $restricted_taxonomies[ $k ] );
					}
				}
				$restricted_taxonomies = array_values( $restricted_taxonomies );

				if ( ! empty( $post_types ) ) {
					$taxonomies = array();
					foreach ( $post_types as $p_t ) {
						$taxonomies = array_merge( $taxonomies, get_object_taxonomies( $p_t ) );
					}
					$restricted_taxonomies = array_intersect( $taxonomies, $restricted_taxonomies );
				}
			}

			if ( ! empty( $restricted_taxonomies ) ) {
				global $wpdb;

				$terms = $wpdb->get_results(
					"SELECT tm.term_id AS term_id,
							tt.taxonomy AS taxonomy
					FROM {$wpdb->termmeta} tm
					LEFT JOIN {$wpdb->term_taxonomy} tt ON tt.term_id = tm.term_id
					WHERE tm.meta_key = 'brand_hub_restriction' AND
						  tt.taxonomy IN('" . implode( "','", $restricted_taxonomies ) . "')",
					ARRAY_A
				);

				if ( ! empty( $terms ) ) {
					foreach ( $terms as $term ) {
						if ( ! $this->is_restricted_term( $term['term_id'] ) ) {
							continue;
						}

						$taxonomy_data = get_taxonomy( $term['taxonomy'] );

						$this->ignore_exclude = true;
						// exclude all posts assigned to current term without individual restriction settings
						$posts = get_posts(
							array(
								'fields'      => 'ids',
								'post_status' => 'any',
								'post_type'   => $taxonomy_data->object_type,
								'numberposts' => -1,
								'tax_query'   => array(
									array(
										'taxonomy' => $term['taxonomy'],
										'field'    => 'id',
										'terms'    => $term['term_id'],
									),
								),
								'meta_query'  => array(
									'relation' => 'OR',
									array(
										'relation' => 'AND',
										array(
											'key'     => 'brand_hub_restriction',
											'value'   => 's:26:"_bh_custom_access_settings";s:1:"1"',
											'compare' => 'NOT LIKE',
										),
										array(
											'key'     => 'brand_hub_restriction',
											'value'   => 's:26:"_bh_custom_access_settings";b:1',
											'compare' => 'NOT LIKE',
										),
									),
									array(
										'key'     => 'brand_hub_restriction',
										'compare' => 'NOT EXISTS',
									),
								),
							)
						);
						$this->ignore_exclude = false;

						if ( empty( $posts ) ) {
							continue;
						}

						$post_ids = array_merge( $post_ids, $posts );
					}
				}
			}

			if ( ! empty( $post_ids ) ) {
				$post_ids = array_unique( $post_ids );

				foreach ( $post_ids as $post_id ) {
					// handle every post privacy setting based on post type maybe it's inactive for now
					// if individual restriction is enabled then get post terms restriction settings
					if ( $this->is_restricted( $post_id ) ) {
						if ( true === $force ) {
							array_push( $exclude_posts, $post_id );
						} else {
							$content_restriction = $this->get_post_privacy_settings( $post_id );
							if ( ! empty( $content_restriction['_bh_access_hide_from_queries'] ) ) {
								array_push( $exclude_posts, $post_id );
							}
						}
					}
				}
			}
		}

		$cache[ $cache_key ] = $exclude_posts;
		return $exclude_posts;
	}

	/**
	 * Protect Post Types in query
	 * Restrict content new logic
	 *
	 * @param array          $posts Posts query result.
	 * @param array|WP_Query $query WP_Query instance.
	 *
	 * @return array
	 */
	public function filter_protected_posts( $posts, $query ) {
		if ( current_user_can( 'administrator' ) ) {
			return $posts;
		}

		// Woocommerce AJAX fixes....remove filtration on wc-ajax which goes to Front Page.
		if ( ! empty( $_GET['wc-ajax'] ) && defined( 'WC_DOING_AJAX' ) && WC_DOING_AJAX ) {
			return $posts;
		}

		//if empty
		if ( empty( $posts ) || is_admin() ) {
			return $posts;
		}

		if ( is_object( $query ) ) {
			$is_singular = $query->is_singular();
		} else {
			$is_singular = ! empty( $query->is_singular ) ? true : false;
		}

		if ( is_object( $query ) && is_a( $query, WP_Query::class ) &&
			 ( $query->is_main_query() || ! empty( $query->query_vars['um_main_query'] ) ) ) {
			if ( $is_singular ) {
				if ( ! $this->disable_restriction_pre_queries() && $this->is_restricted( $posts[0]->ID ) ) {
					$content_restriction = $this->get_post_privacy_settings( $posts[0]->ID );
					if ( ! empty( $content_restriction['_bh_access_hide_from_queries'] ) ) {
						unset( $posts[0] );
						return $posts;
					}
				}
			}
		}

		$filtered_posts = array();

		//other filter
		foreach ( $posts as $post ) {
			if ( is_user_logged_in() && isset( $post->post_author ) && $post->post_author == get_current_user_id() ) {
				$filtered_posts[] = $post;
				continue;
			}

			$restriction = $this->get_post_privacy_settings( $post );
			if ( ! $restriction ) {
				$filtered_posts[] = $post;
				continue;
			}

			if ( $is_singular ) {
				$this->singular_page = true;
			}

			if ( ! $this->is_restricted( $post->ID ) ) {
				$filtered_posts[] = $post;
				continue;
			} else {
				if ( $is_singular ) {
					if ( ! isset( $restriction['_um_noaccess_action'] ) || '0' == $restriction['_um_noaccess_action'] ) {
						if ( UM()->options()->get( 'disable_restriction_pre_queries' ) || empty( $restriction['_um_access_hide_from_queries'] ) ) {
							$filtered_post = $this->maybe_replace_title( $post );

							$filtered_posts[] = $filtered_post;
							continue;
						}
					} elseif ( '1' == $restriction['_um_noaccess_action'] ) {
						$curr = UM()->permalinks()->get_current_url();

						if ( ! isset( $restriction['_um_access_redirect'] ) || '0' == $restriction['_um_access_redirect'] ) {

							exit( wp_redirect( esc_url_raw( add_query_arg( 'redirect_to', urlencode_deep( $curr ), um_get_core_page( 'login' ) ) ) ) );

						} elseif ( '1' == $restriction['_um_access_redirect'] ) {

							if ( ! empty( $restriction['_um_access_redirect_url'] ) ) {
								$redirect = $restriction['_um_access_redirect_url'];
							} else {
								$redirect = esc_url_raw( add_query_arg( 'redirect_to', urlencode_deep( $curr ), um_get_core_page( 'login' ) ) );
							}

							exit( wp_redirect( $redirect ) );
						}
					}
				} else {
					if ( empty( $restriction['_bh_access_hide_from_queries'] ) || $this->disable_restriction_pre_queries() ) {
						$filtered_post = $this->maybe_replace_title( $post );
						/** This filter is documented in includes/core/class-access.php */
						$filtered_post = apply_filters( 'um_access_restricted_post_instance', $filtered_post, $post, $query );

						$filtered_posts[] = $filtered_post;
						continue;
					}
				}
			}
		}

		return $filtered_posts;
	}

	/**
	 * Get privacy settings for post
	 * return false if post is not private
	 * Restrict content new logic
	 *
	 * @param WP_Post|int $post Post ID or object
	 *
	 * @return bool|array
	 */
	public function get_post_privacy_settings( $post ) {
		// break for incorrect post
		if ( empty( $post ) ) {
			return false;
		}

		if ( ! is_numeric( $post ) && ! is_a( $post, WP_Post::class ) ) {
			return false;
		}

		static $cache = array();

		$cache_key = is_numeric( $post ) ? $post : $post->ID;

		if ( isset( $cache[ $cache_key ] ) ) {
			return $cache[ $cache_key ];
		}

		if ( is_numeric( $post ) ) {
			$post = get_post( $post );
		}

		//if logged in administrator all pages are visible
		if ( current_user_can( 'administrator' ) ) {
			$cache[ $cache_key ] = false;
			return false;
		}

		$exclude = false;

		// $restricted_posts = UM()->options()->get( 'restricted_access_post_metabox' );
		$restricted_posts = $this->restrictedAccessPostTypes;

		if ( ! empty( $post->post_type ) && ! empty( $restricted_posts[ $post->post_type ] ) ) {
			$restriction = get_post_meta( $post->ID, 'brand_hub_restriction', true );

			if ( ! empty( $restriction['_bh_custom_access_settings'] ) ) {
				// hier moeten we inkomen.
				if ( ! isset( $restriction['_bh_accessible'] ) ) {
					echo '<pre>';
					var_dump('as');
					exit;
					$restricted_taxonomies = UM()->options()->get( 'restricted_access_taxonomy_metabox' );

					//get all taxonomies for current post type
					$taxonomies = get_object_taxonomies( $post );

					//get all post terms
					$terms = array();
					if ( ! empty( $taxonomies ) ) {
						foreach ( $taxonomies as $taxonomy ) {
							if ( empty( $restricted_taxonomies[ $taxonomy ] ) ) {
								continue;
							}

							$terms = array_merge( $terms, wp_get_post_terms( $post->ID, $taxonomy, array( 'fields' => 'ids', 'um_ignore_exclude' => true, ) ) );
						}
					}

					//get restriction options for first term with privacy settigns
					foreach ( $terms as $term_id ) {
						$restriction = get_term_meta( $term_id, 'um_content_restriction', true );

						if ( ! empty( $restriction['_um_custom_access_settings'] ) ) {
							if ( ! isset( $restriction['_um_accessible'] ) ) {
								continue;
							} else {
								$cache[ $cache_key ] = $restriction;
								return $restriction;
							}
						}
					}

					$cache[ $cache_key ] = false;
					return false;
				} else {
					$cache[ $cache_key ] = $restriction;
					return $restriction;
				}
			}
		}

		//post hasn't privacy settings....check all terms of this post
		$restricted_taxonomies = $this->restrictedAccessTaxonomies;

		//get all taxonomies for current post type
		$taxonomies = get_object_taxonomies( $post );

		//get all post terms
		$terms = array();
		if ( ! empty( $taxonomies ) ) {
			foreach ( $taxonomies as $taxonomy ) {
				if ( empty( $restricted_taxonomies[ $taxonomy ] ) ) {
					continue;
				}

				$terms = array_merge( $terms, wp_get_post_terms( $post->ID, $taxonomy, array( 'fields' => 'ids', 'um_ignore_exclude' => true, ) ) );
			}
		}

		//get restriction options for first term with privacy settings
		foreach ( $terms as $term_id ) {
			$restriction = get_term_meta( $term_id, 'brand_hub_restriction', true );

			if ( ! empty( $restriction['_um_custom_access_settings'] ) ) {
				echo '<pre>';
				var_dump($restriction);
				exit;
				if ( ! isset( $restriction['_um_accessible'] ) ) {
					continue;
				} else {
					$cache[ $cache_key ] = $restriction;
					return $restriction;
				}
			}
		}

		$cache[ $cache_key ] = false;
		//post is public
		return false;
	}

	/**
	 * @param WP_Term_Query $query
	 */
	public function pww_exclude_hidden_terms_query( $query ) {
		if ( current_user_can( 'administrator' ) || ! empty( $query->query_vars['um_ignore_exclude'] ) ) {
			return;
		}

		$exclude = $this->exclude_terms_array( $query );
		if ( ! empty( $exclude ) ) {
			$query->query_vars['exclude'] = ! empty( $query->query_vars['exclude'] ) ? wp_parse_id_list( $query->query_vars['exclude'] ) : $exclude;
		}
	}

	/**
	 * Get array with restricted terms
	 *
	 * @param WP_Term_Query $query
	 *
	 * @return array
	 */
	public function exclude_terms_array( $query ) {
		$exclude = array();

		// $restricted_taxonomies = UM()->options()->get( 'restricted_access_taxonomy_metabox' );
		$restricted_taxonomies = $this->restrictedAccessTaxonomies;

		if ( ! empty( $restricted_taxonomies ) ) {
			$restricted_taxonomies = array_keys( $restricted_taxonomies );
			foreach ( $restricted_taxonomies as $k => $taxonomy ) {
				if ( ! taxonomy_exists( $taxonomy ) ) {
					unset( $restricted_taxonomies[ $k ] );
				}
			}
			$restricted_taxonomies = array_values( $restricted_taxonomies );

			if ( ! empty( $restricted_taxonomies ) ) {
				if ( isset( $query->query_vars['taxonomy'] ) && is_array( $query->query_vars['taxonomy'] ) ) {
					$restricted_taxonomies = array_intersect( $query->query_vars['taxonomy'], $restricted_taxonomies );
				} elseif ( ! empty( $query->query_vars['term_taxonomy_id'] ) ) {
					$term_taxonomy_ids = is_array( $query->query_vars['term_taxonomy_id'] ) ? $query->query_vars['term_taxonomy_id'] : array( $query->query_vars['term_taxonomy_id'] );

					global $wpdb;
					$tax_in_query = $wpdb->get_col( "SELECT DISTINCT taxonomy FROM {$wpdb->term_taxonomy} WHERE term_taxonomy_id IN('" . implode( "','", $term_taxonomy_ids ) . "')" );
					if ( ! empty( $tax_in_query ) ) {
						$restricted_taxonomies = array_intersect( $tax_in_query, $restricted_taxonomies );
					} else {
						$restricted_taxonomies = array();
					}
				}
			}
		}

		if ( empty( $restricted_taxonomies ) ) {
			return $exclude;
		}

		$cache_key = md5( serialize( $restricted_taxonomies ) );

		static $cache = array();

		if ( array_key_exists( $cache_key, $cache ) ) {
			return $cache[ $cache_key ];
		}

		$term_ids = get_terms(
			array(
				'taxonomy'          => $restricted_taxonomies,
				'hide_empty'        => false,
				'fields'            => 'ids',
				'meta_query'        => array(
					'key'     => 'brand_hub_restriction',
					'compare' => 'EXISTS',
				),
				'um_ignore_exclude' => true,
			)
		);

		if ( empty( $term_ids ) || is_wp_error( $term_ids ) ) {
			$cache[ $cache_key ] = $exclude;
			return $exclude;
		}

		foreach ( $term_ids as $term_id ) {
			if ( $this->is_restricted_term( $term_id ) ) {
				$exclude[] = $term_id;
			}
		}

		$exclude = apply_filters( 'um_exclude_restricted_terms_ids', $exclude );
		$cache[ $cache_key ] = $exclude;
		return $exclude;
	}

	/**
	 * Is post restricted?
	 *
	 * @param int $post_id
	 * @param bool $on_single_page
	 * @param bool $ignore_cache
	 *
	 * @return bool
	 */
	public function is_restricted( $post_id, $on_single_page = false, $ignore_cache = false ) {
		// break for incorrect post
		if ( empty( $post_id ) ) {
			return false;
		}

		static $cache = array();

		if ( isset( $cache[ $post_id ] ) && ! $ignore_cache ) {
			return $cache[ $post_id ];
		}

		if ( current_user_can( 'administrator' ) ) {
			if ( ! $ignore_cache ) {
				$cache[ $post_id ] = false;
			}
			return false;
		}

		$post = get_post( $post_id );
		if ( is_user_logged_in() && isset( $post->post_author ) && $post->post_author == get_current_user_id() ) {
			if ( ! $ignore_cache ) {
				$cache[ $post_id ] = false;
			}
			return false;
		}

		$restricted = true;

		$restriction = $this->get_post_privacy_settings( $post_id );

		if ( ! $restriction ) {
			$restricted = false;
		} else {
			if ( '0' == $restriction['_bh_accessible'] ) {
				//post is private
				$restricted = false;
				if ( $on_single_page ) {
					$this->allow_access = true;
				}
			} elseif ( '1' == $restriction['_bh_accessible'] ) {
				//if post for not logged in users and user is not logged in
				if ( ! is_user_logged_in() ) {
					$restricted = false;
					if ( $on_single_page ) {
						$this->allow_access = true;
					}
				}
			} elseif ( '2' == $restriction['_bh_accessible'] ) {
				//if post for logged in users and user is not logged in
				if ( is_user_logged_in() ) {
					if ( empty( $restriction['_bh_access_id'] ) ) {
							$restricted = false;
							if ( $on_single_page ) {
								$this->allow_access = true;
							}
					} else {
						$user_has_access = $this->user_has_access_to_brand( $restriction['_bh_access_id'] );

						if ( $user_has_access ) {
							$restricted = false;
							if ( $on_single_page ) {
								$this->allow_access = true;
							}
						}
					}
				} else {
					if ( ! isset( $restriction['_bh_noaccess_action'] ) || '0' == $restriction['_bh_noaccess_action'] ) {
						if ( $this->disable_restriction_pre_queries() || empty( $restriction['_bh_access_hide_from_queries'] ) ) {
							if ( $on_single_page ) {
								$this->allow_access = true;
							}
						}
					}
				}
			}
		}

		// $restricted = apply_filters( 'um_is_restricted_post', $restricted, $post_id, $on_single_page );

		if ( ! $ignore_cache ) {
			$cache[ $post_id ] = $restricted;
		}

		return $restricted;
	}

	/**
	 * Is term restricted?
	 *
	 * @param int $term_id
	 * @param bool $on_term_page
	 * @param bool $ignore_cache
	 *
	 * @return bool
	 */
	public function is_restricted_term( $term_id, $on_term_page = false, $ignore_cache = false ) {
		static $cache = array();

		if ( isset( $cache[ $term_id ] ) && ! $ignore_cache ) {
			return $cache[ $term_id ];
		}

		if ( current_user_can( 'administrator' ) ) {
			if ( ! $ignore_cache ) {
				$cache[ $term_id ] = false;
			}
			return false;
		}

		// $restricted_taxonomies = UM()->options()->get( 'restricted_access_taxonomy_metabox' );
		$restricted_taxonomies = $this->restrictedAccessTaxonomies;

		if ( empty( $restricted_taxonomies ) ) {
			if ( ! $ignore_cache ) {
				$cache[ $term_id ] = false;
			}
			return false;
		}

		$term = get_term( $term_id );
		if ( empty( $term->taxonomy ) || empty( $restricted_taxonomies[ $term->taxonomy ] ) ) {
			if ( ! $ignore_cache ) {
				$cache[ $term_id ] = false;
			}
			return false;
		}

		$restricted = true;

		// $this->allow_access = true only in case if the

		$restriction = get_term_meta( $term_id, 'brand_hub_restriction', true );

		if ( empty( $restriction ) ) {
			$restricted = false;
		} else {
			if ( empty( $restriction['_bh_custom_access_settings'] ) ) {
				$restricted = false;
			} else {
				if ( '0' == $restriction['_bh_accessible'] ) {
					//term is private
					$restricted = false;
					if ( $on_term_page ) {
						$this->allow_access = true;
					}
				} elseif ( '1' == $restriction['_bh_accessible'] ) {
					//if term for not logged in users and user is not logged in
					if ( ! is_user_logged_in() ) {
						$restricted = false;
						if ( $on_term_page ) {
							$this->allow_access = true;
						}
					}
				} elseif ( '2' == $restriction['_bh_accessible'] ) {
					//if term for logged in users and user is not logged in
					if ( is_user_logged_in() ) {
						// $custom_restrict = $this->um_custom_restriction( $restriction );

						if ( empty( $restriction['_bh_access_id'] ) ) {
							$restricted = false;
							if ( $on_term_page ) {
								$this->allow_access = true;
							}
						} else {
							// $user_can = $this->user_can( get_current_user_id(), $restriction['_um_access_roles'] );
							$user_has_access = $this->user_has_access_to_brand( $restriction['_bh_access_id'] );

							if ( $user_has_access ) {
								$restricted = false;
								if ( $on_term_page ) {
									$this->allow_access = true;
								}
							}
						}
					}
				}
			}
		}

		$restricted = apply_filters( 'um_is_restricted_term', $restricted, $term_id, $on_term_page );

		if ( ! $ignore_cache ) {
			$cache[ $term_id ] = $restricted;
		}

		return $restricted;
	}

	private function user_has_brand_hub()
	{
		if ( ! is_user_logged_in() ) {
			return false;
		}

		$userBrandHub = get_user_meta( get_current_user_id(), 'brand_hub', true );
		if ( $userBrandHub ) {
			$this->set_user_brand_hub_settings( $userBrandHub['id'] );
			$this->set_brand_hub_user_settings( $userBrandHub['id'] );

			return true;
		}

		return false;
	}

	private function user_has_access_to_brand( $brand_hub_id )
	{
		$userBrandSettings = get_user_meta( get_current_user_id(), 'brand_hub', true );

		if ( $userBrandSettings ) {
			if ( isset( $userBrandSettings['id'] ) && $userBrandSettings['id'] == $brand_hub_id ) {
				$this->set_user_brand_hub_settings( $userBrandSettings['id'] );

				return true;
			}
		}

		return false;
	}

	private function get_user_brand_hub_settings(string $key = '')
	{
		if (!empty($key)) {
			if (!isset($this->user_brand_hub[$key])) {
				return false;
			}

			if (empty($this->user_brand_hub[$key])) {
				return false;
			}

			return $this->user_brand_hub[$key];
		}

		return $this->user_brand_hub;
	}

	private function set_user_brand_hub_settings( $brand_hub_id )
	{
		$this->user_brand_hub = get_option('brand_hub_' . $brand_hub_id);
	}

	private function set_brand_hub_user_settings( $brand_hub_id )
	{
		$this->brand_hub_user_settings = get_user_meta( get_current_user_id(), 'brand_hub', true );
	}

	private function get_brand_hub_user_settings( string $key = '' )
	{
		if (!empty($key)) {
			if (!isset($this->brand_hub_user_settings[$key])) {
				return false;
			}

			if (empty($this->brand_hub_user_settings[$key])) {
				return false;
			}

			return $this->brand_hub_user_settings[$key];
		}

		return $this->brand_hub_user_settings;
	}

	private function disable_restriction_pre_queries()
	{
		return false;
	}







	// New

	public function allowed_page_ids()
	{
		if (is_user_logged_in()) {
			// Controlleer of gebruiker een hub gekoppeld heeft
			if ($this->user_has_brand_hub()) {
				$pagesAllowedForUser = $this->get_pages_with_brand_hub_id($this->pww_current_brand_hub_id());

				$pagesAllowedForUser[] = wc_get_page_id('shop');		// Winkelpagina
				$pagesAllowedForUser[] = wc_get_page_id('cart');		// Winkelmandpagina
				$pagesAllowedForUser[] = wc_get_page_id('checkout');	// Afrekenpagina
				$pagesAllowedForUser[] = wc_get_page_id('myaccount');  // Accountpagina

				return $pagesAllowedForUser;
			}
		}

		return [];
	}

	public function brand_hub_pre_get_posts( $query )
	{
		// Wanneer je een admin bent heb je altijd toegang
		if ( current_user_can( 'administrator' ) || is_admin() ) {
			return;
		}

		// Controlleer of gebruiker is ingelogd
		if (is_user_logged_in()) {
			// Controlleer of gebruiker een hub gekoppeld heeft
			if ($this->user_has_brand_hub()) {
				// $pagesAllowedForUser = $this->get_pages_with_brand_hub_id($this->pww_current_brand_hub_id());

				// $pagesAllowedForUser[] = wc_get_page_id('shop');		// Winkelpagina
				// $pagesAllowedForUser[] = wc_get_page_id('cart');		// Winkelmandpagina
				// $pagesAllowedForUser[] = wc_get_page_id('checkout');	// Afrekenpagina
				// $pagesAllowedForUser[] = wc_get_page_id('myaccount');  // Accountpagina

				$query->set('post__in', $this->allowed_page_ids());
			}
		} else {
			// Als gebruiker niet is ingelogd of wel is ingelogd maar geen brand hub gekoppeld heeft dan alle pagina's die de meta key brand_hub_id hebben uitsluiten
			$query->set('post__not_in', $this->get_pages_with_brand_hub_id());
		}
	}

	/**
	 * Protect Post Types in query
	 * Restrict content new logic
	 *
	 * @param array          $posts Posts query result.
	 * @param array|WP_Query $query WP_Query instance.
	 *
	 * @return array
	 */
	public function brand_hub_protected_posts( $posts, $query ) {
		if ( current_user_can( 'administrator' ) ) {
			return $posts;
		}

		// Woocommerce AJAX fixes....remove filtration on wc-ajax which goes to Front Page.
		if ( ! empty( $_GET['wc-ajax'] ) && defined( 'WC_DOING_AJAX' ) && WC_DOING_AJAX ) {
			return $posts;
		}

		//if empty
		if ( empty( $posts ) || is_admin() ) {
			return $posts;
		}

		if ( is_object( $query ) ) {
			$is_singular = $query->is_singular();
		} else {
			$is_singular = ! empty( $query->is_singular ) ? true : false;
		}

		if ( is_object( $query ) && is_a( $query, WP_Query::class ) &&
			 ( $query->is_main_query() || ! empty( $query->query_vars['um_main_query'] ) ) ) {
			if ( $is_singular ) {
				if ( ! $this->disable_restriction_pre_queries() && $this->is_restricted( $posts[0]->ID ) ) {
					$content_restriction = $this->get_post_privacy_settings( $posts[0]->ID );
					if ( ! empty( $content_restriction['_bh_access_hide_from_queries'] ) ) {
						unset( $posts[0] );
						return $posts;
					}
				}
			}
		}

		$filtered_posts = array();

		//other filter
		foreach ( $posts as $post ) {
			if ( is_user_logged_in() && isset( $post->post_author ) && $post->post_author == get_current_user_id() ) {
				$filtered_posts[] = $post;
				continue;
			}

			// $restriction = $this->get_post_privacy_settings( $post );
			// if ( ! $restriction ) {
			// 	$filtered_posts[] = $post;
			// 	continue;
			// }

			if ( $is_singular ) {
				$this->singular_page = true;
			}

			if (in_array($post->ID, $this->allowed_page_ids())) {
				$filtered_posts[] = $post;
				continue;
			}

			// if ( ! $this->post_is_restricted( $post->ID ) ) {
			// 	$filtered_posts[] = $post;
			// 	continue;
			// }
		}

		return $filtered_posts;
	}

	/**
	 * Is post restricted?
	 *
	 * @param int $post_id
	 * @param bool $on_single_page
	 * @param bool $ignore_cache
	 *
	 * @return bool
	 */
	public function post_is_restricted($post_id) {
		// break for incorrect post
		if ( empty( $post_id ) ) {
			return false;
		}

		if (in_array($post_id, $this->get_pages_with_brand_hub_id())) {
			return false;
		}

		return true;
	}

	private function get_pages_with_brand_hub_id($brand_hub_id = false) {
		global $wpdb;
	
		$meta_key = 'brand_hub_id';
		$query = "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s";
	
		// if (is_user_logged_in()) {
		// 	if ($this->user_has_brand_hub()) {
		// 		$brand_hub_id = $this->pww_current_brand_hub_id();

		// 		$query .= " AND meta_value != %s";
		// 	}
		// }

		if ($brand_hub_id) {
			$query .= " AND meta_value = %s";
		}
	
		// Bereid de query voor
		$prepared_query = $wpdb->prepare($query, $meta_key, $brand_hub_id);

		// Voer de query uit en verkrijg de page IDs
		$page_ids = $wpdb->get_col($prepared_query);
	
		return $page_ids;
	}











	// New functions

	/**
	 * Modifies the main query to control access to Brand Hub content based on user permissions.
	 *
	 * This function restricts access to specific posts based on the user's access level.
	 * Administrators bypass all restrictions, while regular users see only their allowed posts
	 * or are restricted from Brand Hub pages if they lack permissions.
	 *
	 * @param WP_Query $query The main query instance to modify.
	 *
	 * @return void
	 */
	public function brandingHubPreGetPosts($query): void
	{
		// Allow full access for administrators and in the admin environment
		if (current_user_can('administrator') || is_admin()) {
			return;
		}

		if (!in_array($query->get('post_type'), ['post', 'page',  'product'])) {
			return;
		}

		// Bypass restrictions for menu items
		if ($query->get('post_type') === 'nav_menu_item') {
			return;
		}

		// Check if user is logged in and has access to the Brand Hub
		if (is_user_logged_in() && $this->user_has_brand_hub()) {
			$brandHubUserSettings = $this->get_brand_hub_user_settings();

			// Deny access if no specific product IDs are set
			if (empty($brandHubUserSettings['product_ids'])) {
				$query->set('post__in', []);
				return;
			}

			// Allow only specific product and default page IDs
			$allowedPageIds = array_merge(
				$brandHubUserSettings['product_ids'],
				$this->defaultPagesIds()
			);

			$brandHubSettings = $this->get_user_brand_hub_settings();
			if (is_array($brandHubSettings) && isset($brandHubSettings['home_id'])) {
				$allowedPageIds[] = $brandHubSettings['home_id'];
			}

			$query->set('post__in', $allowedPageIds);
		} else {
			// Exclude Brand Hub pages for non-Brand Hub users
			$idsToDisallow = $this->getAllIdsOfBrandHubPages();
			$query->set('post__not_in', $idsToDisallow);
		}
	}

	/**
	 * Filters posts based on user permissions, access to Brand Hub, and WooCommerce AJAX conditions.
	 *
	 * @param array $posts  List of posts to filter.
	 * @param WP_Query $query  The query object for the current request.
	 * 
	 * @return array Filtered list of posts based on access permissions.
	 */
	public function brandingHubThePosts($posts, $query)
	{
		// Always allow access for administrators and in the admin environment
		if (current_user_can('administrator') || is_admin()) {
			return $posts;
		}
	
		// Exclude WooCommerce AJAX requests from filtering
		if (!empty($_GET['wc-ajax']) && defined('WC_DOING_AJAX') && WC_DOING_AJAX) {
			return $posts;
		}
	
		// Immediately return if there are no posts
		if (empty($posts)) {
			return $posts;
		}
	
		// Get user data if logged in
		$isLoggedIn = is_user_logged_in();
		$currentUserId = $isLoggedIn ? get_current_user_id() : null;
		$userHasBrandHub = $isLoggedIn && $this->user_has_brand_hub();
		$brandHubUserSettings = $userHasBrandHub ? $this->get_brand_hub_user_settings() : [];
	
		// Merge allowed page IDs if user has access to the Brand Hub
		$allowedPageIds = $userHasBrandHub
			? array_merge($brandHubUserSettings['product_ids'], $this->defaultPagesIds())
			: [];

		$brandHubSettings = $this->get_user_brand_hub_settings();
		if (is_array($brandHubSettings) && isset($brandHubSettings['home_id'])) {
			$allowedPageIds[] = $brandHubSettings['home_id'];
		}
	
		// Filter posts based on conditions
		$filteredPosts = array_filter($posts, function ($post) use ($currentUserId, $isLoggedIn, $userHasBrandHub, $allowedPageIds) {
			// Allow post if the user is the author of the post
			if ($isLoggedIn && $post->post_author == $currentUserId) {
				return true;
			}
	
			// Allow post if the post ID is in the allowed page IDs list
			if ($userHasBrandHub && in_array($post->ID, $allowedPageIds)) {
				return true;
			}
	
			// Exclude post if it belongs to the Brand Hub pages
			return !in_array($post->ID, $this->getAllIdsOfBrandHubPages());
		});
	
		// Return filtered posts
		return $filteredPosts;
	}

	/**
	 * Retrieves default WooCommerce page IDs for commonly used pages.
	 *
	 * This function returns the page IDs for the standard WooCommerce pages:
	 * shop, cart, checkout, and my account.
	 *
	 * @return array An array of page IDs for the default WooCommerce pages.
	 */
	private function defaultPagesIds(): array
	{
		$pages = ['shop', 'cart', 'checkout', 'myaccount'];

		return array_map(fn($page) => wc_get_page_id($page), $pages);
	}

	/**
	 * Retrieves all post IDs associated with the Brand Hub.
	 *
	 * This function queries the WordPress database for posts that have
	 * a specific meta key (`brand_hub_id`) indicating they are part of the Brand Hub.
	 *
	 * @global wpdb $wpdb The WordPress database abstraction object.
	 *
	 * @return array An array of post IDs associated with the Brand Hub.
	 */
	private function getAllIdsOfBrandHubPages(): array
	{
		global $wpdb;

		// Define the meta key to search for
		$metaKey = 'brand_hub_id';

		// Prepare and execute the query to retrieve post IDs
		$query = $wpdb->prepare("SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = %s", $metaKey);

		return $wpdb->get_col($query);
	}

	/**
	 * Redirects to a custom homepage or checkout based on user settings and cart status.
	 *
	 * This function checks if the user has a custom homepage set in their Brand Hub settings.
	 * If so, it redirects to that page when visiting the homepage or front page.
	 * Additionally, it redirects to the checkout page if the cart is not empty.
	 *
	 * @return void
	 */
	public function customHomepageContent(): void
	{
		// Redirect to custom homepage if set and on home or front page
		if (is_home() || is_front_page()) {
			$customPageId = $this->get_user_brand_hub_settings('home_id');

			if ($customPageId) {
				wp_redirect(get_post_permalink($customPageId));
				exit;
			}
		}

		// Redirect to checkout if on cart page and the cart has items
		if (is_cart() && WC()->cart->get_cart_contents_count() > 0) {
			wp_redirect(wc_get_checkout_url());
			exit;
		}
	}

	/**
	 * Optionally changes the layout preset ID based on user settings.
	 *
	 * This function checks if the user has a custom preset ID set in their Brand Hub settings.
	 * If a custom preset ID is found, it returns an array with the custom preset ID.
	 * Otherwise, it returns the original active presets.
	 *
	 * @param array $active_presets The currently active preset IDs.
	 * @param array $all All available preset IDs.
	 *
	 * @return array The modified or original array of preset IDs.
	 */
	public function maybeChangeLayoutId(array $active_presets, array $all): array
	{
		// Get the custom preset ID from user settings
		$customPresetId = $this->get_user_brand_hub_settings('preset_id');

		// If a custom preset ID exists, return it in an array
		if ($customPresetId) {
			return [$customPresetId => $customPresetId];
		}

		// Return the original active presets if no custom preset ID is found
		return $active_presets;
	}

	/**
	 * Adds a custom CSS class to the body tag if the user is in the Brand Hub.
	 *
	 * This function adds the 'is-brand-hub' class to the body class array,
	 * allowing custom styling and scripts specifically for Brand Hub users.
	 *
	 * @return void
	 */
	public function addBrandHubClassToBody(): void
	{
		add_filter('body_class', function ($classes) {
			$classes[] = 'is-brand-hub';
			return $classes;
		});
	}

}