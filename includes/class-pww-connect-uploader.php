<?php

/**
 * @since      1.0.0
 * @package    Pww_Connect
 * @subpackage Pww_Connect/includes
 * @author     Printwebwinkel B.V. <support@printwebwinkel.nl>
 */
class PwwConnectUploader
{

	public function run()
	{

		add_shortcode('pww_print_uploader', array($this, 'pww_print_uploader'));

	}

	public function pww_print_uploader()
	{
		
		$order_id = isset($_GET['order-id']) ? sanitize_text_field($_GET['order-id']) : '';
		$order_email = isset($_GET['order-email']) ? sanitize_text_field($_GET['order-email']) : '';

		$output = '<div id="pww_connect-print_uploader" data-pww-result-wrapper="pww_connect-print_uploader" data-order-id="' . esc_attr($order_id) . '" data-order-email="' . esc_attr($order_email) . '"></div>';

		return $output;

	}

}