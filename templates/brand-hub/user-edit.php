<?php
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<section class="woocommerce-order-downloads">
    <h2>Gebruiker bewerken</h2>

    <form class="woocommerce-EditAccountForm edit-account" action="" method="post">
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
            <label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr( $brand_hub_user->first_name ?? '' ); ?>" />
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
            <label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr( $brand_hub_user->last_name ?? '' ); ?>" />
        </p>
        <div class="clear"></div>

        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr( $brand_hub_user->user_email ?? '' ); ?>" />
        </p>
        <div class="clear"></div>

        <?php if (!isset($brand_hub_user) || (isset($brand_hub_user) && empty($brand_hub_user)) || (isset($brand_hub_user->ID) && $brand_hub_user->ID != get_current_user_id())): ?>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <label for="account_budget">Budget&nbsp;<span class="required">*</span></label>
            <input type="number" class="woocommerce-Input woocommerce-Input--text input-text" name="account_budget" id="account_budget" autocomplete="budget" max="<?php echo $max_budget; ?>" value="<?php echo esc_attr( $brand_hub_user->brand_hub['budget'] ?? '' ); ?>" />

            <span class="form-info-text">
                U kunt een budget instellen tot een maximum van <?php echo wc_price($max_budget); ?>.
            </span>
        </p>
        <?php endif; ?>

        <?php if (!isset($brand_hub_user->user_email)) : ?>
        <div class="clear"></div>
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
            <label for="account_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input type="text" class="woocommerce-Input woocommerce-Input--email input-text" name="account_username" id="account_username" autocomplete="username" />
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
            <label for="account_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="account_password" id="account_password" autocomplete="pass-word" />
        </p>
        <?php endif; ?>

        <p>
            <button type="submit" class="woocommerce-Button button<?php echo esc_attr( wc_wp_theme_get_element_class_name( 'button' ) ? ' ' . wc_wp_theme_get_element_class_name( 'button' ) : '' ); ?>" name="save_hub_user_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
        </p>
    </form>

</section>