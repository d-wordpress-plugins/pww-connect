<section class="woocommerce-info budget-alert-message">
    <p>Deze bestelling overschrijdt het budget van de gebruiker met <?php echo wc_price($overBudget); ?>; het totale budget is <?php echo wc_price($budget); ?>.</p>
</section>