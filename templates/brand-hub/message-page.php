<?php
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<section class="woocommerce-order-downloads">
    <?php if (isset($message) && !empty($message)) { ?>
    <div class="woocommerce-message" role="alert"><?php echo $message; ?></div>
    <?php } ?>
</section>