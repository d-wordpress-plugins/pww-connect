<?php
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<section class="woocommerce-order-downloads">
    <div class="brand-hub-users-actions">
        <h2>Gebruikers</h2>
        <?php if (count($brand_hub_users) < $max_users) : ?>
        <a href="<?php echo add_query_arg('user_create', true, wc_get_endpoint_url('brand-hub-users')); ?>" class="woocommerce-button button view">Gebruiker aanmaken</a>
        <?php endif; ?>
    </div>

	<table class="woocommerce-table woocommerce-table--order-downloads shop_table shop_table_responsive order_details">
		<thead>
			<tr>
                <th class=""><span>Gebruiker</span></th>
                <th class=""><span class="nobr">Rol</span></th>
                <th class=""><span class="nobr">Budget</span></th>
                <th class=""><span class="nobr"></span></th>
			</tr>
		</thead>

		<?php foreach ( $brand_hub_users as $brand_hub_user ) : ?>
			<tr>
                <td class="" data-title="">
                    <strong><?php echo $brand_hub_user->first_name; ?> <?php echo $brand_hub_user->last_name; ?></strong>
                    <br>
                    <?php echo $brand_hub_user->user_email; ?>
                </td>
                <td class="" data-title="">
                    <?php
                    if ($brand_hub_user->brand_hub['role'] == 'user') {
                        echo 'Gebruiker';
                    } else {
                        echo 'Manager';
                    }
                    ?>
                </td>
                <td class="" data-title="">
                    <?php
                    echo wc_price($spent_per_user[$brand_hub_user->id] ?? 0); ?> / <?php echo wc_price($brand_hub_user->brand_hub['budget'] ?? 0);
                    ?>
                </td>
                <td class="" data-title="">
                    <?php echo '<a href="' . add_query_arg('user_id', $brand_hub_user->id, wc_get_endpoint_url('brand-hub-users')) . '">Bewerken</a>'; ?>
                </td>
			</tr>
		<?php endforeach; ?>
	</table>
</section>