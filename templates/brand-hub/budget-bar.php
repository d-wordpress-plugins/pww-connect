<div class="progress-wrapper">
    <div class="progress-container-text">
        <span>U heeft <?php echo wc_price($amountSpent); ?> van uw budget van <?php echo wc_price($totalBudget); ?> besteed.</span>
    </div>
    <div class="progress-container">
        <div class="progress-bar" id="progress-bar" data-percentage="<?php echo $percentage; ?>"></div>
    </div>
</div>